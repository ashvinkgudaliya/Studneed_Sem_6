//
//  MainCategory.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 20/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

var allDirectoryType = NSArray()
var allCategory = NSArray()
var currentOpenCategoryImages = String()

import UIKit
import Alamofire

class Main {
    
    static let webService = Main()
    
    func directoryType(compilation:@escaping (Bool) -> ()){
        Alamofire.request(Web.url.directoryType, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    allDirectoryType = aDict.array(forKey: Web.key.directoryType)
                    compilation(true)
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Failer")
                break
            }
        }
    }
    
    func getCategory(compilation:@escaping (Bool) -> ()){
        Alamofire.request(Web.url.getCategory, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    allCategory = aDict.array(forKey: Web.key.category)
                    compilation(true)
                    break
                default:
                    Nottification.message.error(body: "Category Load request failed")
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Failer")
                break
            }
        }
    }
}

class BookData{
    var name = String()
    var image = String()
    var discription = String()
    
    init(name:String,image:String,discription:String) {
        self.name = name
        self.image = image
        self.discription = discription
    }
}
