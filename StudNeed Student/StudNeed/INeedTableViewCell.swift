//
//  INeedTableViewCell.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 06/04/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class INeedTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var directoryName: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var disLbl: UILabel!
    @IBOutlet weak var requestCount: UILabel!
    @IBOutlet weak var viewsCountLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backView.shadowEnable()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func asignValue(data:NSDictionary){
        headingLbl.text = data.string(forKey: "heading")
        directoryName.text = data.string(forKey: "directory")
        disLbl.text = data.string(forKey: "discription")

        locationLbl.text = data.string(forKey: "city")
        viewsCountLbl.text = data.string(forKey: "view")
        requestCount.text = data.string(forKey: "request")
    }
}
