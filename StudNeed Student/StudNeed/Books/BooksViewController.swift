//
//  BooksViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 27/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class BooksViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    let booksTableView = UITableView()
    var allBooksDetils = NSArray()
    var directoryId = NSInteger()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.code.lightBar
        WebServices.data.displayDirectoryData(directoryId: directoryId) { response in
            self.allBooksDetils = response
            self.booksTableView.reloadData()
        }
        
        navigationBarDesign()
        viewControllerDesign()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func navigationBarDesign(){
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Books Store"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
    }
    
    func viewControllerDesign(){
        booksTableView.frame = CGRect(x: 0, y:  5, width: self.view.frame.width, height: self.view.frame.height - 5)
        booksTableView.delegate = self
        booksTableView.showsHorizontalScrollIndicator = false
        booksTableView.showsVerticalScrollIndicator = false
        booksTableView.separatorStyle = .none
        booksTableView.backgroundColor = Color.code.lightBar
        booksTableView.dataSource = self
        booksTableView.register(UINib(nibName: "BookTableViewCell", bundle: nil), forCellReuseIdentifier: "bookCell")
        self.view.addSubview(booksTableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allBooksDetils.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let bookCell = tableView.dequeueReusableCell(withIdentifier: "bookCell", for: indexPath) as! BookTableViewCell
        bookCell.selectionStyle = .none
        bookCell.backgroundColor = Color.code.lightBar
        bookCell.assignValue(data: allBooksDetils.dictionary(at: indexPath.row))
        return bookCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailsViewController()
        currentDirectory = .book
        vc.venderDirectoryId = allBooksDetils.dictionary(at: indexPath.row).string(forKey: "venderDirectoryId")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return booksTableView.frame.height/3
    }
    
    func backButtonAction(){
        self.navigationController!.popViewController(animated: true)
    }
}
