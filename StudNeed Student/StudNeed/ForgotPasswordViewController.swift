//
//  ForgotPasswordViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 16/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate{
    
    let imagesBackBroundView = UIImageView()
    let txtUserName = AMInputView()
    let btnForgotPassword = ZFRippleButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarDesign()
        designforgotPasswordView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func navigationBarDesign(){
        self.view.backgroundColor = Color.code.lightBar
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Forgot Password"
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
    }
    
    func designforgotPasswordView(){
        imagesBackBroundView.frame = self.view.bounds
        imagesBackBroundView.image = UIImage(named: "HostalDefaultImages.jpg")
        imagesBackBroundView.isBlurEffect()
        self.view.addSubview(imagesBackBroundView)
        
        let gap = self.view.frame.height/33
        txtUserName.frame = CGRect(x: gap, y: self.view.frame.height/3, width: self.view.frame.width - gap*2, height: gap*3)
        btnForgotPassword.frame = CGRect(x: gap, y: txtUserName.frame.height + txtUserName.frame.origin.y + gap, width: self.view.frame.width - gap*2, height: gap*3)
        
        txtUserName.placeholder = "Email Id"
        txtUserName.returnKeyType = .done
        txtUserName.isCheckValidation = .email
        
        btnForgotPassword.setTitle("Submit", for: UIControlState())
        btnForgotPassword.layer.cornerRadius = 5
        btnForgotPassword.rippleBackgroundColor = UIColor.clear
        btnForgotPassword.rippleColor = Color.code.bar
        btnForgotPassword.backgroundColor = Color.code.button
        btnForgotPassword.addTarget(self, action: #selector(ForgotPasswordViewController.forgotPasswordBtnAction), for: .touchUpInside)
        
        self.view.addSubview(txtUserName)
        self.view.addSubview(btnForgotPassword)
    }
    
    //MARK: Action Method Delegate
    
    func forgotPasswordBtnAction(){
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == txtUserName{
            forgotPasswordBtnAction()
        }
        return true
    }
    
}
