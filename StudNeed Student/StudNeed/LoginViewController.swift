//
//  LoginViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 13/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import Alamofire
var currentLoginUserDetais = NSDictionary()
class LoginViewController: UIViewController {

    let imagesBackBroundView = UIImageView()
    let txtUserName = AMInputView()
    let txtPassword = AMInputView()
    let btnLogin = ZFRippleButton()
    let btnForgotPassword = ZFRippleButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarDesign()
        designLoginView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func navigationBarDesign(){
        self.view.backgroundColor = Color.code.lightBar
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Login"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.view.backgroundColor = Color.code.lightBar
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
    }
    
    func designLoginView(){
        imagesBackBroundView.frame = self.view.bounds
        imagesBackBroundView.image = UIImage(named: "HostalDefaultImages.jpg")
        imagesBackBroundView.isBlurEffect()
        self.view.addSubview(imagesBackBroundView)
        
        
        let gap = self.view.frame.height/33
        txtUserName.frame = CGRect(x: gap, y: self.view.frame.height/3, width: self.view.frame.width - gap*2, height: gap*3)
        txtPassword.frame = CGRect(x: gap, y: txtUserName.frame.height + txtUserName.frame.origin.y + gap, width: self.view.frame.width - gap*2, height: gap*3)
        btnLogin.frame = CGRect(x: gap, y: txtPassword.frame.height + txtPassword.frame.origin.y + gap*2, width: self.view.frame.width - gap*2, height: gap*3)
        btnForgotPassword.frame = CGRect(x: gap, y: btnLogin.frame.height + btnLogin.frame.origin.y + gap*2, width: self.view.frame.width - gap*2, height: gap*3)
        
        txtUserName.placeholder = "Username"
        txtUserName.returnKeyType = .next
        txtUserName.isCheckValidation = .notNull
        
        txtPassword.placeholder = "Password"
        txtPassword.returnKeyType = .done
        txtPassword.isCheckValidation = .password
        
        btnLogin.setTitle("Login", for: UIControlState())
        btnLogin.layer.cornerRadius = 5
        btnLogin.addTarget(self, action: #selector(loginViewActionBtnAction), for: .touchUpInside)
        btnLogin.backgroundColor = Color.code.button
        btnLogin.rippleBackgroundColor = UIColor.clear
        btnLogin.rippleColor = Color.code.bar
        
        btnForgotPassword.setTitle("Forgot Password?", for: UIControlState())
        btnForgotPassword.rippleBackgroundColor = UIColor.clear
        btnForgotPassword.rippleColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        btnForgotPassword.addTarget(self, action: #selector(forgotPasswordBtnAction), for: .touchUpInside)
        
        self.view.addSubview(txtUserName)
        self.view.addSubview(txtPassword)
        self.view.addSubview(btnLogin)
        self.view.addSubview(btnForgotPassword)
    }
    
    //MARK: Action Method Delegate
    func loginViewActionBtnAction(){
        if txtUserName.isValidated && txtPassword.isValidated{
            let parameter : Parameters = [
                "password":txtPassword.text,
                "userName":txtUserName.text
            ]
            Alamofire.request(Web.url.userLogin, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                debugPrint(response)
                switch response.result{
                case .success(_):
                    let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    switch aDict.integer(forKey: Web.key.status){
                    case Web.statusCode.ok:
                        currentLoginUserDetais = aDict.dictionary(forKey: "userDetails")
                        UserDefaults.standard.set(currentLoginUserDetais, forKey: "isUserLoginDetails")
                        AppDelegate.shared.setRootViewController(viewController: HomeViewController())
                        break
                    case Web.statusCode.failed:
                        Nottification.message.error(body: aDict.string(forKey: Web.key.message), showButton: true)
                        break
                    default:
                        Nottification.message.error(body: "Login request failed")
                        break
                    }
                    break
                case .failure(_):
                    Nottification.message.error(body: "Failer")
                    break
                }
            }
        }
    }
    
    func forgotPasswordBtnAction(){
        self.navigationController?.pushViewController(ForgotPasswordViewController(), animated: true)
    }
}
