//
//  HomeViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 16/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,YSLDraggableCardContainerDelegate, YSLDraggableCardContainerDataSource {
    
    var container = YSLDraggableCardContainer()
    var bookData = [BookData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarDesign()
        self.view.backgroundColor = UIColor.RGB(r: 235, g: 235, b: 235)
        
        Main.webService.directoryType { response in
            if response {
                self.loadData()
                self.container.reload()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view = container
        container.backgroundColor = Color.code.lightBar
        container.dataSource = self
        container.delegate = self
        container.canDraggableDirection = [.down,.left,.right,.up]
    }
    
    func navigationBarDesign(){
        self.view.backgroundColor = Color.code.lightBar
        self.navigationItem.title = "Select Directory"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
    }
    
    func loadData() {
        for i in 0..<allDirectoryType.count{
            bookData.append(BookData(name: allDirectoryType.dictionary(at: i).string(forKey: Web.key.name), image: allDirectoryType.dictionary(at: i).string(forKey: Web.key.image), discription: allDirectoryType.dictionary(at: i).string(forKey: Web.key.discription)))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cardContainerViewNextView(with index: Int) -> UIView? {
        let view = CardView(frame: CGRect(x: 10, y: (self.view.frame.height - self.view.frame.size.width - 20)/2, width: self.view.frame.size.width - 20, height: self.view.frame.size.width - 20))
        view.backgroundColor = UIColor.white
        view.imageView?.setImage(imageNamed: bookData[index].image)
        view.label.text = "\(bookData[index].discription)"
        view.headerLabel.text = "   \(bookData[index].name)"
        return view
    }
    
    func cardContainerViewNumberOfView(in index: Int) -> Int {
        return bookData.count
    }
    // MARK: -- YSLDraggableCardContainer Delegate
    
    func cardContainerView(_ cardContainerView: YSLDraggableCardContainer, didEndDraggingAt index: Int, draggableView: UIView, draggableDirection: YSLDraggableDirection) {
        if draggableDirection == .left {
            cardContainerView.movePosition(with: draggableDirection, isAutomatic: false)
        }
        if draggableDirection == .right {
            cardContainerView.movePosition(with: draggableDirection, isAutomatic: false)
        }
        if draggableDirection == .up{
            cardContainerView.movePosition(with: draggableDirection, isAutomatic: false)
        }
        if draggableDirection == .down{
            cardContainerView.movePosition(with: draggableDirection, isAutomatic: false)
        }
    }
    
    func cardContainderView(_ cardContainderView: YSLDraggableCardContainer, updatePositionWithDraggableView draggableView: UIView, draggableDirection: YSLDraggableDirection, widthRatio: CGFloat, heightRatio: CGFloat) {
        let view: CardView? = (draggableView as? CardView)
        if draggableDirection == .down {
            view?.selectedView?.alpha = 0
        }
        if draggableDirection == .left {
            view?.selectedView?.backgroundColor = UIColor.RGB(r: 215, g: 104, b: 91)
            view?.selectedView?.alpha = widthRatio > 0.8 ? 0.8 : widthRatio
        }
        if draggableDirection == .right {
            view?.selectedView?.backgroundColor = UIColor.RGB(r: 114, g: 209, b: 142)
            view?.selectedView?.alpha = widthRatio > 0.8 ? 0.8 : widthRatio
        }
        if draggableDirection == .up {
            view?.selectedView?.backgroundColor = UIColor.RGB(r: 66, g: 172, b: 225)
            view?.selectedView?.alpha = heightRatio > 0.8 ? 0.8 : heightRatio
        }
    }
    
    func cardContainerViewDidCompleteAll(_ container: YSLDraggableCardContainer) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(0.3 * Double(NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {() -> Void in
            container.reload()
        })
    }
    
    func cardContainerView(_ cardContainerView: YSLDraggableCardContainer, didSelectAt index: Int, draggableView: UIView) {
        switch index {
        case 0,1:
            currentDirectory = .pg
            break
        case 2,3,4:
            currentDirectory = .school
            break
        case 5:
            currentDirectory = .book
            break
        default:
            break
        }
        
        let vc = DirectoryCartViewController()
        vc.viewTitle = allDirectoryType.dictionary(at: index).string(forKey: "name")
        vc.directoryId = allDirectoryType.dictionary(at: index).integer(forKey: "directoryId")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
