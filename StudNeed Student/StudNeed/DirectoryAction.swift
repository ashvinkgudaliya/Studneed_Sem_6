//
//  DirectoryAction.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 09/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import Alamofire

class DirectoryAction {

    static let webService = DirectoryAction()
    
    func like(venderDirectoryId:Any,userId:Any,like:Bool, compilation:@escaping (String,NSInteger) -> ()){
        let parameter : Parameters = [
            "venderDirectoryId" : venderDirectoryId,
            "userId" : userId,
            "like" : like
        ]
        
        Alamofire.request(Web.url.likeRattingWishlistAction, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    compilation(aDict.string(forKey: "liked"), aDict.integer(forKey: "likeCount"))
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Failer")
                break
            }
        }
    }

    func wishlist(venderDirectoryId:Any,userId:Any,wishlist:Bool, compilation:@escaping (NSInteger) -> ()){
        let parameter : Parameters = [
            "venderDirectoryId" : venderDirectoryId,
            "userId" : userId,
            "wishlist" : wishlist
        ]
        
        Alamofire.request(Web.url.likeRattingWishlistAction, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    compilation(aDict.integer(forKey: "wishlist"))
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Failer")
                break
            }
        }
    }
    
    func comment(venderDirectoryId:Any,userId:Any,comment:String, compilation:@escaping (NSInteger) -> ()){
        let parameter : Parameters = [
            "venderDirectoryId" : venderDirectoryId,
            "userId" : userId,
            "comment" : comment
        ]
        
        Alamofire.request(Web.url.addComments, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    compilation(aDict.integer(forKey: "commentCount"))
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Failer")
                break
            }
        }
    }
    
    func rate(venderDirectoryId:Any,userId:Any,ratting:Any, compilation:@escaping (NSDictionary,NSInteger) -> ()){
        let parameter : Parameters = [
            "venderDirectoryId" : venderDirectoryId,
            "userId" : userId,
            "ratting" : ratting
        ]
        
        Alamofire.request(Web.url.likeRattingWishlistAction, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    compilation(aDict.dictionary(forKey: "ratting"), aDict.integer(forKey: "rating"))
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Failer")
                break
            }
        }
    }
}
