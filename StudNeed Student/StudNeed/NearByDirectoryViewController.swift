//
//  NearByDirectoryViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 04/04/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class NearByDirectoryViewController: UIViewController ,MKMapViewDelegate,CLLocationManagerDelegate{
    
    var mapView = MKMapView()
    var locationManager = CLLocationManager()
    var directoryPin : [MapLocationData] = []
    var displayData = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView = MKMapView(frame: view.bounds)
        mapView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        mapView.delegate = self
        view.addSubview(mapView)
        
        if locationManager.responds(to:#selector(requestAlwaysAuthorization)) {
            locationManager.requestAlwaysAuthorization()
        }
        
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
        
        mapView.showsUserLocation = true
        mapView.delegate = self
        locationManager.delegate = self
        
        navigationBarDesign()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addNotation() {
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotations(annotations())
        
        mapView.showAnnotations(mapView.annotations, animated: true)
    }
    
    func navigationBarDesign(){
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.title = "Near By"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
    }
    
    func annotations() -> [JPSThumbnailAnnotation] {
        
        var annotation : [JPSThumbnailAnnotation] = []
        
        for i in 0..<directoryPin.count{
            let directoryLocation = JPSThumbnail()
            directoryLocation.image = ImagesPath + directoryPin[i].image
            directoryLocation.title = directoryPin[i].title
            directoryLocation.subtitle = directoryPin[i].locality
            directoryLocation.coordinate = directoryPin[i].coordinate
            directoryLocation.disclosureBlock = { title in
                for i in 0..<self.directoryPin.count{
                    if title == self.directoryPin[i].title {
                        switch currentDirectory {
                        case .pg:
                            let vc = PgDetailsViewController(nibName: "PgDetailsViewController", bundle: nil)
                            vc.venderDirectoryId = self.displayData.dictionary(at: i).string(forKey: "venderDirectoryId")
                            self.navigationController?.pushViewController(vc, animated: true)
                            break
                        case .school:
                            let vc = ClassDeatilsViewController(nibName: "ClassDeatilsViewController", bundle: nil)
                            vc.venderDirectoryId = self.displayData.dictionary(at: i).string(forKey: "venderDirectoryId")
                            self.navigationController?.pushViewController(vc, animated: true)
                            break
                        case .book:
                            let vc = BooksDeatilsViewController(nibName: "BooksDeatilsViewController", bundle: nil)
                            vc.venderDirectoryId = self.displayData.dictionary(at: i).string(forKey: "venderDirectoryId")
                            self.navigationController?.pushViewController(vc, animated: true)
                            break
                        }
                    }
                }
            }
            annotation.append(JPSThumbnailAnnotation(thumbnail: directoryLocation))
        }
        
        return annotation
    }
    
    // MARK: - MKMapViewDelegate
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view is JPSThumbnailAnnotationViewProtocol {
            (view as? JPSThumbnailAnnotationViewProtocol)?.didSelectAnnotationView(inMap: mapView)
        }
    }
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view is JPSThumbnailAnnotationViewProtocol {
            (view as? JPSThumbnailAnnotationViewProtocol)?.didDeselectAnnotationView(inMap: mapView)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is JPSThumbnailAnnotationProtocol {
            return ((annotation as? JPSThumbnailAnnotationProtocol)?.annotationView(inMap: mapView)!)!
        }
        
        guard !annotation.isKind(of: MKUserLocation.classForCoder()) else { return nil }
        return AnnotationView(annotation: annotation, reuseIdentifier: "PulsatorDemoAnnotation")
    }
    
    //MARK : UserLocation Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        requestAlwaysAuthorization()
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        let region: MKCoordinateRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800)
        self.mapView.setRegion(self.mapView.regionThatFits(region), animated: true)
        
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)) { (placemark, error) in
            let placemark :CLPlacemark  = (placemark?.last)!
            let point = MKPointAnnotation()
            point.coordinate = userLocation.coordinate
            point.title = "I am Here"
            point.subtitle = placemark.locality
            mapView.addAnnotation(point)
            self.locationManager.stopUpdatingLocation()
            
            for i in 0..<self.displayData.count{
                self.directoryPin.append(MapLocationData(image: self.displayData.dictionary(at: i).string(forKey: "images"), title: self.displayData.dictionary(at: i).string(forKey: "name").capitalized, locality: self.displayData.dictionary(at: i).string(forKey: "locality"), latitude: CGFloat(self.displayData.dictionary(at: i).float(forKey: "latitute")), longitude: CGFloat(self.displayData.dictionary(at: i).float(forKey: "longitute"))))
            }
            
            Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.addNotation), userInfo: nil, repeats: false)
        }
    }
    
    func requestAlwaysAuthorization()
    {
        let status : CLAuthorizationStatus = CLLocationManager.authorizationStatus()
        if status == .authorizedWhenInUse || status == .denied {
            //            let title:String = (status == .denied) ? "Location services are off" : "Background location is not enabled"
            //            let message:String = "To use background location you must turn on 'Always' in the Location Services Settings"
        }
        else if status == .denied {
            locationManager.requestAlwaysAuthorization()
        }
    }
}
