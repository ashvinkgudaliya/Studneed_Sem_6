//
//  TableViewCell.swift
//  Kitchry
//
//  Created by HiddenMind on 28/03/17.
//  Copyright © 2017 Daniel Kim. All rights reserved.
//

import UIKit

extension NSDictionary{
    func string(forKey:String) -> String{
        if self.value(forKey: forKey) != nil{
            if let temp = self.value(forKey: forKey) as? String{
                return temp
            }
            else if let temp = self.value(forKey: forKey) as? NSString{
                return temp as String
            }
//            else if let temp = (self.value(forKey: forKey) as AnyObject).stringValue {
//                return temp
//            }
            else{
                return "\(self.value(forKey: forKey)!)"
            }
        }
        else{
            return String()
        }
    }
    
    func integer(forKey:String) -> NSInteger{
        if self.value(forKey: forKey) != nil{
            if let temp = self.value(forKey: forKey) as? NSInteger {
                return temp
            }
            else if let temp = (self.value(forKey: forKey) as AnyObject).integerValue{
                return temp
            }
            else if let temp = (self.value(forKey: forKey) as? NSString)?.intValue{
                return NSInteger(temp)
            }
            else{
                return NSInteger()
            }
        }
        else{
            return NSInteger()
        }
    }
    
    func float(forKey:String) -> Float{
        if self.value(forKey: forKey) != nil{
            if let temp = self.value(forKey: forKey) as? Float{
                return temp
            }
            else if let temp = self.value(forKey: forKey) as? CGFloat{
                return Float(temp)
            }
            else if let temp = (self.value(forKey: forKey) as AnyObject).floatValue{
                return temp
            }
            else {
                return Float()
            }
        }
        else{
            return Float()
        }
    }
    
    func bool(forKey:String) -> Bool{
        if self.value(forKey: forKey) != nil{
            if let temp = (self.value(forKey: forKey) as? NSString)?.boolValue{
                return temp
            }
            else if let temp = (self.value(forKey: forKey) as? Bool){
                return temp
            }
            else {
                return Bool()
            }
        }
        else{
            return Bool()
        }
    }
    
    func dictionary(forKey:String) -> NSDictionary{
        if self.value(forKey: forKey) != nil{
            if let temp = self.value(forKey: forKey) as? NSDictionary{
                return temp
            }
            else if let temp = (self.value(forKey: forKey) as AnyObject) as? NSDictionary {
                return temp
            }
            else{
                return NSDictionary()
            }
        }
        else{
            return NSDictionary()
        }
    }
    
    func array(forKey:String) -> NSArray{
        if self.value(forKey: forKey) != nil{
            if let temp = self.value(forKey: forKey) as? NSArray{
                return temp
            }
            else{
                return NSArray()
            }
        }
        else{
            return NSArray()
        }
    }
    
    func mutableArray(forKey:String) -> NSMutableArray{
        if self.value(forKey: forKey) != nil{
            if let temp = self.value(forKey: forKey) as? NSMutableArray{
                return temp
            }
            else if let temp = (self.value(forKey: forKey) as AnyObject) as? NSMutableArray{
                return temp
            }
            else{
                return NSMutableArray()
            }
        }
        else{
            return NSMutableArray()
        }
    }
}

extension NSArray{
    func dictionary(at:NSInteger) -> NSDictionary{
        return self.object(at: at) as! NSDictionary
    }
}
