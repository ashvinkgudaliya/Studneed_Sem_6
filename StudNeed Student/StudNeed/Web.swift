//
//  Web.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 20/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
let DomainName = "http://localhost/StudNeed/"
let ImagesPath = "\(DomainName)images/"

class Web {
    
    class url {
        static let userLogin = "\(DomainName)Login/user/login.php"
        static let registration = "\(DomainName)Login/user/registration.php"
        static let directoryType = "\(DomainName)Category/directoryType.php"
        static let getCategory = "\(DomainName)Category/getCategory.php"
        static let directoryData = "\(DomainName)Details/venderDirectoryData.php"
        static let likeRattingWishlistAction = "\(DomainName)Details/likeRattingWishlistAction.php"
        static let addComments = "\(DomainName)Details/addComments.php"
        static let venderFullDetails = "\(DomainName)profile/vender/venderFullDetails.php"
    }
    
    class key {
        static let status = "status"
        static let directoryType = "directoryType"
        static let discription = "discription"
        static let name = "name"
        static let image = "image"
        static let category = "category"
        static let message = "message"
    }
    
    class statusCode {
        static let ok = 200
        static let failed = 401
    }
    
}


