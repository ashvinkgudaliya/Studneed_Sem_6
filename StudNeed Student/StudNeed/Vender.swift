//
//  Vender.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 11/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import Alamofire

class Vender {

    static let webService = Vender()

    func getVender(venderId:Any,userId:Any ,compilation:@escaping (NSDictionary) -> ()){
        let parameter : Parameters = [
            "venderId" : venderId,
            "userId" : userId
        ]
        
        Alamofire.request(Web.url.venderFullDetails, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    compilation(aDict.dictionary(forKey: "venderFullDetails"))
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Failer")
                break
            }
        }
    }
}
