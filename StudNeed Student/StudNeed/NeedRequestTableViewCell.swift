//
//  NeedRequestTableViewCell.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 07/04/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class NeedRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var backViewGround: UIView!
    @IBOutlet weak var directoryImages: UIImageView!
    @IBOutlet weak var directoryName: UILabel!
    @IBOutlet weak var directoryLocation: UILabel!
    @IBOutlet weak var viewLbl: UILabel!
    @IBOutlet weak var likesLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backViewGround.shadowEnable()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func assignValue(data:NSDictionary){
        directoryImages.setImage(imageNamed: data.string(forKey: "images"))
        directoryName.text = data.string(forKey: "name")
        directoryLocation.text = data.string(forKey: "city")
        viewLbl.text = data.string(forKey: "views")
        commentLbl.text = data.string(forKey: "comment")
        likesLbl.text = data.string(forKey: "likes")
    }
    
}
