//
//  MapLocationData.swift
//  mapWithImages
//
//  Created by HiddenMind on 03/04/17.
//  Copyright © 2017 HiddenMind. All rights reserved.
//

import UIKit
import CoreLocation

class MapLocationData{
    var image = ""
    var title = ""
    var locality = ""
    var coordinate = CLLocationCoordinate2D()
    
    init(image:String,title:String,locality:String,latitude:CGFloat,longitude:CGFloat){
        self.image = image
        self.title = title
        self.locality = locality
        self.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
    }
}

class AnnotationView: MKAnnotationView {
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        addHalo()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addHalo() {
        let pulsator = Pulsator()
        pulsator.position = center
        pulsator.numPulse = 4
        pulsator.radius = 100
        pulsator.animationDuration = 4.5
        pulsator.backgroundColor = UIColor(red: 0, green: 0.455, blue: 0.756, alpha: 1).cgColor
        layer.addSublayer(pulsator)
        pulsator.start()
    }
}
