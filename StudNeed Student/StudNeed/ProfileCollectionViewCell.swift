//
//  ProfileCollectionViewCell.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 06/04/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var directoryImages: UIImageView!
    @IBOutlet weak var directoryNameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var viewsLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var likesLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func assignDirectoryData(data: NSDictionary){
        
        viewBack.shadowEnable()
        directoryImages.setImage(imageNamed: data.dictionary(forKey: "directoryData").array(forKey: "images").dictionary(at: 0).string(forKey: "name"))
        directoryNameLbl.text = data.dictionary(forKey: "directoryData").string(forKey: "name")
        viewsLbl.text = data.dictionary(forKey: "directoryData").string(forKey: "views")
        likesLbl.text = data.dictionary(forKey: "directoryAction").string(forKey: "likeCount")
        ratingLbl.text = data.dictionary(forKey: "directoryAction").string(forKey: "rating")
    }

}
