//
//  NeedRequestViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 07/04/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class NeedRequestViewController: UITableViewController {
    var directoryDetailsArr = NSArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "NeedRequestTableViewCell", bundle: nil), forCellReuseIdentifier: "needRequestTableViewCell")
        tableView.estimatedRowHeight = 20
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        navigationBarDesign()
        
        WebServices.data.displayDirectoryData(directoryId: 2, compilation:{ response in
            self.directoryDetailsArr = response
            self.tableView.reloadData()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func navigationBarDesign(){
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.title = "My Dictonary"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
        self.view.backgroundColor = Color.code.lightBar
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return directoryDetailsArr.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let iNeedTableViewCell = tableView.dequeueReusableCell(withIdentifier: "needRequestTableViewCell", for: indexPath) as! NeedRequestTableViewCell
        iNeedTableViewCell.backgroundColor = Color.code.lightBar
        iNeedTableViewCell.assignValue(data:directoryDetailsArr.dictionary(at: indexPath.row))
        return iNeedTableViewCell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
