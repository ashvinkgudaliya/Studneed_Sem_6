//
//  ProfileViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 05/04/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource{
    
    @IBOutlet weak var profileImageBackGround: UIImageView!
    @IBOutlet weak var profileImagesView: UIImageView!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    var displayData = NSDictionary()
    @IBOutlet weak var emailIdLbl: UILabel!
    @IBOutlet weak var mobileNoLbl: UILabel!
    @IBOutlet weak var venderName: UILabel!
    @IBOutlet weak var venderLocaion: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        self.view.backgroundColor = Color.code.lightBar
    
        collectionView.register(UINib(nibName: "ProfileCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProfileCollectionViewCell")

        Vender.webService.getVender(venderId: displayData.string(forKey: "venderId"),userId: currentLoginUserDetais.string(forKey: "userId")) { data in
            self.displayData = data
            self.Configuration()
            self.collectionView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Configuration(){
        profileImageBackGround.setImage(imageNamed: displayData.string(forKey: "proHeader"))
        profileImagesView.setImage(imageNamed: displayData.string(forKey: "profilePic"))
        venderName.text = displayData.string(forKey: "venderName")
        venderLocaion.text = displayData.string(forKey: "city")
        mobileNoLbl.text = displayData.string(forKey: "mobile")
        emailIdLbl.text = displayData.string(forKey: "email")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        flowLayout.itemSize = CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        flowLayout.minimumLineSpacing = 0
        profileImageBackGround.isBlurEffect()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return displayData.array(forKey: "venderTypeDetails").count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCollectionViewCell", for: indexPath) as! ProfileCollectionViewCell
        cell.assignDirectoryData(data: displayData.array(forKey: "venderTypeDetails").dictionary(at: indexPath.row))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch displayData.array(forKey: "venderTypeDetails").dictionary(at: indexPath.row).integer(forKey: "directoryId"){
        case 2,3:
            let vc = PgDetailsViewController(nibName: "PgDetailsViewController", bundle: nil)
            vc.venderDirectoryId = displayData.array(forKey: "venderTypeDetails").dictionary(at: indexPath.row).string(forKey: "venderDirectoryId")
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 4,5,6:
            let vc = ClassDeatilsViewController(nibName: "ClassDeatilsViewController", bundle: nil)
            vc.venderDirectoryId = displayData.array(forKey: "venderTypeDetails").dictionary(at: indexPath.row).string(forKey: "venderDirectoryId")
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 7:
            let vc = BooksDeatilsViewController(nibName: "BooksDeatilsViewController", bundle: nil)
            vc.venderDirectoryId = displayData.array(forKey: "venderTypeDetails").dictionary(at: indexPath.row).string(forKey: "venderDirectoryId")
            self.navigationController?.pushViewController(vc, animated: true)
            break
        default:
            break
        }
        
        switch currentDirectory {
        case .pg:
            
            break
        case .school:
            
            break
        case .book:
            
            break
        }
    }
}

extension UIView{
    func gradientColor(with:[CGColor]){
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = with
        self.layer.insertSublayer(gradient, at: 0)
    }
}
