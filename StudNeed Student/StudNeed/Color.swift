//
//  Color.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 15/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class Color {

    static let code = Color()
    
    let bar = UIColor(hex: 0x75C2FF)
    let button = UIColor(hex: 0x007ee6)
    let lightBar = UIColor(hex: 0xe6f4ff)
    let navigationBar = UIColor(hex: 0x184b98) //UIColor(red: 24/255, green: 75/255, blue: 152/255, alpha: 1)
}
