//
//  BookTableViewCell.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 28/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class BookTableViewCell: UITableViewCell {

    @IBOutlet weak var booksImageView: UIImageView!
    @IBOutlet weak var backgroundImagesView: UIImageView!
    
    @IBOutlet weak var ratingView: EDStarRating!
    @IBOutlet weak var ratingCountLbl: UILabel!
    
    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var venderNameLbl: UILabel!
    @IBOutlet weak var shopStatusLbl: UILabel!
    @IBOutlet weak var likeCountBtn: UIButton!
    @IBOutlet weak var commentCountBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        booksImageView.isAssign3DEffect()
        backgroundImagesView.setImage(imageNamed: currentOpenCategoryImages)
        backgroundImagesView.isBlurEffect()
        
        likeCountBtn.setImage(UIImage.Asset.like.image, for: .normal)
        commentCountBtn.setImage(UIImage.Asset.comment.image, for: .normal)
        

        ratingView.starImage = UIImage(named: "starDesabled.png")
        ratingView.starHighlightedImage = UIImage(named:"starSelected.png")
        ratingView.maxRating = 5
        ratingView.horizontalMargin = 5
        ratingView.editable = false
        ratingView.displayMode = UInt(EDStarRatingDisplayHalf)
        ratingView.setNeedsDisplay()
        ratingView.tintColor = UIColor.yellow
        
        ratingCountLbl.textAlignment = .center

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func assignValue(data:NSDictionary){
        storeNameLbl.text = data.string(forKey: "storeName")
//        venderNameLbl.text = data.string(forKey: "")
        booksImageView.setImage(imageNamed: data.string(forKey: "images"))
        likeCountBtn.setTitle("\(data.integer(forKey: "likes"))", for: .normal)
        commentCountBtn.setTitle("\(data.integer(forKey: "comment"))", for: .normal)
        ratingView.rating = data.float(forKey: "rating")
        ratingCountLbl.text = String(format: "%.1f",data.float(forKey: "rating"))
    }
    
}
