//
//  CollegeTableViewCell.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 01/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class CollegeTableViewCell: UITableViewCell {

    @IBOutlet weak var backGroundImages: UIImageView!
    @IBOutlet weak var ratingView: EDStarRating!
    @IBOutlet weak var directoryImages: UIImageView!
    @IBOutlet weak var directoryName: UILabel!
    @IBOutlet weak var likesCountBtn: UIButton!
    @IBOutlet weak var venderName: UILabel!
    @IBOutlet weak var commentCount: UIButton!
    @IBOutlet weak var ratingCountLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backGroundImages.shadowEnable()
        backGroundImages.setImage(imageNamed: currentOpenCategoryImages)
        backGroundImages.isBlurEffect()
        
        likesCountBtn.setImage(UIImage.Asset.like.image, for: .normal)
        commentCount.setImage(UIImage.Asset.comment.image, for: .normal)
        
        ratingView.starImage = UIImage(named: "starDesabled.png")
        ratingView.starHighlightedImage = UIImage(named:"starSelected.png")
        ratingView.maxRating = 5
        ratingView.editable = false
        ratingView.horizontalMargin = 5
        ratingView.displayMode = UInt(EDStarRatingDisplayHalf)
        ratingView.setNeedsDisplay()
        ratingView.tintColor = UIColor.yellow
        ratingCountLbl.textAlignment = .center
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func assignValue(data:NSDictionary)  {
//                venderName.text = data.string(forKey: "")
        directoryName.text = data.string(forKey: "name")
        likesCountBtn.setTitle("\(data.integer(forKey: "likes"))", for: .normal)
        commentCount.setTitle("\(data.integer(forKey: "comment"))", for: .normal)
        directoryImages.setImage(imageNamed: data.string(forKey: "images"))
        ratingView.rating = data.float(forKey: "rating")
        ratingCountLbl.text = String(format: "%.1f",data.float(forKey: "rating"))
    }
}
