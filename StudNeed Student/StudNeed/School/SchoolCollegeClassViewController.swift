//
//  SchoolCollegeClassViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 01/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class SchoolCollegeClassViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{

    let schCollegeClsTableView = UITableView()
    var directoryId = NSInteger()
    var viewTitle = String()
    var allClassSchollCollege = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.code.lightBar
        
        WebServices.data.displayDirectoryData(directoryId: directoryId, compilation:{ response in
            self.allClassSchollCollege = response
            self.schCollegeClsTableView.reloadData()
        })
        navigationBarDesign()
        viewControllerDesign()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func navigationBarDesign(){
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = viewTitle
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
    }
    
    func viewControllerDesign(){
        schCollegeClsTableView.frame = CGRect(x: 0, y:  5, width: self.view.frame.width, height: self.view.frame.height - 5)
        schCollegeClsTableView.delegate = self
        schCollegeClsTableView.showsHorizontalScrollIndicator = false
        schCollegeClsTableView.showsVerticalScrollIndicator = false
        schCollegeClsTableView.separatorStyle = .none
        schCollegeClsTableView.backgroundColor = Color.code.lightBar
        schCollegeClsTableView.dataSource = self
        schCollegeClsTableView.register(UINib(nibName: "CollegeTableViewCell", bundle: nil), forCellReuseIdentifier: "collegeCell")
        self.view.addSubview(schCollegeClsTableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allClassSchollCollege.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let collegeCell = tableView.dequeueReusableCell(withIdentifier: "collegeCell", for: indexPath) as! CollegeTableViewCell
        collegeCell.selectionStyle = .none
        collegeCell.backgroundColor = Color.code.lightBar
        collegeCell.assignValue(data: allClassSchollCollege.dictionary(at: indexPath.row))
        return collegeCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return schCollegeClsTableView.frame.height/3
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailsViewController()
        currentDirectory = .school
        vc.venderDirectoryId = allClassSchollCollege.dictionary(at: indexPath.row).string(forKey: "venderDirectoryId")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func backButtonAction(){
        self.navigationController!.popViewController(animated: true)
    }

}
