//
//  CreateNeedViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 11/04/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class CreateNeedViewController: UIViewController {

    @IBOutlet weak var submitButton: CustomButton!
    @IBOutlet weak var menu: KPDropMenu!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = Color.code.lightBar
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.title = "Create Need"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
        
        submitButton.backgroundColor = Color.code.button
        var data : [String] = []
        for i in 0..<allDirectoryType.count{
            data.append(allDirectoryType.dictionary(at: i).string(forKey: Web.key.name))
        }
        menu.items = data
        menu.itemsFont = UIFont(name: "Helvetica-Regular", size: CGFloat(12.0))
        menu.titleTextAlignment = .center
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
