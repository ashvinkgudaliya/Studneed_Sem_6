//
//  AppDelegate.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 13/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigation = UINavigationController()
    static let shared = UIApplication.shared.delegate as! AppDelegate

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        
        if UserDefaults.standard.bool(forKey: "isSeconTimeLoad"){
            if UserDefaults.standard.object(forKey: "isUserLoginDetails") != nil{
                currentLoginUserDetais = UserDefaults.standard.object(forKey: "isUserLoginDetails") as! NSDictionary
                setRootViewController(viewController: HomeViewController())
            }
            else{
                setRootViewController(viewController: LoginAndSingUpViewController())
            }
        }
        else{
            setRootViewController(viewController: FristScreenViewController())
            AGSideMenu.isSwipeGestureEnabled = false
        }

        return true
    }
    
    func setRootViewController(viewController:UIViewController){
        navigation = UINavigationController(rootViewController: viewController)
        navigation.navigationBar.tintColor = Color.code.bar
        window?.rootViewController = navigation
        window?.makeKeyAndVisible()
        window?.addSubview(AGSideMenu)
        AGSideMenu.isSwipeGestureEnabled = true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

