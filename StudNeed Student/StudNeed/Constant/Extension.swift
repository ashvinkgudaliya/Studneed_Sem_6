//
//  Extension.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 15/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(hex:Int, alpha:CGFloat = 1.0) {
        self.init(
            red:   CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8)  / 255.0,
            blue:  CGFloat((hex & 0x0000FF) >> 0)  / 255.0,
            alpha: alpha
        )
    }
    
    class func RGB(r:CGFloat,g:CGFloat,b:CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1.0)
    }
}

extension UIView{
    func isBlurEffect(){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.alpha = 0.5
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
    
    func isFullBlurEffect(style: UIBlurEffectStyle = .dark){
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
    
    func shadowEnable(height:NSInteger = 2,width:NSInteger = 0){
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowOpacity = 0.6
    }
    
    func isAssign3DEffect(){
        self.layer.shadowOffset = CGSize(width:10 ,height:10)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.6
        self.layer.masksToBounds = false
    }
    
    func rounding(ByCorners:UIRectCorner,Radii:CGFloat){
        let maskPath = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: ByCorners,
                                    cornerRadii: CGSize(width: Radii, height: Radii))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        self.layer.mask = shape
    }
}

extension UIImageView{
    func setImage(imageNamed:String){
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                let url =  URL(string: "\(ImagesPath)\(imageNamed)")
                self.sd_setImage(with: url, placeholderImage: Images.name.lodding, options: SDWebImageOptions.refreshCached, completed: { (image, error, cache, url) in
                })
            }
        }
    }
}

extension String{
    func heightForString(font:CGFloat, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont.systemFont(ofSize: font)
        label.text = self
        label.sizeToFit()
        return label.frame.height
    }
}

extension UIViewController {
    func backButtonAction(){
        self.navigationController!.popViewController(animated: true)
    }
}

