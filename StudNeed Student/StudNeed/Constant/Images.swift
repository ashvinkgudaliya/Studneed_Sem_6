//
//  Images.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 15/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class Images {
    static let name = Images()
    
    let lodding = UIImage(named: "placeHolder.png")
    
    let back = UIImage(named: "leftArrow.png")
    let menu = UIImage(named: "menu.png")

}

extension UIImage {
    enum Asset: String {
        case Banks = "Banks"
        case Hotels = "Hotels"
        case Key = "Key"
        case Shopping_Cart = "Shopping-cart"
        case Stores = "Stores"
        case Wallet = "Wallet"
        case book = "book"
        case like = "like.png"
        case comment = "comment.png"
        
        var image: UIImage {
            return UIImage(asset: self)
        }
    }
    
    convenience init!(asset: Asset) {
        self.init(named: asset.rawValue)
    }
}



enum Directory{
    case pg,school,book
}

var currentDirectory : Directory = .book
