//
//  CLabsImageSlider.swift
//  Pods
//
//  Created by apple on 18/09/16.
//
//

import UIKit


@objc public protocol imageSliderDelegate {
    func didMovedToIndex(index:Int)
}

public class CLabsImageSlider:UIView
{
    public enum slideCase
    {
        case ManualSwipe
        case Automatic(timeIntervalinSeconds:Double)
    }

    var imageSourceArray : [String] = []
    var isAnimating =   false
    var isGestureEnabled    =   Bool()
    var visibleImageView    =   UIImageView()
    var imageView1  =   UIImageView()
    var imageView2  =   UIImageView()
    var imageView3  =   UIImageView()
    var currentIndex    =   Int()
    var placeHolderImage    =   UIImage()
    let pageControl = DDPageControl()
    
    public weak var sliderDelegate  :   imageSliderDelegate?
    
    var rightFrame  :CGRect
    {
        return CGRect(x:self.frame.size.width, y:0, width:self.frame.size.width, height:self.frame.size.height)
    }
    
    var midFrame    :   CGRect
    {
        return CGRect(x:0, y:0,width: self.frame.size.width,height: self.frame.size.height)
    }
    
    var leftFrame   :   CGRect
    {
        return CGRect(x:-self.frame.size.width,y:0, width:self.frame.size.width, height:self.frame.size.height)
    }
    
    
    var leftSwipe : UISwipeGestureRecognizer
    {
        let leftSwipe   =   UISwipeGestureRecognizer(target: self, action: #selector(CLabsImageSlider.swipeRight(swipeGesture:)))
        leftSwipe.direction =   .left
        return leftSwipe
    }
    
    var rightSwipe : UISwipeGestureRecognizer
    {
        
        return UISwipeGestureRecognizer(target: self, action: #selector(CLabsImageSlider.swipeLeft(swipeGesture:)))
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    func addPageController(numberOfPages:NSInteger){
        pageControl.center = CGPoint(x: CGFloat(self.center.x), y: CGFloat(self.bounds.size.height - 30.0))
        pageControl.numberOfPages = numberOfPages
        pageControl.currentPage = 0
//        pageControl.addTarget(self, action: #selector(self.pageControlClicked), for: .valueChanged)
        pageControl.defersCurrentPageDisplay = true
        pageControl.type = DDPageControlTypeOnFullOffEmpty
        pageControl.onColor = UIColor(white: CGFloat(0.9), alpha: CGFloat(1.0))
        pageControl.offColor = UIColor(white: CGFloat(0.7), alpha: CGFloat(1.0))
        pageControl.indicatorDiameter = 15.0
        pageControl.indicatorSpace = 15.0
        pageControl.layer.zPosition = 1
        self.addSubview(pageControl)
    }
    
    public func setUpView(imageSource:[String],slideType:slideCase,isArrowBtnEnabled:Bool)
    {
        addPageController(numberOfPages: imageSource.count)
        imageSourceArray = imageSource
        switch slideType{
        case .Automatic(let timer):
            isGestureEnabled = true
            Timer.scheduledTimer(timeInterval: timer, target: self, selector: #selector(CLabsImageSlider.moveRight), userInfo: nil, repeats: true)
            
        case .ManualSwipe:
            isGestureEnabled = true
            if imageSourceArray.count>1
            {
                imageView2.addGestureRecognizer(leftSwipe)
                imageView2.addGestureRecognizer(rightSwipe)
            }
        }
        
        imageView1.frame   =   leftFrame
        imageView1.tag  =   1
        imageView1.isUserInteractionEnabled   =  true
        imageView1.contentMode  =   .scaleToFill
        self.addSubview(imageView1)
        
        imageView2.frame   =   midFrame
        imageView2.tag  =   2
        imageView2.contentMode  =   .scaleToFill
        imageView2.isUserInteractionEnabled   =  true
        visibleImageView    =   imageView2
        
        if imageSourceArray.count > 0
        {
            imageView2.setImage(imageNamed: getImage(index: 0))
        }
        else{
            imageView2.image = UIImage(named: "noImages.jpeg")
        }
        self.addSubview(imageView2)
        
        
        imageView3.frame    =   rightFrame
        imageView3.tag  =   3
        imageView3.contentMode  =   .scaleToFill
        imageView3.isUserInteractionEnabled   =  true
        self.addSubview(imageView3)
        
        
        if imageSourceArray.count>1{
            if isArrowBtnEnabled{
                enableArrows()
            }
        }
        
    }
    
    
    @objc  func moveRight()
    {
        if !isAnimating{
            currentIndex    =   currentIndex    +   1
            if currentIndex ==   imageSourceArray.count{currentIndex=0}
            visibleImageView.removeGestureRecognizer(leftSwipe)
            visibleImageView.removeGestureRecognizer(rightSwipe)
            swipeRightAnimate(condition: visibleImageView.tag)
            pageControl.currentPage = currentIndex
            pageControl.updateCurrentPageDisplay()
            self.sliderDelegate?.didMovedToIndex(index: currentIndex)
        }
    }
    
    
    @objc  func moveLeft()
    {
        if !isAnimating{
            currentIndex    =   currentIndex    -   1
            if currentIndex < 0{currentIndex    =   imageSourceArray.count    -   1}
            visibleImageView.removeGestureRecognizer(leftSwipe)
            visibleImageView.removeGestureRecognizer(rightSwipe)
            swipeLeftAnimate(condition: visibleImageView.tag)
            pageControl.currentPage = currentIndex
            pageControl.updateCurrentPageDisplay()
            self.sliderDelegate?.didMovedToIndex(index: currentIndex)
        }
    }
    
    @objc  func swipeLeft(swipeGesture:UISwipeGestureRecognizer)
    {
        moveLeft()
    }
    
    func swipeLeftAnimate(condition:Int)
    {
        isAnimating=true
        switch condition {
        case 1:
            self.visibleImageView    =   self.imageView3
            imageView3.setImage(imageNamed: getImage(index: currentIndex))
            UIView.animate(withDuration: 0.5, animations: {
                self.imageView2.isHidden  =   true
                self.imageView1.frame    =   self.rightFrame
                self.imageView2.frame    =   self.leftFrame
                self.imageView3.frame    =   self.midFrame
                
            }, completion: {[unowned self](finished:Bool) -> Void in
                
                if self.isGestureEnabled{
                    self.imageView3.addGestureRecognizer(self.leftSwipe)
                    self.imageView3.addGestureRecognizer(self.rightSwipe)
                }
                self.imageView2.isHidden  =   false
                self.isAnimating=false
            })
            
        case 2:
            self.visibleImageView    =   self.imageView1
            imageView1.setImage(imageNamed: getImage(index: currentIndex))
            UIView.animate(withDuration: 0.5, animations: {
                self.imageView3.isHidden  =   true
                self.imageView1.frame    =   self.midFrame
                self.imageView2.frame    =   self.rightFrame
                self.imageView3.frame    =   self.leftFrame
                
            }, completion: {[unowned self](finished:Bool) -> Void in
                if self.isGestureEnabled{
                    self.imageView1.addGestureRecognizer(self.leftSwipe)
                    self.imageView1.addGestureRecognizer(self.rightSwipe)
                }
                self.imageView3.isHidden  =   false
                self.isAnimating=false
            })
            
        case 3:
            self.visibleImageView    =   self.imageView2
            imageView2.setImage(imageNamed: getImage(index: currentIndex))
            UIView.animate(withDuration: 0.5, animations: {
                self.imageView1.isHidden  =   true
                self.imageView1.frame    =   self.leftFrame
                self.imageView2.frame    =   self.midFrame
                self.imageView3.frame    =   self.rightFrame
                
            }, completion: {[unowned self](finished:Bool) -> Void in
                if self.isGestureEnabled{
                    self.imageView2.addGestureRecognizer(self.leftSwipe)
                    self.imageView2.addGestureRecognizer(self.rightSwipe)
                }
                self.imageView1.isHidden  =   false
                self.isAnimating=false
            })
            
        default:
            break
        }
    }
    
    @objc  func swipeRight(swipeGesture:UISwipeGestureRecognizer)
    {
        moveRight()
    }
    
    
    
    func swipeRightAnimate(condition:Int){
        isAnimating=true
        switch condition {
        case 1:
            self.visibleImageView    =   self.imageView2
            imageView2.setImage(imageNamed: getImage(index: currentIndex))
            UIView.animate(withDuration: 0.5, animations: {
                self.imageView3.isHidden  =   true
                self.imageView1.frame    =   self.leftFrame
                self.imageView2.frame    =   self.midFrame
                self.imageView3.frame    =   self.rightFrame
                
            }, completion: {[unowned self](finished:Bool) -> Void in
                if self.isGestureEnabled{
                    self.imageView2.addGestureRecognizer(self.leftSwipe)
                    self.imageView2.addGestureRecognizer(self.rightSwipe)
                }
                self.imageView3.isHidden  =   false
                self.isAnimating=false
            })
            
        case 2:
            self.visibleImageView    =   self.imageView3
            imageView3.setImage(imageNamed: getImage(index: currentIndex))
            UIView.animate(withDuration: 0.5, animations: {
                self.imageView1.isHidden  =   true
                self.imageView1.frame    =   self.rightFrame
                self.imageView2.frame    =   self.leftFrame
                self.imageView3.frame    =   self.midFrame
                
            }, completion: {[unowned self](finished:Bool) -> Void in
                if self.isGestureEnabled{
                    self.imageView3.addGestureRecognizer(self.leftSwipe)
                    self.imageView3.addGestureRecognizer(self.rightSwipe)
                }
                self.imageView1.isHidden  =   false
                self.isAnimating=false
            })
            
        case 3:
            self.visibleImageView    =   self.imageView1
            imageView1.setImage(imageNamed: getImage(index: currentIndex))
            UIView.animate(withDuration: 0.5, animations: {
                self.imageView2.isHidden  =   true
                self.imageView1.frame    =   self.midFrame
                self.imageView2.frame    =   self.rightFrame
                self.imageView3.frame    =   self.leftFrame
                
            }, completion: {[unowned self](finished:Bool) -> Void in
                if self.isGestureEnabled{
                    self.imageView1.addGestureRecognizer(self.leftSwipe)
                    self.imageView1.addGestureRecognizer(self.rightSwipe)
                }
                self.imageView2.isHidden  =   false
                self.isAnimating=false
            })
        default: break
            
        }
        
    }
    
    func getImage(index:Int) -> String{
        return imageSourceArray[index]
    }
    
    func enableArrows()
    {
        let shape   =  CAShapeLayer()
        let path    =   UIBezierPath()
        path.move(to: CGPoint(x:self.frame.size.width-16, y:self.frame.size.height/2-10))
        path.addLine(to: CGPoint(x:self.frame.size.width-8,y: self.frame.size.height/2))
        path.addLine(to: CGPoint(x:self.frame.size.width-16,y: self.frame.size.height/2+10))
        shape.lineWidth =   2.0
        shape.path=path.cgPath;
        shape.fillColor=UIColor.clear.cgColor
        shape.strokeColor   =   UIColor.white.withAlphaComponent(0.7).cgColor
        self.layer.addSublayer(shape)
        
        
        let shape1   =  CAShapeLayer()
        let path1    =   UIBezierPath()
        path1.move(to: CGPoint(x:self.frame.size.width-18, y:self.frame.size.height/2-10))
        path1.addLine(to: CGPoint(x:self.frame.size.width-10,y: self.frame.size.height/2))
        path1.addLine(to: CGPoint(x:self.frame.size.width-18, y:self.frame.size.height/2+10))
        shape1.lineWidth =   2.0
        shape1.path=path1.cgPath;
        shape1.fillColor=UIColor.clear.cgColor
        shape1.strokeColor   =   UIColor.black.withAlphaComponent(0.7).cgColor
        self.layer.addSublayer(shape1)
        
        
        let shape2   =  CAShapeLayer()
        let path2    =   UIBezierPath()
        path2.move(to: CGPoint(x:16, y:self.frame.size.height/2-10))
        path2.addLine(to: CGPoint(x:8, y:self.frame.size.height/2))
        path2.addLine(to: CGPoint(x:16, y:self.frame.size.height/2+10))
        shape2.lineWidth =   2.0
        shape2.path=path2.cgPath;
        shape2.fillColor=UIColor.clear.cgColor
        shape2.strokeColor   =   UIColor.white.withAlphaComponent(0.7).cgColor
        self.layer.addSublayer(shape2)
        
        
        let shape3   =  CAShapeLayer()
        let path3    =   UIBezierPath()
        path3.move(to: CGPoint(x:18, y:self.frame.size.height/2-10))
        path3.addLine(to: CGPoint(x:10, y:self.frame.size.height/2))
        path3.addLine(to: CGPoint(x:18, y:self.frame.size.height/2+10))
        shape3.lineWidth =   2.0
        shape3.path=path3.cgPath;
        shape3.fillColor=UIColor.clear.cgColor
        shape3.strokeColor   =   UIColor.black.withAlphaComponent(0.7).cgColor
        self.layer.addSublayer(shape3)
        
        let leftBtn    =   UIButton()
        leftBtn.frame  =   CGRect(x:6,y: self.frame.size.height/2-15,width: 20,height: 30)
        leftBtn.addTarget(self, action: #selector(CLabsImageSlider.moveLeft), for: .touchUpInside)
        self.addSubview(leftBtn)
        
        let rightBtn    =   UIButton()
        rightBtn.frame  =   CGRect(x:self.frame.size.width-26, y:self.frame.size.height/2-15,width: 20, height:30)
        rightBtn.addTarget(self, action: #selector(CLabsImageSlider.moveRight), for: .touchUpInside)
        self.addSubview(rightBtn)
    }
}

extension UIImageView{
    func setImage(imageNamed:String,placeholderImage:String = "placeHolder.png"){
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.sd_setImage(with:  URL(string: imageNamed), placeholderImage:  UIImage(named: placeholderImage), options: SDWebImageOptions.cacheMemoryOnly, completed: { (image, error, cache, url) in})
            }
        }
    }
}
