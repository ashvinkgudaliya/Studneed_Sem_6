//
//  INeedTableViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 06/04/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class INeedTableViewController: UITableViewController {

    let data = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "INeedTableViewCell", bundle: nil), forCellReuseIdentifier: "iNeedTableViewCell")
        tableView.estimatedRowHeight = 20
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.clear
        tableView.rowHeight = UITableViewAutomaticDimension
        navigationBarDesign()
        
        let temp = NSMutableDictionary()
        temp.setValue("i Need Books Store", forKey: "heading")
        temp.setValue("i buy the b.com economy Books", forKey: "discription")
        temp.setValue("Ahmedabad", forKey: "city")
        temp.setValue("2", forKey: "view")
        temp.setValue("1", forKey: "request")
        temp.setValue("Books Store", forKey: "directory")
        
        let temp1 = NSMutableDictionary()
        temp1.setValue("i Need P.G", forKey: "heading")
        temp1.setValue("i need p.g only two months", forKey: "discription")
        temp1.setValue("Mumbai", forKey: "city")
        temp1.setValue("2", forKey: "view")
        temp1.setValue("2", forKey: "request")
        temp1.setValue("P.G", forKey: "directory")
        
        data.add(temp)
        data.add(temp1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func navigationBarDesign(){
        self.view.backgroundColor = Color.code.lightBar
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.title = "User Request"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
        
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(createNeedView))
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func createNeedView(){
        self.navigationController?.pushViewController(CreateNeedViewController(), animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let iNeedTableViewCell = tableView.dequeueReusableCell(withIdentifier: "iNeedTableViewCell", for: indexPath) as! INeedTableViewCell
        iNeedTableViewCell.backgroundColor = UIColor.clear
        iNeedTableViewCell.asignValue(data: data.dictionary(at: indexPath.row))
        return iNeedTableViewCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(NeedRequestViewController(), animated: true)
    }
}
