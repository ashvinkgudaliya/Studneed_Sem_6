
//  AGNotification.swift
//  TiffinChimp
//
//  Created by HiddenMind on 25/01/17.
//  Copyright © 2017 HiddenMind. All rights reserved.
//

import UIKit

class AGNotification: NSObject {
    
    class func success(withTitle:String = "Sucess",status:String,hideDone:Bool = true){
        let alert = FCAlertView()
        alert.makeAlertTypeSuccess()
        alert.hideDoneButton = hideDone
        if hideDone == true {
            alert.autoHideSeconds = 2
        }
        alert.dismissOnOutsideTouch = false
        alert.animateAlertInFromTop = true
        alert.showAlert(withTitle: withTitle, withSubtitle: status, withCustomImage: nil, withDoneButtonTitle: "Done", andButtons: nil)
        
        alert.doneActionBlock {
            print("Done button")
        }
    }
    
    class func retry(withTitle:String = "Failer",status:String = "Data",buttonName:String = "Retry",action:@escaping (Bool) -> ()){
        let alert = FCAlertView()
        alert.makeAlertTypeWarning()
        alert.animateAlertInFromTop = true
        alert.showAlert(withTitle: withTitle, withSubtitle: "\(status)", withCustomImage: nil, withDoneButtonTitle: buttonName, andButtons: nil)
        
        alert.addButton("Cancel") {
            action(false)
        }
        
        alert.doneActionBlock {
            action(true)
            alert.dismiss()
        }
    }
    
    
    class func error(withTitle:String = "Error",status:String,donebtnName:String="Nathing"){
        let alert = FCAlertView()
        alert.makeAlertTypeWarning()
        if donebtnName == "Nathing" {
            alert.hideDoneButton = true
            alert.autoHideSeconds = 2
        }
        alert.animateAlertInFromTop = true
        alert.showAlert(withTitle: withTitle, withSubtitle: status, withCustomImage: nil, withDoneButtonTitle: donebtnName, andButtons: nil)
        alert.doneActionBlock({})
    }
    
    class func warning(withTitle:String = "Warning",status:String){
        let alert = FCAlertView()
        alert.makeAlertTypeCaution()
        alert.hideDoneButton = true
        alert.autoHideSeconds = 2
        alert.animateAlertInFromTop = true
        alert.showAlert(withTitle: withTitle, withSubtitle: status, withCustomImage: nil, withDoneButtonTitle: "Done", andButtons: nil)
    }
    
    class func warning(withTitle:String = "Warning",status:String,donebtnName:String="Done",action:@escaping (Bool) -> ()){
        let alert = FCAlertView()
        alert.makeAlertTypeCaution()
        alert.hideDoneButton = false
        alert.animateAlertInFromTop = true
        alert.showAlert(withTitle: withTitle, withSubtitle: status, withCustomImage: nil, withDoneButtonTitle: donebtnName, andButtons: nil)
        alert.doneActionBlock({
            action(true)
        })
    }
    
    
    class func progress(title:String = "Sucess",status:String){
        let alert = FCAlertView()
        alert.makeAlertTypeProgress()
        alert.hideDoneButton = false
        alert.animateAlertInFromTop = true
        alert.showAlert(withTitle: title, withSubtitle: status, withCustomImage: nil, withDoneButtonTitle: "Done", andButtons: nil)
        
        alert.addButton("test") {
            print("This is clicked")
        }
        
        alert.doneActionBlock {
            print("Done button")
        }
    }
    
    class func rating(title:String = "Rate",status:String,rate:@escaping (Int) -> ()){
        let alert = FCAlertView()
        alert.makeAlertTypeRateHearts { rating in
            rate(rating)
        }

        alert.bounceAnimations = true
        alert.detachButtons = true
        alert.showAlert(withTitle: title, withSubtitle: status, withCustomImage: nil, withDoneButtonTitle: "Submite", andButtons: nil)
        alert.doneActionBlock {}
    }
}
