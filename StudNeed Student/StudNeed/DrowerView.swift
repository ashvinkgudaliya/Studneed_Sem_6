//
//  DeowerMenu.swift
//  TiffinChimp
//
//  Created by Ashvin Gudaliya on 28/12/16.
//  Copyright © 2016 HiddenMind. All rights reserved.
//

let CScreenHeight = UIScreen.main.bounds.height
let CScreenWidth = UIScreen.main.bounds.width

import UIKit

var AGSideMenu = DrowerView()

class DrowerView: UIView, UITableViewDelegate,UITableViewDataSource{

    open var isSwipeGestureEnabled:Bool = true{
        didSet{
            addSwipeGesture()
        }
    }
    
    var arrOption = NSMutableArray()
    var arrImages = NSMutableArray()
    
    var drowerBackView = UIView()
    var drowerOptionTable = UITableView()
    
    var userLoginView = UIView()
    var appIcon = UIImageView()
    var appNameLbl = UILabel()
    var userNameLbl = UILabel()
    
    override init(frame:CGRect){
        super.init(frame:CGRect(x: 0, y: 0, width: 0, height: CScreenHeight))
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        arrOption = ["Home" ,"I Need" ,"Profile" ,"My Wislist", "Feedback", "Change Password","FAQs" ,"Log out"]
        arrImages = [
            "AG_Memu_Home.png" ,"AG_menu_i_Need.png" ,"AG_Memu_Profile.png" ,"AG_Memu_wishlist.png" ,
            "AG_Memu_Comment.png", "keyword.png","AG_Memu_Faqs.png" ,"AG_Memu_Logout.png"
        ]

        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: drower menu design
    func setup(){
        self.frame = CGRect(x: 0, y: 0, width: CScreenWidth, height: CScreenHeight)
        drowerBackView.frame = CGRect(x: -(self.frame.width/5)*4, y: 0, width: (self.frame.width/5)*4, height: self.frame.height)
        userLoginView.frame = CGRect(x: 0, y: 0, width: drowerBackView.frame.width, height: drowerBackView.frame.height/3.5)
        appIcon.frame = CGRect(x: (userLoginView.frame.width/4)*1.5, y: 30, width: userLoginView.frame.width/4, height: userLoginView.frame.width/4)
        
        let lableHeight = userLoginView.frame.height - (appIcon.frame.height + appIcon.frame.origin.y + 15)
        appNameLbl.frame = CGRect(x: 0, y: appIcon.frame.height + appIcon.frame.origin.y + 5, width: userLoginView.frame.width, height: lableHeight/2)
        
        userNameLbl.frame = CGRect(x: 0, y: appNameLbl.frame.height + appNameLbl.frame.origin.y , width: userLoginView.frame.width, height: lableHeight/2)
        
        drowerOptionTable.frame = CGRect(x: 0, y: userLoginView.frame.height + 5, width: drowerBackView.frame.width , height: drowerBackView.frame.height - userLoginView.frame.height - 10)
        
        self.isHidden = true
        
        appIcon.image = UIImage(named: "college.jpg")
        appIcon.clipsToBounds = true
        appIcon.layer.cornerRadius = appIcon.frame.height/2
        
        appNameLbl.text = "StudNeed"
        appNameLbl.textAlignment = .center
        appNameLbl.textColor = UIColor.white
        appNameLbl.font = UIFont.systemFont(ofSize: 18)
        appNameLbl.adjustsFontSizeToFitWidth = true
        
        userNameLbl.textAlignment = .center
        userNameLbl.text = currentLoginUserDetais.string(forKey: "userName")
        userNameLbl.textColor = UIColor.white
        userNameLbl.font = UIFont.systemFont(ofSize: 20)
        userNameLbl.adjustsFontSizeToFitWidth = true
        
        drowerBackView.backgroundColor = UIColor.white
        
        userLoginView.backgroundColor = Color.code.bar

        userLoginView.layer.zPosition = 1
        
        drowerOptionTable.delegate = self
        drowerOptionTable.dataSource = self
        
        drowerOptionTable.separatorStyle = .none
        drowerOptionTable.showsVerticalScrollIndicator = false
        drowerOptionTable.showsHorizontalScrollIndicator = false
        drowerOptionTable.register(UITableViewCell.self, forCellReuseIdentifier: "optionCell")
        drowerOptionTable.backgroundColor = UIColor.clear
        drowerOptionTable.isScrollEnabled = true
        drowerOptionTable.bounces = false
        drowerBackView.addSubview(drowerOptionTable)
        
        userLoginView.addSubview(appIcon)
        userLoginView.addSubview(appNameLbl)
        userLoginView.addSubview(userNameLbl)
        
        drowerBackView.addSubview(userLoginView)
        self.addSubview(drowerBackView)
        
        UIApplication.shared.keyWindow?.addSubview(self)
    }
    
    //MARK: ActionMethod
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch:UITouch = (event?.allTouches?.first)!
        let touchPoint:CGPoint = touch.location(in: self)
        if !drowerBackView.frame.contains(CGRect(x: touchPoint.x, y: touchPoint.y, width: 0, height: 0)){
            openOrClose()
        }
    }
    
    //MARK: TableView dataSorce and delegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOption.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let optionCell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath)
        optionCell.backgroundColor = UIColor.white

        if optionCell.viewWithTag(101) != nil{
            let img = optionCell.viewWithTag(101) as! UIImageView
            img.removeFromSuperview()
        }
        
        let iconImageView:UIImageView = UIImageView(frame: CGRect(x:15 ,y:(optionCell.frame.height/2)/2 ,width: (optionCell.frame.height/2) ,height:(optionCell.frame.height/2)))
        iconImageView.tag=101
        iconImageView.image = UIImage(named: arrImages.object(at: indexPath.row)as! String)
        optionCell.addSubview(iconImageView)
        
        if optionCell.viewWithTag(102) != nil{
            let lbl = optionCell.viewWithTag(102) as! UILabel
            lbl.removeFromSuperview()
        }
        
        let optionNamelbl = UILabel(frame: CGRect(x:optionCell.frame.height + 5 ,y:0 ,width: optionCell.frame.width - optionCell.frame.height + 20  ,height:optionCell.frame.height))
        optionNamelbl.text = arrOption.object(at: indexPath.row) as? String
        optionNamelbl.tag=102
        optionNamelbl.textColor = UIColor.darkGray
        optionNamelbl.font = UIFont.systemFont(ofSize: 18)
        optionNamelbl.numberOfLines = 0
        optionNamelbl.adjustsFontSizeToFitWidth = true
        optionCell.addSubview(optionNamelbl)
        
        return optionCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return drowerOptionTable.frame.height / 9
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            AppDelegate.shared.setRootViewController(viewController: HomeViewController())
            break
        case 1:
            AppDelegate.shared.setRootViewController(viewController: INeedTableViewController())
            break
        case 5:
            
            AppDelegate.shared.setRootViewController(viewController: ChangePassViewController())
        case 6:
            UserDefaults.standard.removeObject(forKey: "isUserLoginDetails")
            AppDelegate.shared.setRootViewController(viewController: LoginAndSingUpViewController())
            break
        default:
            break
        }
        closeDrower()
    }
    
    //MARK: drower open and close methods
    func openOrClose(){
        userNameLbl.text = currentLoginUserDetais.string(forKey: "userName")
        self.isHidden ? openDrower():closeDrower()
    }
    
    func openDrower(){
        if self.isHidden{
            self.isHidden = false
            UIView.animate(withDuration: 0.4,
                           delay: 0.0,
                           options: UIViewAnimationOptions.transitionFlipFromBottom,
                           animations: {
                            self.drowerBackView.frame.origin.x = 0
            },
                           completion: { finished in
                            self.isHidden = false
            })
        }
        
    }
    
    func closeDrower(){
        if !self.isHidden{
            UIView.animate(withDuration: 0.4,
                           delay: 0.0,
                           options: UIViewAnimationOptions.transitionFlipFromBottom,
                           animations: {
                            self.drowerBackView.frame.origin.x = -(self.frame.width/5)*4
            },
                           completion: { finished in
                            self.isHidden = true
            })
        }
    }
    
    //MARK: drower swiper Gesture Recognizer
    func addSwipeGesture(){
        let pan_gr_left = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(moveDrawer))
        pan_gr_left.edges = .left
        
        let pan_gr_right = UIPanGestureRecognizer(target: self, action: #selector(moveDrawer))
        pan_gr_right.maximumNumberOfTouches = 1
        pan_gr_right.minimumNumberOfTouches = 1

        UIApplication.shared.keyWindow?.addGestureRecognizer(pan_gr_left)
        self.addGestureRecognizer(pan_gr_right)
    }
    
    func moveDrawer(_ recognizer: UIPanGestureRecognizer) {
        if isSwipeGestureEnabled {
            let translation: CGPoint = recognizer.translation(in: UIApplication.shared.keyWindow)
            if recognizer.state == .began {
                self.isHidden = false
            }
            if recognizer.state == .changed {
                let movingx: CGFloat = self.drowerBackView.center.x + translation.x
                if movingx <= (self.drowerBackView.frame.width/2) {
                    self.drowerBackView.center = CGPoint(x: CGFloat(movingx), y: CGFloat(UIApplication.shared.keyWindow!.center.y))
                    recognizer.setTranslation(CGPoint(x: CGFloat(0), y: CGFloat(0)), in: UIApplication.shared.keyWindow)
                }
                else if movingx <= (self.drowerBackView.frame.width/2){
                    self.drowerBackView.center = CGPoint(x: CGFloat(self.drowerBackView.frame.width/2), y: CGFloat(UIApplication.shared.keyWindow!.center.y))
                }
            }
            
            if recognizer.state == .ended {
                if self.drowerBackView.center.x >= 0{
                    self.isHidden = true
                    openDrower()
                }
                else {
                    closeDrower()
                }
            }
        }
    }
}
