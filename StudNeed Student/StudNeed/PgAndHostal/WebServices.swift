//
//  PgAndHostal.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 01/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import Alamofire

class WebServices {
    static let data = WebServices()
    
    func displayDirectoryData(directoryId:NSInteger, compilation:@escaping (NSArray) -> ()){
        let parameter : Parameters = [
            "directoryId" : directoryId
        ]
        
        Alamofire.request(Web.url.directoryData, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    compilation(aDict.array(forKey: "displayArray"))
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: (response.result.error?.localizedDescription)!)
                break
            }
        }
    }
    
    func getDirecotyData(venderDirectoryId:String,userId:String,compilation:@escaping (NSDictionary) -> ()){
        let parameter : Parameters = [
            "venderDirectoryId" : venderDirectoryId,
            "userId" : userId
        ]
        
        Alamofire.request(Web.url.directoryData, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    compilation(aDict.dictionary(forKey: "venderDirecotryDetails"))
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: (response.result.error?.localizedDescription)!)
                break
            }
        }
    }
}
