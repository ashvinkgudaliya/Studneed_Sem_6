//
//  PgAndHostalViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 01/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class DirectoryCartViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{//UISearchResultsUpdating,UISearchBarDelegate{

    let directoryCartTableView = UITableView()
    var directoryId = NSInteger()
    var viewTitle = String()
    var directoryDetailsArr = NSArray()

    var searchController = UISearchController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.code.lightBar
        navigationBarDesign()
        searchBar()
        viewControllerDesign()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        WebServices.data.displayDirectoryData(directoryId: directoryId, compilation:{ response in
            self.directoryDetailsArr = response
            self.directoryCartTableView.reloadData()
        })
    }
    
    func navigationBarDesign(){
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "mark.png"), style: .plain, target: self, action: #selector(mapOpenAction))
        
        self.navigationItem.titleView = menuView()
    }
    
    func viewControllerDesign(){
        directoryCartTableView.frame = self.view.bounds
        directoryCartTableView.delegate = self
        directoryCartTableView.showsHorizontalScrollIndicator = false
        directoryCartTableView.showsVerticalScrollIndicator = false
        directoryCartTableView.separatorStyle = .none
        directoryCartTableView.backgroundColor = Color.code.lightBar
        directoryCartTableView.dataSource = self
        directoryCartTableView.estimatedRowHeight = 22
        directoryCartTableView.rowHeight = UITableViewAutomaticDimension
        directoryCartTableView.register(UINib(nibName: "DirectoryDetailsCell", bundle: nil), forCellReuseIdentifier: "directoryCartCell")
        self.view.addSubview(directoryCartTableView)
    }
    
    func menuView() -> BTNavigationDropdownMenu{
        var items : [String] = []
        for i in 0..<allDirectoryType.count{
            items.append(allDirectoryType.dictionary(at: i).string(forKey: Web.key.name))
        }
        
        let menuView = BTNavigationDropdownMenu(navigationController: self.navigationController!, containerView: self.navigationController!.view, title: viewTitle, items: items)
        
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = self.navigationController?.navigationBar.tintColor
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left
        menuView.arrowPadding = 15
        menuView.menuTitleColor = self.navigationController?.navigationBar.tintColor
        menuView.maskBackgroundColor = self.navigationController?.navigationBar.tintColor
        menuView.animationDuration = 0.5
        menuView.maskBackgroundOpacity = 0.3
        menuView.backgroundColor = self.navigationController?.navigationBar.barTintColor
        //        menuView.checkMarkImage
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            self.directoryId = allDirectoryType.dictionary(at: indexPath).integer(forKey: "directoryId")
            self.viewWillAppear(true)
            switch indexPath {
            case 0,1:
                currentDirectory = .pg
                break
            case 2,3,4:
                currentDirectory = .school
                break
            case 5:
                currentDirectory = .book
                break
            default:
                break
            }
        }
        
        return menuView
    }
    
    
    func searchBar(){
        let home = SearchViewController()
        searchController = UISearchController(searchResultsController: home)
        searchController.searchResultsUpdater = home
        searchController.searchBar.delegate = home
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        UISearchBar.appearance().barTintColor = self.navigationController?.navigationBar.barTintColor
        UISearchBar.appearance().tintColor = self.navigationController?.navigationBar.tintColor
        searchController.searchBar.scopeButtonTitles = ["City", "Area", "Name"]
        directoryCartTableView.tableHeaderView = searchController.searchBar
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return directoryDetailsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let directoryCartCell = tableView.dequeueReusableCell(withIdentifier: "directoryCartCell", for: indexPath) as! DirectoryDetailsCell
        directoryCartCell.selectionStyle = .none
        directoryCartCell.backgroundColor = Color.code.lightBar
        
        directoryCartCell.venderImageBtn.tag = indexPath.row
        directoryCartCell.venderImageBtn.addTarget(self, action: #selector(openDirectoryAction(sender:)), for: .touchUpInside)
        directoryCartCell.assignValue(data: directoryDetailsArr.dictionary(at: indexPath.row))
        return directoryCartCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch currentDirectory {
        case .pg:
            let vc = PgDetailsViewController(nibName: "PgDetailsViewController", bundle: nil)
            vc.venderDirectoryId = directoryDetailsArr.dictionary(at: indexPath.row).string(forKey: "venderDirectoryId")
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case .school:
            let vc = ClassDeatilsViewController(nibName: "ClassDeatilsViewController", bundle: nil)
            vc.venderDirectoryId = directoryDetailsArr.dictionary(at: indexPath.row).string(forKey: "venderDirectoryId")
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case .book:
            let vc = BooksDeatilsViewController(nibName: "BooksDeatilsViewController", bundle: nil)
            vc.venderDirectoryId = directoryDetailsArr.dictionary(at: indexPath.row).string(forKey: "venderDirectoryId")
            self.navigationController?.pushViewController(vc, animated: true)
            break
        }
    }
    
    func mapOpenAction(){
        let vc = NearByDirectoryViewController()
        vc.displayData = self.directoryDetailsArr
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openDirectoryAction(sender:UIButton){
        let viewController = PopupDialogDefaultViewController()
        viewController.titleText   = directoryDetailsArr.dictionary(at: sender.tag).dictionary(forKey: "venderDetails").string(forKey: "venderName").uppercased()
        viewController.messageText = "Contact No. : " + directoryDetailsArr.dictionary(at: sender.tag).dictionary(forKey: "venderDetails").string(forKey: "mobile") + "\n\n Email : " + directoryDetailsArr.dictionary(at: sender.tag).dictionary(forKey: "venderDetails").string(forKey: "email")
        viewController.standardView.imageView.setImage(imageNamed: directoryDetailsArr.dictionary(at: sender.tag).dictionary(forKey: "venderDetails").string(forKey: "profilePic"))

        let popup = PopupDialog(viewController: viewController)
        let buttonOne = CancelButton(title: "CANCEL") {
        }
        let buttonTwo = DefaultButton(title: "See Full Vender Details".uppercased()) {
            let vc = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
            vc.displayData = self.directoryDetailsArr.dictionary(at: sender.tag).dictionary(forKey: "venderDetails")
            self.navigationController?.pushViewController(vc, animated: true)
        }

        popup.addButtons([buttonTwo,buttonOne])
        
        self.present(popup, animated: true, completion: nil)
    }
}

struct Search {
    let name : String
    let scope : String
}


class SearchViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchResultsUpdating,UISearchBarDelegate{
    
    var tableView = UITableView()
    var searchData = [Search]()
    var filteredSearchData = [Search]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //SerchBar
        // Setup the Search Controller
        definesPresentationContext = true
        tableView.frame = self.view.bounds
        self.view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        searchData = [
            Search(name:"Ahmedabad" ,scope: "City"),
            Search(name:"Surat" ,scope: "City"),
            Search(name:"Bhavnager" ,scope: "City"),
            Search(name:"rajkot" ,scope: "City"),
            Search(name:"Vadodra" ,scope: "City"),
            Search(name:"Morbi" ,scope: "City"),
            Search(name:"Bhuj" ,scope: "City"),
            Search(name:"Amreli" ,scope: "City"),
        ]
        filteredSearchData = searchData
    }
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredSearchData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)        
        cell.textLabel?.text = filteredSearchData[indexPath.row].name
        return cell
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String) {
        filteredSearchData = searchData.filter({( candy : Search) -> Bool in
            return candy.name.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
    
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchController.searchBar.text!, scope: scope)
    }
}
