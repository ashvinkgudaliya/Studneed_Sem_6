//
//  PgandHostalTableViewCell.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 01/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class DirectoryDetailsCell: UITableViewCell {

    @IBOutlet weak var viewLbl: UILabel!
    @IBOutlet weak var likesLbl: UILabel!
    @IBOutlet weak var CommentLbl: UILabel!
    @IBOutlet weak var backgroundContainView: UIView!
    @IBOutlet weak var venderImageBtn: UIButton!
    @IBOutlet weak var contactNoLbl: UILabel!
    @IBOutlet weak var emailIdLbl: UILabel!
    
    @IBOutlet weak var directoryLbl: UILabel!
    @IBOutlet weak var directoryAddressLbl: UILabel!
    
    @IBOutlet weak var directoryImagesView: UIImageView!
    
    @IBOutlet weak var venderImageView: CustomImageView!
    @IBOutlet weak var directoryNameBackGroundView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundContainView.shadowEnable(height: 4, width: 0)
        backgroundContainView.layer.cornerRadius = 8
        directoryNameBackGroundView.isFullBlurEffect()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func assignValue(data:NSDictionary){
        directoryLbl.text = data.string(forKey: "name").capitalized
        directoryAddressLbl.text = data.string(forKey: "locality") + ", " + data.string(forKey: "city")
        likesLbl.text = "\(data.integer(forKey: "likes"))"
        CommentLbl.text = "\(data.integer(forKey: "comment"))"
        directoryImagesView.setImage(imageNamed: data.string(forKey: "images"))
        contactNoLbl.text = data.string(forKey: "contactNo")
        emailIdLbl.text = data.string(forKey: "emailId")
        viewLbl.text = data.string(forKey: "views")
        venderImageView.setImage(imageNamed: data.dictionary(forKey: "venderDetails").string(forKey: "profilePic"))
    }
}
