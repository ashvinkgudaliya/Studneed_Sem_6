//
//  ClassDeatilsViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 06/04/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class ClassDeatilsViewController: UIViewController {

    @IBOutlet weak var imgSlider: CLabsImageSlider!
    
    @IBOutlet weak var ratingChat: HACBarChart!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var shortLocationLbl: UILabel!
    @IBOutlet weak var likeCount: UILabel!
    
    @IBOutlet weak var wislistBtn: DOFavoriteButton!
    @IBOutlet weak var likeBtn: DOFavoriteButton!
    @IBOutlet weak var ratingBtn: CustomButton!
    @IBOutlet weak var commentBtn: CustomButton!
    
    @IBOutlet weak var discriptionLbl: UILabel!
    
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var ContactLbl: UILabel!
    
    @IBOutlet weak var genderSagment: UISegmentedControl!
    
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var localityLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var countyLbl: UILabel!
    @IBOutlet weak var pincodeLbl: UILabel!
    
    var venderDirectoryId = String()
    var urlImages : [String] = []
    var displayFullData = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        WebServices.data.getDirecotyData(venderDirectoryId: venderDirectoryId, userId: currentLoginUserDetais.string(forKey: "userId")) { directoryData in
            self.displayFullData = directoryData
            self.updateData()
            self.urlImages = self.displayFullData.array(forKey: "images").count == 0 ? [] : self.displayFullData.array(forKey: "images") as! [String]
            
            self.imgSlider.setUpView(imageSource: self.urlImages ,slideType:.ManualSwipe ,isArrowBtnEnabled: true)
        }
        
        navigationBarDesign()
        
        self.view.backgroundColor = Color.code.lightBar
        
        
        navigationBarDesign()
        ratingChart()
    }
    
    func navigationBarDesign(){
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func ratingChart(){
        ratingChat.showAxis = true
        ratingChat.showProgressLabel = true
        ratingChat.vertical = false
        ratingChat.reverse = false
        ratingChat.showDataValue = true
        ratingChat.showCustomText = true
        ratingChat.barsMargin = 15
        ratingChat.sizeLabelProgress = 40
        ratingChat.numberDividersAxisY = 5
        ratingChat.animationDuration = 2
        ratingChat.progressTextColor = UIColor.darkGray
        ratingChat.axisYTextColor = UIColor(red: CGFloat(0.80), green: CGFloat(0.80), blue: CGFloat(0.80), alpha: CGFloat(1.0))
        ratingChat.progressTextFont = UIFont(name: "DINCondensed-Bold", size: 10)
        ratingChat.typeBar = HACBarType(HACBarType2)
        ratingChat.dashedLineColor = UIColor(red: CGFloat(0.44), green: CGFloat(0.66), blue: CGFloat(0.86), alpha: CGFloat(0.3))
        ratingChat.axisXColor = UIColor(red: CGFloat(0.44), green: CGFloat(0.66), blue: CGFloat(0.86), alpha: CGFloat(1.0))
        ratingChat.axisYColor = UIColor(red: CGFloat(0.44), green: CGFloat(0.66), blue: CGFloat(0.86), alpha: CGFloat(1.0))
        ratingChat.clear()
        ratingChat.draw()
    }
    
    func updateData(){
        nameLbl.text = self.displayFullData.string(forKey: "name").capitalized
        self.navigationItem.title = self.displayFullData.string(forKey: "name").capitalized
        shortLocationLbl.text = self.displayFullData.string(forKey: "city")
        
        self.likeCount.text = "\(self.displayFullData.integer(forKey: "likeCount"))"
        commentBtn.setTitle("\(self.displayFullData.integer(forKey: "commentCount"))", for: .normal)
        ratingBtn.setTitle("\(self.displayFullData.integer(forKey: "userRating"))", for: .normal)
        
        likeBtn.isSelected = displayFullData.string(forKey: "bLiked") == "1"
        wislistBtn.isSelected = displayFullData.string(forKey: "wishlist") == "1"
        
        discriptionLbl.text = displayFullData.string(forKey: "discription")
        emailLbl.text = self.displayFullData.string(forKey: "emailId")
        ContactLbl.text = self.displayFullData.string(forKey: "contactNo")
        
        addressLbl.text = self.displayFullData.string(forKey: "address")
        cityLbl.text = self.displayFullData.string(forKey: "city")
        localityLbl.text = self.displayFullData.string(forKey: "locality")
        stateLbl.text = self.displayFullData.string(forKey: "state")
        countyLbl.text = self.displayFullData.string(forKey: "country")
        pincodeLbl.text = self.displayFullData.string(forKey: "pinCode")
        
        if displayFullData.string(forKey: "gender") == "Girls Only"{
            genderSagment.selectedSegmentIndex = 1
        }
        else if displayFullData.string(forKey: "gender") == "Boys Only"{
            genderSagment.selectedSegmentIndex = 0
        }
        else {
            genderSagment.selectedSegmentIndex = 2
        }
        
        let data = [
            [kHACPercentage: self.displayFullData.dictionary(forKey: "rating").integer(forKey: "five"), kHACColor: UIColor(red: CGFloat(0.000), green: CGFloat(0.620), blue: CGFloat(0.890), alpha: CGFloat(1.0)), kHACCustomText: "5"],
            [kHACPercentage: self.displayFullData.dictionary(forKey: "rating").integer(forKey: "four"), kHACColor: UIColor(red: CGFloat(0.431), green: CGFloat(0.000), blue: CGFloat(0.533), alpha: CGFloat(1.0)), kHACCustomText: "4"],
            [kHACPercentage: self.displayFullData.dictionary(forKey: "rating").integer(forKey: "there"), kHACColor: UIColor(red: CGFloat(0.922), green: CGFloat(0.000), blue: CGFloat(0.000), alpha: CGFloat(1.0)), kHACCustomText: "3"],
            [kHACPercentage: self.displayFullData.dictionary(forKey: "rating").integer(forKey: "two"), kHACColor: UIColor(red: CGFloat(0.000), green: CGFloat(0.671), blue: CGFloat(0.180), alpha: CGFloat(1.0)), kHACCustomText: "2"],
            [kHACPercentage: self.displayFullData.dictionary(forKey: "rating").integer(forKey: "one"), kHACColor: UIColor(red: CGFloat(1.000), green: CGFloat(0.000), blue: CGFloat(0.851), alpha: CGFloat(1.0)), kHACCustomText: "1"]
        ]
        ratingChat.data = data
        ratingChat.clear()
        ratingChat.draw()
    }
    
    //MARK: action method
    @IBAction func wislistBtnAction(_ sender: DOFavoriteButton) {
        DirectoryAction.webService.wishlist(venderDirectoryId: displayFullData.string(forKey: "venderDirectoryId"), userId: currentLoginUserDetais.string(forKey: "userId"), wishlist: !sender.isSelected) { wislist in
            if wislist == 1 {
                sender.select()
            }
            else{
                sender.deselect()
            }
        }
    }
    
    @IBAction func likeBtnAction(_ sender: DOFavoriteButton) {
        DirectoryAction.webService.like(venderDirectoryId: displayFullData.string(forKey: "venderDirectoryId"), userId: currentLoginUserDetais.string(forKey: "userId"), like: !sender.isSelected) { (like, likeCount) in
            if like == "1" {
                sender.select()
            }
            else{
                sender.deselect()
            }
            self.likeCount.text = "\(likeCount)"
        }
    }
    
    @IBAction func commentBtnAction(_ sender: UIButton) {
        let commentsViewController = CommentsViewController()
        commentsViewController.venderDirectoryId = displayFullData.integer(forKey: "venderDirectoryId")
        commentsViewController.venderDirectoryName = displayFullData.string(forKey: "name")
        commentsViewController.directoryImages = displayFullData.array(forKey: "images").object(at: 0) as! String
        self.navigationController?.pushViewController(commentsViewController, animated: true)
    }
    
    @IBAction func ratingBtnAction(_ sender: UIButton) {
        AGNotification.rating(title: "\(displayFullData.dictionary(forKey: "directryType").string(forKey: "name")) Ratting", status: "Give This \(displayFullData.string(forKey: "name")) Directory Your Rating") { rate in
            
            DirectoryAction.webService.rate(venderDirectoryId: self.displayFullData.string(forKey: "venderDirectoryId"), userId: currentLoginUserDetais.string(forKey: "userId"), ratting: rate, compilation: { (ratingDict, userRating) in
                
                let data = [
                    [kHACPercentage: ratingDict.integer(forKey: "five"), kHACColor: UIColor(red: CGFloat(0.000), green: CGFloat(0.620), blue: CGFloat(0.890), alpha: CGFloat(1.0)), kHACCustomText: "5"],
                    [kHACPercentage: ratingDict.integer(forKey: "four"), kHACColor: UIColor(red: CGFloat(0.431), green: CGFloat(0.000), blue: CGFloat(0.533), alpha: CGFloat(1.0)), kHACCustomText: "4"],
                    [kHACPercentage: ratingDict.integer(forKey: "there"), kHACColor: UIColor(red: CGFloat(0.922), green: CGFloat(0.000), blue: CGFloat(0.000), alpha: CGFloat(1.0)), kHACCustomText: "3"],
                    [kHACPercentage: ratingDict.integer(forKey: "two"), kHACColor: UIColor(red: CGFloat(0.000), green: CGFloat(0.671), blue: CGFloat(0.180), alpha: CGFloat(1.0)), kHACCustomText: "2"],
                    [kHACPercentage: ratingDict.integer(forKey: "one"), kHACColor: UIColor(red: CGFloat(1.000), green: CGFloat(0.000), blue: CGFloat(0.851), alpha: CGFloat(1.0)), kHACCustomText: "1"]
                ]
                self.ratingChat.data = data
                self.ratingChat.clear()
                self.ratingChat.draw()
                
                self.ratingBtn.setTitle("\(userRating)", for: .normal)
            })
        }
    }
}
