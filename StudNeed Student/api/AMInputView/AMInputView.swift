//
//  AMInputView.swift
//  AMLoginSingup
//
//  Created by amir on 10/11/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit

@IBDesignable class AMInputView: UIView,UITextFieldDelegate {
    
    let animationDuration = 0.25
    var isCheckValidation:Validation = .notNull{
        didSet{
            setKeyboardType()
        }
    }
    
    var isValidated:Bool {
        get{
            return validationCheck()
        }
    }
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var textFieldView: UITextField!
    @IBOutlet weak var backWhiteView: UIView!
    @IBOutlet weak var labelView: UILabel!
    
    //MARK: - IBInspectable properties
    @IBInspectable var placeholder:String = ""{
        didSet{
            if labelView != nil{
                labelView.text = placeholder
            }
        }
    }
    
    open var text:String {
        get{
            return textFieldView.text!
        }
    }
    
    @IBInspectable var keyboardType = UIKeyboardType.default{
        didSet{
            if textFieldView != nil{
                textFieldView.keyboardType = UIKeyboardType(rawValue: keyboardType.rawValue)!
            }
        }
    }
    
    @IBInspectable var returnKeyType = UIReturnKeyType.default{
        didSet{
            if textFieldView != nil{
                textFieldView.returnKeyType = returnKeyType
            }
        }
    }
    
    
    @IBInspectable var isSecureTextEntry:Bool = false{
        didSet{
            if textFieldView != nil{
                textFieldView.isSecureTextEntry = isSecureTextEntry
            }
        }
    }
    
    //MARK: - constraints
    @IBOutlet weak var backViewHeightExpandConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelViewRightExpandConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelViewLeftExpandConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelViewHTopExpandConstraint: NSLayoutConstraint!
    
    //MARK: - init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    private func setup(){
        loadNib()
        self.addSubview(contentView)
        self.addConstraints([
            
            NSLayoutConstraint(item: contentView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: contentView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: contentView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: contentView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
            
            ])
        textFieldView.textColor = UIColor.white
        backWhiteView.backgroundColor = Color.code.bar
        textFieldView.delegate = self
        textFieldView.returnKeyType = .done
        textFieldView.autocorrectionType = .no
        backgroundColor = UIColor.clear
        self.layer.cornerRadius = 4
    }
    
    private func loadNib() {
        let bundle = Bundle(for: AMInputView.classForCoder())
        bundle.loadNibNamed("AMInputView", owner: self, options: nil)
    }
    
    func validationCheck() -> Bool{
        if !textFieldView.isValid(rex: isCheckValidation){
            AFViewShaker(view: self).shake()
            return false
        }
        return true
    }
    
    func setKeyboardType(){
        switch isCheckValidation {
        case .email:
            keyboardType = .emailAddress
            break
        case .userName:
            keyboardType = .alphabet
            break
        case .password:
            textFieldView.isSecureTextEntry = true
            keyboardType = .default
            break
        case .phone:
            keyboardType = .numberPad
            break
        default:
            keyboardType = .default
        }
    }
    
    //MARK: - textfield delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (textField.text?.lengthOfBytes(using: .utf8))! < 1 {
            setViewExpandMode(expand:true)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (textField.text?.lengthOfBytes(using: .utf8))! < 1 {
            setViewExpandMode(expand:false)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if isCheckValidation == .phone{
            if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) == nil{
                let len = (textField.text?.characters.count)! + string.characters.count
                if len <= 10 {
                    return true
                }
            }
        }
        else if isCheckValidation == .userName{
            if string.rangeOfCharacter(from: NSCharacterSet.letters.inverted) == nil{
                return true
            }
        }
        else{
            return true
        }
        
        AFViewShaker(view: self).shake()
        return false
    }
    
    //MARK: - change view
    private func setViewExpandMode(expand:Bool){
    
        backViewHeightExpandConstraint.priority = expand ? 500:200
        labelViewHTopExpandConstraint.priority = expand ? 500:200
        labelViewRightExpandConstraint.priority = expand ? 500:300
        labelViewLeftExpandConstraint.priority = expand ? 999:300
        
        UIView.animate(withDuration: animationDuration) {
            self.layoutIfNeeded()
            self.backWhiteView.alpha = expand ? 0.6:0.08
            
            let scale:CGFloat = expand ? 0.7:1
            
            self.labelView.transform = CGAffineTransform(scaleX: scale, y: scale)
            
            if expand {
            
                let leftMargin = (-1 * ((1-scale)*(self.labelView.layer.bounds.size.width))/2 ) - 11;
                self.labelView.transform = self.labelView.transform.translatedBy(x: leftMargin, y: 0)
            }
        }
    }
}

enum Validation{
    case userName
    case email
    case password
    case phone
    case notNull
    case nullAllow
}

extension UITextField{
    func isValid(rex:Validation) -> Bool {
        
        if Validation.nullAllow != rex{
            if self.text!.isEmpty{
                return false
            }
        }
        
        var rexString = String()
        switch rex {
        case .userName:
            rexString = "[A-Za-z0-9]{3,10}"
            break
        case .email:
            rexString = "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
            break
        case .password:
            rexString = "^.{6,20}$"
            break
        case .phone:
            rexString = "[0-9]{10}"
            break
        case .notNull:
            return !self.text!.isEmpty
        case .nullAllow:
            return true
        }

        let emailTest = NSPredicate(format:"SELF MATCHES %@", rexString)
        return emailTest.evaluate(with: self.text)
    }
}



