//
//  CardView.m
//  YSLDraggingCardContainerDemo
//
//  Created by yamaguchi on 2015/11/09.
//  Copyright © 2015年 h.yamaguchi. All rights reserved.
//

#import "CardView.h"

@implementation CardView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    _imageView = [[UIImageView alloc]init];
    _imageView.backgroundColor = [UIColor orangeColor];
    
    _imageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height * 0.75);
    [self addSubview:_imageView];
    
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_imageView.bounds
                                     byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
                                           cornerRadii:CGSizeMake(7.0, 7.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = _imageView.bounds;
    maskLayer.path = maskPath.CGPath;
    _imageView.layer.mask = maskLayer;
    
    _selectedView = [[UIView alloc]init];
    _selectedView.frame = _imageView.frame;
    _selectedView.backgroundColor = [UIColor clearColor];
    _selectedView.alpha = 0.0;
    [_imageView addSubview:_selectedView];
    
    _headerLabel = [[UILabel alloc]init];
    _headerLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _headerLabel.frame = CGRectMake(0, (self.frame.size.height * 0.75) - self.frame.size.height * 0.14, self.frame.size.width, self.frame.size.height * 0.14);
    _headerLabel.numberOfLines = 1;
    _headerLabel.textColor = [UIColor whiteColor];
    _headerLabel.font = [UIFont boldSystemFontOfSize:23];
    [self addSubview:_headerLabel];

    
    _label = [[UILabel alloc]init];
    _label.backgroundColor = [UIColor clearColor];
    _label.frame = CGRectMake(10, self.frame.size.height * 0.75, self.frame.size.width - 20, self.frame.size.height * 0.25);
    _label.numberOfLines = 0;
    _label.font = [UIFont fontWithName:@"Futura-Medium" size:16];
    [self addSubview:_label];
}

@end
