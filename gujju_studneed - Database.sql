-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 13, 2017 at 04:36 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gujju_studneed`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(20) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Before 12'),
(2, 'After 12'),
(3, 'Post Graduation');

-- --------------------------------------------------------

--
-- Table structure for table `category_sub`
--

CREATE TABLE `category_sub` (
  `id` int(20) NOT NULL,
  `category` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_sub`
--

INSERT INTO `category_sub` (`id`, `category`, `name`, `image`) VALUES
(34, '3', 'Management & Business', '0001109001453181129.png'),
(32, '3', 'Science', '0276580001453181102.png'),
(33, '3', 'Law', '0334269001453181114.png'),
(4, '1', 'Std 2nd', '0775963001453180269.png'),
(3, '1', 'Std 1st', '0777475001453180208.png'),
(5, '1', 'Std 3d', '0037465001453180296.png'),
(6, '1', 'Std 4th', '0525951001453180304.png'),
(7, '1', 'Std 5th', '0035381001453180312.png'),
(8, '1', 'Std 6th', '0217790001453180319.png'),
(9, '1', 'Std 7th', '0570156001453180325.png'),
(10, '1', 'Std 8th', '0185590001453180333.png'),
(11, '1', 'Std 9th', '0043982001453180340.png'),
(12, '1', 'Std 10th', '0503409001453180347.png'),
(13, '1', 'Arts', '0598930001453180356.png'),
(14, '1', 'Commerce', '0451607001453180368.png'),
(15, '1', 'Science', '0715309001453180380.png'),
(16, '2', 'B.E./B.TECH.', '0470867001453180390.png'),
(17, '2', 'Science', '0067705001453180475.png'),
(18, '2', 'Law', '0072621001453180491.png'),
(19, '2', 'Management & Business', '0159516001453180559.png'),
(20, '2', 'Arts & Humanities', '0574597001453180595.png'),
(21, '2', 'Fashion and Design', '0969651001453180613.png'),
(22, '2', 'Medical & Health Care', '0925108001453180656.png'),
(23, '2', 'Teaching', '0288214001453180676.png'),
(24, '2', 'Hotel Management', '0502601001453180700.png'),
(25, '2', 'Architecture', '0021545001453180717.png'),
(26, '2', 'Pharmacy', '0006489001453180751.png'),
(27, '2', 'Mass Comm & Media', '0126383001453180784.png'),
(28, '2', 'Commerce', '0340253001453180799.png'),
(29, '2', 'Computers', '0378399001453180819.png'),
(30, '2', 'Travel & Tourism', '0521037001453180865.png'),
(31, '3', 'B.E./B.TECH.', '0503678001453181086.png'),
(35, '3', 'Arts & Humanities', '0900961001453181143.png'),
(36, '3', 'Fashion and Design', '0252724001453181157.png'),
(37, '3', 'Medical & Health Care', '0451224001453181183.png'),
(38, '3', 'Teaching', '0660008001453181214.png'),
(39, '3', 'Mass Comm & Media', '0746642001453181225.png'),
(40, '3', 'Architecture', '0129349001453181238.png'),
(41, '3', 'Pharmacy', '0358052001453181250.png'),
(42, '3', 'Travel & Tourism', '0876823001453181263.png'),
(43, '3', 'Commerce', '0039747001453181280.png'),
(44, '3', 'Computers', '0551434001453181309.png'),
(46, '1', 'Diploma Engineering', '0081901001459749012.png');

-- --------------------------------------------------------

--
-- Table structure for table `category_sub_sub`
--

CREATE TABLE `category_sub_sub` (
  `id` int(20) NOT NULL,
  `category` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fform` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sem` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `noyear` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_sub_sub`
--

INSERT INTO `category_sub_sub` (`id`, `category`, `subcategory`, `name`, `fform`, `sem`, `noyear`) VALUES
(3, '1', '13', 'Std 11th', 'Standard 11th', 'Sem', '1'),
(4, '1', '13', 'Std 12th', 'Standard 12th', 'Sem', '1'),
(5, '1', '14', 'Std 11th', 'Standard 11th', 'Sem', '1'),
(6, '1', '14', 'Std 12th', 'Standard 12th', 'Sem', '1'),
(7, '1', '15', 'Std 11th', 'Standard 11th', 'Sem', '1'),
(8, '1', '15', 'Std 12th', 'Standard 12th', 'Sem', '1'),
(9, '2', '16', 'B.E [Comp.E]', 'Bachelor of Computer Engineering', 'Sem', '4'),
(10, '2', '16', 'B.E [Mech.E]', 'Bachelor of Mechanical Engineering', 'Sem', '4'),
(11, '2', '16', 'B.E [E.C]', 'Bachelor of Electronics & Communication Engineering', 'Sem', '4'),
(12, '2', '16', 'B.E [Civil.E]', 'Bachelor of Civil Engineering', 'Sem', '4'),
(13, '2', '16', 'B.E [E.E]', 'Bachelor of Electrical & Electronics Engineering', 'Sem', '4'),
(14, '2', '16', 'B.E [A.E]', 'Bachelor of Aerospace Engineering', 'Sem', '4'),
(15, '2', '16', 'B.E [I.T]', 'Bachelor of  Information Technology', 'Sem', '4'),
(16, '2', '16', 'B.E [E.I]', 'Bachelor of Electronics & Instrumentation Engineering', 'Sem', '4'),
(17, '2', '16', 'B.E [A.E]', 'Bachelor of Aeronautical Engineering', 'Sem', '4'),
(18, '2', '16', 'B.E [E.C]', 'Bachelor of Electronics & Computer Engineering', 'Sem', '4'),
(19, '2', '16', 'B.E [Chem.E]', 'Bachelor of Chemical Engineering', 'Sem', '4'),
(20, '2', '16', 'B.E [A.E]', 'Bachelor of Automobile Engineering', 'Sem', '4'),
(21, '2', '16', 'B.E [MetaM.E]', 'Bachelor of Metallurgical Engineering', 'Sem', '4'),
(22, '2', '16', 'B.E [Mari.E]', 'Bachelor of Marine Engineering', 'Sem', '4'),
(23, '2', '16', 'B.E [Avi.E]', 'Bachelor of Avionics Engineering', 'Sem', '4'),
(24, '2', '16', 'B.E [Air.M.E]', 'Bachelor of Aircraft Maintenance', 'Sem', '4'),
(25, '2', '16', 'B.E [H.E]', 'Bachelor of Harbour and Ocean Engineering', 'Sem', '4'),
(26, '2', '16', 'B.E [N.S.E]', 'Bachelor of Naval Architecture and Ship Building Engineering', 'Sem', '4'),
(27, '2', '16', 'B.E [P.T]', 'Bachelor of Printing & Packaging Technology', 'Sem', '4'),
(28, '2', '16', 'B.E [Ele.Tele]', 'Bachelor of Electronics & Telecommunication Engineering', 'Sem', '4'),
(29, '2', '16', 'B.E [T.T]', 'Bachelor of Textile Technology', 'Sem', '4'),
(30, '2', '16', 'B.E [O.T]', 'Bachelor of Oil & Plant Technology', 'Sem', '4'),
(31, '2', '16', 'B.E [Bi.T]', 'Bio Technology', 'Sem', '4'),
(32, '2', '16', 'B.E [Fire.T]', 'Bachelor of Fire Technology and Safety', 'Sem', '4'),
(33, '2', '16', 'B.E [Grafi.E]', 'Bachelor of 3D Animation & Graphics Engineering', 'Sem', '4'),
(34, '2', '16', 'B.E [M.A]', 'Bachelor of Mechanical & Automation Engineering', 'Sem', '4'),
(35, '2', '16', 'Polytechnic', 'Polytechnic', 'Sem', '3'),
(36, '2', '17', 'BSc Psychology', 'Bachelor of Science Psychology', 'Sem', '3'),
(37, '2', '17', 'BSc Mathematics', 'Bachelor of Science Mathematics', 'Sem', '3'),
(38, '2', '17', 'BSc Chemistry', 'Bachelor Science of Chemistry', 'Sem', '3'),
(39, '2', '17', 'BSc Physics', 'Bachelor Science of Physics', 'Sem', '3'),
(40, '2', '17', 'BSc Computer Science', 'Bachelor of Science in Computer Science', 'Sem', '3'),
(41, '2', '17', 'BSc Biology', 'Bachelor Science of Biology', 'Sem', '3'),
(42, '2', '18', 'LLB', 'Bachelor of Law', 'Sem', '3'),
(43, '2', '18', 'BA.LLB', 'Bachelor of Law - Integrated', 'Sem', '5'),
(44, '2', '18', 'BBA.LLB', 'BBA + LLB - Integrated', 'Sem', '5'),
(45, '2', '19', 'BBA', 'Bachelor of Business Administration', 'Sem', '3'),
(46, '2', '20', 'BA', 'Bachelor of Arts', 'Sem', '4'),
(47, '2', '20', 'BFA', 'Bachelor of Fine Arts', 'Sem', '4'),
(48, '2', '21', 'Fashion Design', 'Fashion Design', 'Sem', '3'),
(49, '2', '21', 'Design', 'Design', 'Sem', '4'),
(50, '2', '21', 'Diploma in Fashion', 'Diploma in Fashion', 'Sem', '3'),
(51, '2', '21', 'Diploma in Design', 'Diploma in Design', 'Sem', '3'),
(52, '2', '21', 'Fashion Technology', 'Fashion Technology', 'Sem', '4'),
(53, '2', '21', 'Bachelor in Interior Design', 'Bachelor in Interior Design', 'Sem', '3'),
(54, '2', '21', 'BFD', 'Bachelor in Fashion Design', 'Sem', '3'),
(55, '2', '21', 'Bachelor in Design', 'Bachelor in Design', 'Sem', '4'),
(56, '2', '22', 'MBBS', 'Bachelor of Medicine and Surgery', 'Sem', '5'),
(57, '2', '22', 'BDS', 'Bachelor of Dental Surgery', 'Sem', '4'),
(58, '2', '22', 'BPT', 'Bachelor of Physiotherapy', 'Sem', '4'),
(59, '2', '22', 'BHMS', 'Bachelor of Homoeopathic Medicine and Surgery', 'Sem', '4'),
(60, '2', '22', 'BAMS', 'Bachelor of Ayurvedic Medicine and Surgery', 'Sem', '4'),
(61, '2', '22', 'GNM', 'Diploma in General Nursery and Midwifery', 'Sem', '3'),
(62, '2', '23', 'B.Ed', 'Bachelor of Education', 'Sem', '2'),
(63, '2', '24', 'BHM', 'Bachelor of Hotel Management', 'Sem', '3'),
(64, '2', '24', 'Hotel Management', 'Hotel Management', 'Year', '1'),
(65, '2', '24', 'Hotel Management Diploma', 'Diploma in Hotel Management', 'Sem', '3'),
(66, '2', '24', 'BJMC', 'Bachelor of Journalism & Mass Communication', 'Sem', '3'),
(67, '2', '25', 'B.Arch', 'Bachelors of Architecture', 'Sem', '5'),
(68, '2', '26', 'B.Pharm', 'Bachelor of Pharmacy', 'Sem', '4'),
(69, '2', '26', 'D.Pharm', 'Diploma of Pharmacy', 'Sem', '4'),
(70, '2', '27', 'Mass Communication', 'Mass Communication', 'Year', '4'),
(71, '2', '27', 'Media', 'Media', 'Year', '3'),
(72, '2', '27', 'BMM', 'Bachelor of Mass Media', 'Year', '3'),
(73, '2', '27', 'Diploma Event Management', 'Diploma in Event Management', 'Year', '1'),
(74, '2', '28', 'B.Com', 'Bachelor of Commerce', 'Sem', '3'),
(75, '2', '29', 'BCA', 'Bachelor of Computer Applications', 'Sem', '3'),
(76, '2', '30', 'Travel Tourism', 'Travel Tourism', 'Sem', '3'),
(77, '2', '30', 'B.Com', 'BCom in Travel & Tourism Management', 'Sem', '3'),
(78, '2', '30', 'BA', 'BA in Travel & Tourism Management', 'Sem', '3'),
(79, '2', '30', 'Diploma', 'Diploma in Travel & Tourism Management', 'Sem', '1'),
(80, '3', '31', 'M.Tech [C.S]', 'Master of Technology in Computer Science & Engineering', 'Sem', '2'),
(81, '3', '31', 'M.Tech [VLSI]', 'Master of Technology in Very Large Scale Integration (VLSI) Engineering', 'Sem', '2'),
(82, '3', '31', 'M.Tech [S.E]', 'Master of Technology in Structural Engineering', 'Sem', '2'),
(83, '3', '31', 'M.Tech [P.E]', 'Master of Technology in Production Engineering', 'Sem', '2'),
(84, '3', '32', 'M.Sc Computer Science', 'Master of Science in Computer Science', 'Sem', '2'),
(85, '3', '32', 'M.Sc Mathematics', 'Master of Science in Mathematics', 'Sem', '2'),
(86, '3', '32', 'M.Sc Physics', 'Master of Science in Physics', 'Sem', '2'),
(87, '3', '32', 'M.Sc Psychology', 'Master of Science in Psychology', 'Sem', '2'),
(88, '3', '32', 'M.Sc Botany', 'Master of Science in Botany', 'Sem', '2'),
(89, '3', '32', 'M.Sc Chemistry', 'Master of Science in Chemistry', 'Sem', '2'),
(90, '3', '33', 'LLM', 'Master of Law', 'Sem', '2'),
(91, '3', '34', 'MBA', 'Master of Business Administration', 'Sem', '3'),
(92, '3', '34', 'PGDM', 'Post Graduate Diploma in Management', 'Sem', '2'),
(93, '3', '34', 'PGDBM', 'Post Graduate Diploma in Business Management', 'Sem', '2'),
(94, '3', '35', 'MA', 'Master of Arts', 'Sem', '2'),
(95, '3', '35', 'MFA', 'Master of Fine Arts', 'Sem', '2'),
(96, '3', '36', 'Fashion Design', 'Fashion Design', 'Year', '2'),
(97, '3', '36', 'MFM', 'Fashion Management', 'Year', '2'),
(98, '3', '36', 'Fashion Business', 'Fashion Business', 'Year', '2'),
(99, '3', '36', 'MBA', 'MBA in Design', 'Year', '2'),
(100, '3', '36', 'Master of Design', 'Master of Design', 'Year', '2'),
(101, '3', '37', 'MD', 'Doctor of Medicine', 'Sem', '3'),
(102, '3', '37', 'MDS', 'Master of Dental Surgery', 'Sem', '2'),
(103, '3', '37', 'MPT', 'Master of Physiotherapy', 'Sem', '2'),
(104, '3', '38', 'M.Ed', 'Master of Education', 'Sem', '2'),
(105, '3', '39', 'Mass Communication', 'Mass Communication', 'Sem', '2'),
(106, '3', '39', 'Media', 'Media', 'Sem', '2'),
(107, '3', '39', 'MJMC', 'Master of Journalism & Mass Communication', 'Sem', '2'),
(108, '3', '40', 'M.Arch [U.D]', 'Master of Architecture in Urban Design', 'Sem', '2'),
(109, '3', '40', 'M.Arch Landscape', 'Master of Architecture in Landscape', 'Sem', '2'),
(110, '3', '41', 'M.Pharm Pharmacology', 'Master of Pharmacy in Pharmacology', 'Sem', '2'),
(111, '3', '41', 'M.Pharm Pharmaceutics', 'Master of Pharmacy in Pharmaceutics', 'Sem', '2'),
(112, '3', '41', 'M.Pharm Pharmaceutical Chemistry', 'Master of Pharmacy in Pharmaceutical Chemistry', 'Sem', '2'),
(113, '3', '42', 'MBA', 'MBA in Travel & Tourism', 'Year', '2'),
(114, '3', '42', 'Diploma', 'Diploma in Travel & Tourism Management', 'Sem', '2'),
(115, '3', '43', 'M.Com', 'Master of Commerce', 'Sem', '2'),
(116, '3', '44', 'MCA', 'Master of Computer Applications', 'Sem', '3'),
(117, '1', '46', 'D.E[AUTO.E]', 'Automobile Engineering', 'Sem', '3'),
(118, '1', '46', 'D.E[BIO.E]', 'Biomedical Engineering', 'Sem', '3'),
(119, '1', '46', 'D.E[CHEMI.E]', 'Chemical Engineering', 'Sem', '3'),
(120, '1', '46', 'D.E[CIVIL.E]', 'Civil Engineering', 'Sem', '3'),
(121, '1', '46', 'D.E[COM.E]', 'Computer Engineering', 'Sem', '3'),
(122, '1', '46', 'D.E[ELE.E]', 'Electrical Engineering', 'Sem', '3'),
(123, '1', '46', 'D.E[E&C.E]', 'Electronics & Communication Engineering', 'Sem', '3'),
(124, '1', '46', 'D.E[ENVI.E]', 'Environment Engineering', 'Sem', '3'),
(125, '1', '46', 'D.E[I.T]', 'Information Technology', 'Sem', '3'),
(126, '1', '46', 'D.E[I&C.E]', 'Instrumentation & Control Engineering', 'Sem', '3'),
(127, '1', '46', 'D.E[MECHA.E]', 'Mechanical Engineering', 'Sem', '3'),
(128, '1', '46', 'D.E[MACHATR.E]', 'Mechatronics Engineering', 'Sem', '3'),
(129, '1', '46', 'D.E[META.E]', 'Metallurgy Engineering', 'Sem', '3'),
(130, '1', '46', 'D.E[MINI.E]', 'Mining Engineering', 'Sem', '3'),
(131, '1', '46', 'D.E[PLASTIC.E]', 'Plastic Engineering', 'Sem', '3'),
(132, '1', '46', 'D.E[POWER.E]', 'Power Electronics', 'Sem', '3'),
(133, '1', '46', 'D.E[TEX.TECH]', 'Textile Processing Technology', 'Sem', '3'),
(134, '1', '46', 'D.E[TEX.MANU.TECH]', 'Textile Manufacturing Technology', 'Sem', '3'),
(135, '1', '46', 'D.E[ARCHI.ASSI]', 'Architectural Assistantship', 'Sem', '3'),
(136, '1', '46', 'D.E[COMP.AID&DRESS MAKING]', 'Computer Aided Costume Design & Dress Making', 'Sem', '3'),
(137, '1', '46', 'D.E[CERA.E]', 'Ceramic Engineering', 'Sem', '3'),
(138, '1', '46', 'D.E[FABRI.TECH]', 'Fabrication Technology', 'Sem', '3'),
(139, '1', '46', 'D.E[PRIN.TECH]', 'Printing Technology', 'Sem', '3'),
(140, '1', '46', 'D.E[TEX.DESI]', 'Textile Designing', 'Sem', '3'),
(141, '1', '46', 'D.E[TRANS.E]', 'Transportation Engineering', 'Sem', '3'),
(142, '2', '16', 'ECE/E & TC', 'Electronics & Telecommunication Engineering/Electronics & Communication Engineering', 'Sem', '4'),
(143, '3', '31', 'Master of comp.application', 'master of computer application', 'Sem', '2');

-- --------------------------------------------------------

--
-- Table structure for table `directory_type`
--

CREATE TABLE `directory_type` (
  `directoryId` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `image` varchar(40) NOT NULL,
  `discription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directory_type`
--

INSERT INTO `directory_type` (`directoryId`, `name`, `image`, `discription`) VALUES
(2, 'Hostel', 'hostal.jpg', 'Find Hostels for Rent and get details of Accommodation and Facilities without any broking charges.'),
(3, 'PG', 'pg.jpg', 'Find PG for Rent and get details of Accommodation and Facilities without any broking charges.'),
(4, 'Classes', 'class.jpg', 'List of coaching classes in various cities and get information for tuition fees, class timings, contact addresses.'),
(5, 'School', 'school.jpg', 'List of coaching School in various cities and get information for tuition fees, class timings, contact addresses.'),
(6, 'Colleges', 'college.jpg', 'List of coaching Colleges in various cities and get information for tuition fees, class timings, contact addresses.'),
(7, 'Books Store', 'books.jpg', 'Best online bookstore and we are offering facilities to buy, sell or search old books of all categories online.');

-- --------------------------------------------------------

--
-- Table structure for table `d_book_store_details`
--

CREATE TABLE `d_book_store_details` (
  `id` int(10) NOT NULL,
  `venderDirectoryId` int(10) NOT NULL,
  `directoryId` int(10) NOT NULL DEFAULT '7',
  `name` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `locality` varchar(30) NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `discription` text NOT NULL,
  `emailId` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `contactNo` varchar(10) NOT NULL,
  `pinCode` int(7) NOT NULL,
  `country` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `latitute` float NOT NULL,
  `longitute` float NOT NULL,
  `views` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_book_store_details`
--

INSERT INTO `d_book_store_details` (`id`, `venderDirectoryId`, `directoryId`, `name`, `address`, `locality`, `startTime`, `endTime`, `discription`, `emailId`, `website`, `contactNo`, `pinCode`, `country`, `state`, `city`, `latitute`, `longitute`, `views`) VALUES
(1, 52, 7, 'Atul Book Store', 'laldarvaja', 'Pritham Rai Road', '08:30:00', '20:00:00', 'here all type of books avaliable', 'stro@gmail.xom', '', '1231231233', 380007, 'India', 'Gujarat', 'Ahmedabad', 23.0225, 72.5714, 12);

-- --------------------------------------------------------

--
-- Table structure for table `d_clg_sch_cls_details`
--

CREATE TABLE `d_clg_sch_cls_details` (
  `id` int(10) NOT NULL,
  `venderDirectoryId` int(10) NOT NULL,
  `directoryId` int(2) NOT NULL,
  `gender` varchar(10) NOT NULL DEFAULT 'Both',
  `name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `locality` varchar(30) NOT NULL,
  `discription` text NOT NULL,
  `emailId` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `contactNo` varchar(10) NOT NULL,
  `pinCode` int(6) NOT NULL,
  `country` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `latitute` float NOT NULL,
  `longitute` float NOT NULL,
  `views` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_clg_sch_cls_details`
--

INSERT INTO `d_clg_sch_cls_details` (`id`, `venderDirectoryId`, `directoryId`, `gender`, `name`, `address`, `locality`, `discription`, `emailId`, `website`, `contactNo`, `pinCode`, `country`, `state`, `city`, `latitute`, `longitute`, `views`) VALUES
(1, 51, 4, 'Both', 'learniphi', 'titenume city center', 'Pritham Rai Road', 'All faculty is experiences\n', 'learniphi@gmail.com', 'www.learniphi.com', '3453453453', 380007, 'India', 'Gujarat', 'Ahmedabad', 0, 0, 7);

-- --------------------------------------------------------

--
-- Table structure for table `d_pg_hostel_details`
--

CREATE TABLE `d_pg_hostel_details` (
  `id` int(10) NOT NULL,
  `venderDirectoryId` int(10) NOT NULL,
  `directoryId` int(2) NOT NULL,
  `gender` varchar(10) NOT NULL DEFAULT 'Both',
  `breakfartEndTime` time NOT NULL,
  `breakfartStartTime` time NOT NULL,
  `lunchStartTime` time NOT NULL,
  `lunchEndTime` time NOT NULL,
  `dinnerStartTime` time NOT NULL,
  `dinnerEndTime` time NOT NULL,
  `services` varchar(250) NOT NULL,
  `fees` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `locality` varchar(30) NOT NULL,
  `discription` text NOT NULL,
  `emailId` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `contactNo` varchar(10) NOT NULL,
  `pinCode` int(6) NOT NULL,
  `country` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `latitute` float NOT NULL,
  `longitute` float NOT NULL,
  `views` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_pg_hostel_details`
--

INSERT INTO `d_pg_hostel_details` (`id`, `venderDirectoryId`, `directoryId`, `gender`, `breakfartEndTime`, `breakfartStartTime`, `lunchStartTime`, `lunchEndTime`, `dinnerStartTime`, `dinnerEndTime`, `services`, `fees`, `name`, `address`, `locality`, `discription`, `emailId`, `website`, `contactNo`, `pinCode`, `country`, `state`, `city`, `latitute`, `longitute`, `views`) VALUES
(10, 48, 2, 'Boys Only', '09:30:02', '08:00:02', '12:00:02', '14:30:02', '18:30:02', '20:30:02', ' WIFI T.V. Fan', 30000, 'Sandipani Hostel', 'L/3/72 Motibag societry', 'Reynolds Road', 'Good atmosphere you feel at home', 'sandipani@gmail.com', 'www.sandipani.com', '9723297232', 400037, 'India', 'Maharashtra', 'Mumbai', 19.0143, 72.8579, 16),
(11, 50, 2, 'Girls Only', '15:42:23', '15:42:23', '15:42:23', '15:42:23', '15:42:23', '15:42:23', 'Refrigerator WIFI', 3453, 'shapt', 'Gdfdgdfgdf', 'Nathalal Parekh Road', '453453453', 'sdfsdfsdf@gmail.com', 'erertert', '3434534534', 400019, 'India', 'Maharashtra', 'Mumbai', 19.0207, 72.856, 3);

-- --------------------------------------------------------

--
-- Table structure for table `d_pg_hostel_services_details`
--

CREATE TABLE `d_pg_hostel_services_details` (
  `id` int(10) NOT NULL,
  `serviceName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_pg_hostel_services_details`
--

INSERT INTO `d_pg_hostel_services_details` (`id`, `serviceName`) VALUES
(1, 'WIFI'),
(2, 'T.V.'),
(3, 'Refrigerator'),
(4, 'Fan');

-- --------------------------------------------------------

--
-- Table structure for table `ebook_comments`
--

CREATE TABLE `ebook_comments` (
  `id` int(10) NOT NULL,
  `bookId` int(10) NOT NULL,
  `userId` int(10) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebook_images`
--

CREATE TABLE `ebook_images` (
  `id` int(10) NOT NULL,
  `bookId` int(10) NOT NULL,
  `name` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebook_likes`
--

CREATE TABLE `ebook_likes` (
  `id` int(10) NOT NULL,
  `bookID` int(10) NOT NULL,
  `userId` int(10) NOT NULL,
  `likes` int(1) NOT NULL DEFAULT '0',
  `ratting` float NOT NULL DEFAULT '0',
  `wishlist` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iNeedShareDirectory`
--

CREATE TABLE `iNeedShareDirectory` (
  `id` int(11) NOT NULL,
  `NeedId` int(11) NOT NULL,
  `venderDirectoryID` int(11) NOT NULL,
  `discription` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_review`
--

CREATE TABLE `student_review` (
  `id` int(20) NOT NULL,
  `mid` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sid` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `descript` blob NOT NULL,
  `dt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_review`
--

INSERT INTO `student_review` (`id`, `mid`, `sid`, `descript`, `dt`) VALUES
(1, '1', '2', 0x4c6f72656d20497073756d2069732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e, '2016-03-09 16:37:33');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `id` int(20) NOT NULL,
  `type` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `syear` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `type`, `category`, `name`, `syear`) VALUES
(2, '2', '2', 'Computer enginnering', 'Sem'),
(3, '2', '4', 'Pharmacy', 'Sem'),
(4, '3', '10', 'English', 'Sem'),
(5, '2', '2', 'Chemicals enginnering', 'Sem'),
(6, '2', '2', 'mechanical engineering', 'Sem'),
(7, '2', '2', 'Electrical engineering', 'Sem'),
(8, '2', '2', 'Automobile engineering', 'Sem'),
(9, '2', '2', 'Biomedical Engineering', ''),
(10, '2', '2', 'Civil Engineering', 'Sem'),
(11, '2', '2', 'Electronics &amp; Communication Engineering', 'Sem'),
(12, '2', '2', 'Environment Engineering', 'Sem'),
(13, '2', '2', 'Information Technology', 'Sem'),
(14, '2', '2', 'Instrumentation &amp; Control Engineering', 'Sem'),
(15, '2', '2', 'Mechatronics Engineering', 'Sem'),
(16, '2', '2', 'Metallurgy Engineering', 'Sem'),
(17, '2', '2', 'Mining Engineering', 'Sem'),
(18, '2', '2', 'Plastic Engineering', 'Sem'),
(19, '2', '2', 'Power Electronics', 'Sem'),
(20, '2', '2', 'Textile Processing Technology', 'Sem'),
(21, '2', '2', 'Textile Manufacturing Technology', 'Sem'),
(22, '2', '2', 'Architectural Assistantship', 'Sem'),
(23, '2', '2', 'Computer Aided Costume Design &amp; Dress Making', 'Sem'),
(24, '2', '2', 'Ceramic Engineering', 'Sem'),
(25, '2', '2', 'Fabrication Technology', 'Sem'),
(26, '2', '2', 'Printing Technology', 'Sem'),
(27, '2', '2', 'Textile Designing', 'Sem'),
(28, '2', '2', 'Transportation Engineering', 'Sem'),
(29, '2', '28', 'Aeronautical Engineering', 'Sem'),
(30, '2', '28', 'Automobile engineering', 'Sem'),
(31, '2', '28', 'Bio Medical Engineering', 'Sem'),
(32, '2', '28', 'Bio- Technology', 'Sem'),
(33, '2', '28', 'Chemical Engineering', 'Sem'),
(34, '2', '28', 'Civil Engineering', 'Sem'),
(35, '2', '28', 'Computer Engineering', 'Sem'),
(36, '2', '28', 'Electrical &amp; Electronics Engineering', 'Sem'),
(37, '2', '28', 'Electrical Engineering', 'Sem'),
(38, '2', '28', 'Electronics Engineering', 'Sem'),
(39, '2', '28', 'Electronics &amp; Communication Engineering', 'Sem'),
(40, '2', '28', 'Electronics &amp; Telecommunication Engineering', 'Sem'),
(41, '2', '28', 'Environmental Engineering', 'Sem'),
(42, '2', '28', 'Food Processing &amp; Technology', 'Sem'),
(43, '2', '28', 'Industrial Engineering', 'Sem'),
(44, '2', '28', 'Information Technology', 'Sem'),
(45, '2', '28', 'Instrumentation &amp; Control Engineering', 'Sem'),
(46, '2', '28', 'Marine Engineering', 'Sem'),
(47, '2', '28', 'Mechanical Engineering', 'Sem'),
(48, '2', '28', 'Mechatronics Engineering', 'Sem'),
(49, '2', '28', 'Metallurgy Engineering', 'Sem'),
(50, '2', '28', 'Mining Engineering', 'Sem'),
(51, '2', '28', 'Plastic Technology', 'Sem'),
(52, '2', '28', 'Power Electronics', 'Sem'),
(53, '2', '28', 'Production Engineering', 'Sem'),
(54, '2', '28', 'Rubber Technology', 'Sem'),
(55, '2', '28', 'Textile Processing', 'Sem'),
(56, '2', '28', 'Textile Technology', 'Sem'),
(57, '2', '28', 'Computer Science &amp; Engineering', 'Sem'),
(58, '2', '28', 'Information &amp; Communication Technology', 'Sem'),
(59, '2', '28', 'Manufacturing Engineering', 'Sem'),
(60, '2', '28', 'Environmental Science And Technology', 'Sem'),
(61, '2', '28', 'Chemical Technology', 'Sem'),
(62, '2', '28', 'Environmental Science &amp; Engineering', 'Sem'),
(63, '2', '29', 'Civil Engineering', 'Sem'),
(64, '2', '29', 'Electrical Engineering', 'Sem'),
(65, '2', '28', 'Mechanical Engineering', 'Sem'),
(66, '2', '29', 'Electronics &amp; Communication Engineering', 'Sem'),
(67, '2', '17', 'Marketing Management ', 'Sem'),
(68, '2', '17', 'Finance Management ', 'Sem'),
(69, '2', '17', 'Human Resource Management ', 'Sem'),
(70, '2', '17', 'Information Systems Management ', 'Sem'),
(71, '2', '17', 'International Business Management ', 'Sem'),
(72, '2', '17', 'Banking &amp; Insurance ', 'Sem'),
(73, '2', '17', 'Rural &amp; Agro-Based Management ', 'Sem'),
(74, '2', '17', 'Asian Business ', 'Sem'),
(75, '2', '17', 'Sustainable Global Enterprise ', 'Sem'),
(76, '2', '17', 'Entrepreneurship and Family Business ', 'Sem'),
(77, '2', '21', 'Quality Assurance and Farm Regulatory Affairs', 'Sem'),
(78, '2', '21', 'Pharmaceutical Management and Regulatory Affairs', 'Sem'),
(79, '2', '29', 'Pharmaceutics', 'Sem'),
(80, '2', '21', 'Pharm. Chemistry', 'Sem'),
(81, '2', '21', 'Pharmaceutical Tech. and Pharmaceutics', 'Sem'),
(82, '2', '21', 'Pharmacology', 'Sem'),
(83, '2', '20', 'Qual. Assurance', 'Sem'),
(84, '2', '21', 'Pharmacognosy', 'Sem'),
(85, '2', '21', 'Clinical Pharmacy', 'Sem'),
(86, '2', '21', 'Pharmaceutical Quality Assurance', 'Sem'),
(87, '2', '21', 'Pharmaceutical Technology', 'Sem'),
(88, '2', '21', 'Pharmacology &amp;Toxicology', 'Sem'),
(89, '2', '21', 'Industrial Pharmacy', 'Sem'),
(90, '2', '21', 'Quality Assurance Technique', ''),
(91, '2', '21', 'Medical Chemistry', 'Sem'),
(92, '2', '21', 'Medical Chemistry', 'Sem'),
(93, '2', '21', 'Pharmaceutical Analysis', 'Sem'),
(94, '2', '20', 'Computer Science &amp;  Engineering', 'Sem'),
(95, '2', '20', 'Computer Engineering', ''),
(96, '2', '20', 'Instrumentation And  Control (Applied Instrumentation)', 'Sem'),
(97, '2', '20', 'Electronics And  Communication Engineering', 'Sem'),
(98, '2', '20', 'Communication Systems Engineering', 'Sem'),
(99, '2', '20', 'Communication  Engineering', 'Sem'),
(100, '2', '20', 'Electrical Engineering', 'Sem'),
(101, '2', '20', 'Mechanical (CAD/CAM)', 'Sem'),
(102, '2', '20', 'Mechanical (Machine Design)', 'Sem'),
(103, '2', '20', 'Mechanical (Cryogenic Engg.)', 'Sem'),
(104, '2', '20', 'Mechanical (I.C.Engine &amp; Automobile) ', 'Sem'),
(105, '2', '20', 'Civil ( Water Resources Management)', 'Sem'),
(106, '2', '20', 'Civil (Transportation Engineering)', 'Sem'),
(107, '2', '20', 'Construction Engineering &amp;Management', 'Sem'),
(108, '2', '20', ' Civil (Casad)', 'Sem'),
(109, '2', '20', 'Chemical Engineering  (Computer Aided Process Design)', 'Sem'),
(110, '2', '20', 'Environmental  Engineering', 'Sem'),
(111, '2', '20', 'Environmental  Management', 'Sem'),
(112, '2', '20', 'Transportation System Engineering', 'Sem'),
(113, '2', '20', 'Civil (Structural Engineering)', 'Sem'),
(114, '2', '20', 'Mechanical (Thermal Engg.)', 'Sem'),
(115, '2', '20', 'Digital Communications', 'Sem'),
(116, '2', '20', 'Information Technology', 'Sem'),
(117, '2', '20', 'Plastic Engineering', 'Sem'),
(118, '2', '20', 'Textile Engineering', 'Sem'),
(119, '2', '20', 'Signal Processing And VLSI Technology (EC)', 'Sem'),
(120, '2', '20', 'Wireless Communication Systems And Networks (EC)', 'Sem'),
(121, '2', '20', 'Mechanical (Production Engineering)', 'Sem'),
(122, '2', '20', 'Power Electronics', 'Sem'),
(123, '2', '20', 'Chemical Engineering', 'Sem'),
(124, '2', '20', 'Bio-Medical Engineering', 'Sem'),
(125, '2', '20', 'Computer Aided Structural Analysis', 'Sem'),
(126, '2', '20', 'Water Resourses Eng. &amp; Management', 'Sem'),
(127, '2', '20', 'Thermal Science', 'Sem'),
(128, '2', '20', 'Automobile Engg.', 'Sem'),
(129, '2', '20', 'Automation &amp; Control', 'Sem'),
(130, '2', '20', 'Power System', 'Sem'),
(131, '2', '20', 'Computer Science &amp; Technology ', 'Sem'),
(132, '2', '20', 'Energy Engineering', 'Sem'),
(133, '2', '20', 'Rubber Engineering', 'Sem'),
(134, '2', '20', 'Signal Processing &amp; Communication', 'Sem'),
(135, '2', '20', 'VLSI System Design', 'Sem'),
(136, '2', '20', 'Civil (Geotechnical Engineering)', 'Sem'),
(137, '2', '20', 'Wireless Communication Technology (EC)', 'Sem'),
(138, '2', '20', 'Power Electronics &amp; Electrical Drives', 'Sem'),
(139, '2', '20', 'Mechanical (Industrial Engineering)', 'Sem'),
(140, '2', '20', 'Mechatronics', 'Sem'),
(141, '2', '20', 'Civil (Town And Country Planning)', 'Sem'),
(142, '2', '20', 'Civil (Construction Management)', 'Sem'),
(143, '2', '20', 'Mechanical (Advance Manufacturing System)', 'Sem'),
(144, '2', '20', 'IT Systems And Network Security', 'Sem'),
(145, '2', '20', 'VLSI &amp; Embedded Systems Design', 'Sem'),
(146, '2', '20', 'Wireless Mobile Computing', 'Sem'),
(147, '2', '20', 'Embedded System', 'Sem'),
(148, '2', '27', 'Engineering', 'Sem'),
(149, '2', '27', 'Pharmacy', 'Sem'),
(150, '2', '27', 'Management', 'Sem'),
(153, '2', '27', 'MCA', 'Sem'),
(152, '2', '27', 'Humanities', 'Sem');

-- --------------------------------------------------------

--
-- Table structure for table `UseriNeed`
--

CREATE TABLE `UseriNeed` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `directoryId` int(11) NOT NULL,
  `discription` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `userId` int(11) NOT NULL,
  `userName` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `country` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `address` varchar(225) NOT NULL,
  `pinCode` varchar(10) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `profilePic` varchar(30) NOT NULL,
  `emailConfirmed` int(1) NOT NULL DEFAULT '0',
  `mobileOTPCode` int(6) NOT NULL,
  `mobileConfirmed` int(11) NOT NULL DEFAULT '0',
  `status` varchar(11) NOT NULL,
  `AcCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`userId`, `userName`, `password`, `mobile`, `fullName`, `email`, `country`, `state`, `city`, `address`, `pinCode`, `gender`, `profilePic`, `emailConfirmed`, `mobileOTPCode`, `mobileConfirmed`, `status`, `AcCreation`) VALUES
(1, 'ashvinkgudaliya', 'ashvin', '9601518691', 'ashvin k gudaliya', 'ashvinkgudaliya@gmail.com', 'india', 'gujarat', 'ahmedabad', 'hoem se bhai nathi devu ja', '364750', 'Male', '', 0, 0, 0, 'Active', '2017-01-23 16:00:33'),
(2, 'ashvin', 'ashvin', '9601518692', 'ashvin k gudaliya', 'ashvinkgudaliya1@gmail.com', 'india', 'gujarat', 'ahmedabad', 'sdghdfjg djfh jdfhjhdf', '364750', 'Male', '', 0, 0, 0, 'Active', '2017-01-23 16:01:55'),
(4, 'akki', 'ashvine', '9876543123', '', 'akki@gmail.com', '', '', '', '', '', '', '', 0, 0, 0, '', '2017-02-26 16:04:10');

-- --------------------------------------------------------

--
-- Table structure for table `vender_details`
--

CREATE TABLE `vender_details` (
  `venderId` int(11) NOT NULL,
  `venderName` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `country` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `address` varchar(225) NOT NULL,
  `locality` varchar(30) NOT NULL,
  `pinCode` varchar(10) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `profilePic` varchar(30) NOT NULL,
  `proHeader` varchar(50) NOT NULL,
  `emailConfirmed` int(1) NOT NULL DEFAULT '0',
  `mobileOTPCode` int(6) NOT NULL,
  `mobileConfirmed` int(11) NOT NULL DEFAULT '0',
  `status` varchar(11) NOT NULL,
  `AcCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vender_details`
--

INSERT INTO `vender_details` (`venderId`, `venderName`, `password`, `mobile`, `fullName`, `email`, `country`, `state`, `city`, `address`, `locality`, `pinCode`, `gender`, `profilePic`, `proHeader`, `emailConfirmed`, `mobileOTPCode`, `mobileConfirmed`, `status`, `AcCreation`) VALUES
(4, 'ashvin', 'ashvin', '9601518691', 'as', 'ashvinKgudaliya@gmail.com', '', '', '', '', '', '', 'Male', '110417064141.', '110417064137.', 0, 0, 0, '', '2017-03-19 08:40:22'),
(5, '', 'test123', '1231231231', 'ashvin Gudaliya', 'test@gmail.com', '', '', '', '', '', '', 'Male', '', 'temp', 0, 0, 0, '', '2017-03-19 12:23:40');

-- --------------------------------------------------------

--
-- Table structure for table `vender_directory`
--

CREATE TABLE `vender_directory` (
  `venderDirectoryId` int(10) NOT NULL,
  `venderId` int(10) NOT NULL,
  `directoryId` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vender_directory`
--

INSERT INTO `vender_directory` (`venderDirectoryId`, `venderId`, `directoryId`) VALUES
(48, 4, 2),
(50, 4, 2),
(51, 4, 4),
(52, 4, 7);

-- --------------------------------------------------------

--
-- Table structure for table `vender_directory_comments`
--

CREATE TABLE `vender_directory_comments` (
  `id` int(10) NOT NULL,
  `venderDirectoryId` int(10) NOT NULL,
  `userId` int(10) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vender_directory_comments`
--

INSERT INTO `vender_directory_comments` (`id`, `venderDirectoryId`, `userId`, `comment`, `time`) VALUES
(13, 48, 0, 'Test message', '2017-03-25 13:18:20'),
(14, 48, 0, 'Jkhkvjhdf', '2017-03-25 13:18:55'),
(15, 48, 0, 'Sgfssdf', '2017-03-25 13:19:40'),
(17, 48, 5, 'i am user', '2017-03-25 13:30:48'),
(19, 48, 5, 'i am user', '2017-03-25 13:30:53'),
(20, 48, 0, 'Testing', '2017-03-25 13:33:34'),
(21, 50, 0, 'T', '2017-03-25 13:34:57'),
(22, 48, 0, 'This is a long message try to underspend \n\nFor just testing per post to send this message', '2017-03-25 13:41:09'),
(29, 48, 0, '@the hostal: tsduyftusydf', '2017-03-25 14:23:12'),
(30, 48, 0, 'Rhfgjhgdfs', '2017-03-25 14:23:18'),
(31, 50, 0, '@hosdfj: ', '2017-03-25 14:23:29'),
(32, 48, 0, 'This is test', '2017-03-25 18:39:33'),
(33, 48, 0, 'Y', '2017-03-25 18:40:02'),
(34, 48, 0, 'Gg', '2017-03-25 19:31:17'),
(35, 48, 0, 'Tt', '2017-03-25 19:47:57'),
(36, 51, 0, 'Te', '2017-03-26 12:02:24'),
(45, 50, 0, '@hosdfj: ', '2017-03-26 12:06:24'),
(46, 50, 0, 'Uu', '2017-03-26 12:10:27'),
(47, 50, 0, 'Hhhh', '2017-03-26 12:10:35'),
(48, 48, 0, '@Deactivate User: ', '2017-03-26 12:20:45'),
(49, 50, 0, 'Gjhg', '2017-03-26 19:04:07'),
(50, 48, 10, '', '2017-03-29 17:48:46'),
(51, 48, 0, 'Fsjdfhkjsd', '2017-03-30 15:08:48'),
(52, 48, 0, 'Dd', '2017-03-30 15:08:59'),
(53, 48, 10, '', '2017-03-30 16:30:42'),
(54, 48, 10, 'Etts', '2017-03-30 16:40:37'),
(55, 48, 1, 'This is a', '2017-03-30 18:45:35'),
(56, 48, 1, 'C', '2017-03-30 18:46:13'),
(57, 48, 1, '@ashvinkgud', '2017-03-30 19:11:21'),
(58, 48, 1, 'Tetete', '2017-04-03 18:40:40'),
(59, 48, 1, '@Deactivate User: ', '2017-04-03 18:41:02'),
(60, 48, 1, 'Erere', '2017-04-03 20:28:37'),
(61, 51, 1, 'Test', '2017-04-05 19:32:56'),
(62, 52, 1, 'No comments', '2017-04-05 19:48:42'),
(63, 52, 1, 'Hi', '2017-04-08 06:08:34'),
(64, 48, 0, 'Hi', '2017-04-08 06:09:52'),
(65, 48, 0, 'Hello buddy', '2017-04-11 16:04:09');

-- --------------------------------------------------------

--
-- Table structure for table `vender_directory_images`
--

CREATE TABLE `vender_directory_images` (
  `id` int(10) NOT NULL,
  `venderDirectoryId` int(10) NOT NULL,
  `name` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vender_directory_images`
--

INSERT INTO `vender_directory_images` (`id`, `venderDirectoryId`, `name`) VALUES
(33, 48, '110417061556.'),
(34, 48, '110417061651.'),
(36, 48, '110417062151.'),
(37, 51, '110417062357.'),
(38, 51, '110417062637.'),
(39, 52, '110417062719.'),
(40, 52, '110417062723.'),
(41, 52, '110417062728.'),
(42, 50, '110417063930.');

-- --------------------------------------------------------

--
-- Table structure for table `vender_directory_likes`
--

CREATE TABLE `vender_directory_likes` (
  `id` int(10) NOT NULL,
  `venderDirectoryId` int(10) NOT NULL,
  `userId` int(10) NOT NULL,
  `likes` int(1) NOT NULL DEFAULT '0',
  `ratting` float NOT NULL DEFAULT '0',
  `wishlist` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vender_directory_likes`
--

INSERT INTO `vender_directory_likes` (`id`, `venderDirectoryId`, `userId`, `likes`, `ratting`, `wishlist`) VALUES
(1, 1, 1, 1, 3, 1),
(3, 1, 3, 1, 3, 0),
(4, 1, 2, 0, 4, 0),
(5, 0, 0, 0, 0, 0),
(7, 52, 1, 1, 3, 1),
(8, 50, 11, 0, 4, 0),
(9, 48, 10, 1, 0, 0),
(12, 48, 1, 1, 3, 1),
(13, 50, 1, 1, 3, 0),
(14, 51, 1, 1, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vender_directry_standard`
--

CREATE TABLE `vender_directry_standard` (
  `id` int(10) NOT NULL,
  `venderDirectoryId` int(10) NOT NULL,
  `directoryId` int(10) NOT NULL,
  `subCategory` int(40) NOT NULL,
  `subSubCategory` int(11) NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `fees` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vender_directry_standard`
--

INSERT INTO `vender_directry_standard` (`id`, `venderDirectoryId`, `directoryId`, `subCategory`, `subSubCategory`, `startTime`, `endTime`, `fees`) VALUES
(1, 3, 6, 0, 1, '00:00:00', '00:00:00', 0),
(6, 1, 5, 0, 56, '00:00:00', '00:00:00', 0),
(7, 1, 5, 0, 2, '00:00:00', '00:00:00', 0),
(12, 51, 4, 28, 74, '12:30:40', '17:30:40', 7000),
(13, 51, 4, 31, 143, '18:30:28', '21:30:28', 4000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_sub`
--
ALTER TABLE `category_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_sub_sub`
--
ALTER TABLE `category_sub_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `directory_type`
--
ALTER TABLE `directory_type`
  ADD PRIMARY KEY (`directoryId`);

--
-- Indexes for table `d_book_store_details`
--
ALTER TABLE `d_book_store_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `d_clg_sch_cls_details`
--
ALTER TABLE `d_clg_sch_cls_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `d_pg_hostel_details`
--
ALTER TABLE `d_pg_hostel_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `d_pg_hostel_services_details`
--
ALTER TABLE `d_pg_hostel_services_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_review`
--
ALTER TABLE `student_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `vender_details`
--
ALTER TABLE `vender_details`
  ADD PRIMARY KEY (`venderId`);

--
-- Indexes for table `vender_directory`
--
ALTER TABLE `vender_directory`
  ADD PRIMARY KEY (`venderDirectoryId`);

--
-- Indexes for table `vender_directory_comments`
--
ALTER TABLE `vender_directory_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vender_directory_images`
--
ALTER TABLE `vender_directory_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vender_directory_likes`
--
ALTER TABLE `vender_directory_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vender_directry_standard`
--
ALTER TABLE `vender_directry_standard`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `category_sub`
--
ALTER TABLE `category_sub`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `category_sub_sub`
--
ALTER TABLE `category_sub_sub`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `directory_type`
--
ALTER TABLE `directory_type`
  MODIFY `directoryId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `d_book_store_details`
--
ALTER TABLE `d_book_store_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `d_clg_sch_cls_details`
--
ALTER TABLE `d_clg_sch_cls_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `d_pg_hostel_details`
--
ALTER TABLE `d_pg_hostel_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `d_pg_hostel_services_details`
--
ALTER TABLE `d_pg_hostel_services_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `student_review`
--
ALTER TABLE `student_review`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `vender_details`
--
ALTER TABLE `vender_details`
  MODIFY `venderId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `vender_directory`
--
ALTER TABLE `vender_directory`
  MODIFY `venderDirectoryId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `vender_directory_comments`
--
ALTER TABLE `vender_directory_comments`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `vender_directory_images`
--
ALTER TABLE `vender_directory_images`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `vender_directory_likes`
--
ALTER TABLE `vender_directory_likes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `vender_directry_standard`
--
ALTER TABLE `vender_directry_standard`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
