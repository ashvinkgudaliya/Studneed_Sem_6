<?php 
include "../Connection.php";
include "../function.php";

if($_SERVER['REQUEST_METHOD'] == "POST"){

	if (isset($_POST['directoryId']) && isset($_POST['pinCode'])){
		$table = getDirectoryTableName($_POST['directoryId']);
		$data = getVenderDirectoryDataImageWith("SELECT * from $table where pinCode = $_POST[pinCode] and directoryId = $_POST[directoryId]");
		$json = array("status" => 200, "details" => $data);
	}
	else if (isset($_POST['directoryId']) && isset($_POST['city'])){
		$table = getDirectoryTableName($_POST['directoryId']);
		$data = getVenderDirectoryDataImageWith("SELECT * from $table where city = $_POST[city] and directoryId = $_POST[directoryId]");
		$json = array("status" => 200, "details" => $data);
	}
	else if (isset($_POST['directoryId']) && isset($_POST['standard'])){
		$standard = fetchMultipleData("SELECT * from vender_directry_standard where subSubCategory = $_POST[standard] and directoryId = $_POST[directoryId]");
		$data = array();
		for ($i=0; $i < count($standard); $i++) { 
			$table = getDirectoryTableName($standard[$i]['directoryId']);
			$venderDirectoryId = $standard[$i]['venderDirectoryId'];
			$venderData = getVenderDirectoryDataImageWith("SELECT * from $table where venderDirectoryId = $venderDirectoryId and directoryId = $_POST[directoryId]");
			array_push($data, $venderData[0]);
		}
		$json = array("status" => 200, "details" => $data);
	}
	else{
		$json = array("status" => 400, "message" => "field is requred");
	}
}
else{
	$json = array("status" => 400, "message" => "field is requred");
}

header('Content-type: application/json');
echo json_encode($json);
?>
