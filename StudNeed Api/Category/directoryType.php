<?php 
include "../Connection.php";
if($_SERVER['REQUEST_METHOD'] == "POST"){
	$arrCategory = array();
	if ($result = mysqli_query($link,"SELECT * FROM directory_type")) {

	    while ($row = $result->fetch_assoc()) {
	    	$data = array(
				"directoryId" => intval($row["directoryId"]),
				"name" => $row["name"],
				"image" => $row["image"] == "" ?"":$row["image"],
				"discription" => $row["discription"]
			);
			array_push($arrCategory,$data);
	    }
	    $result->free();
	    $json = array("status" => 200, "directoryType" => $arrCategory);
	}
	else{
		$json = array("status" => 404, "message" => "Data Not Found");
	}
}
else{
	$json = array("status" => 400, "message" => "field is requred");
}


header('Content-type: application/json');
	echo json_encode($json);
?>
	