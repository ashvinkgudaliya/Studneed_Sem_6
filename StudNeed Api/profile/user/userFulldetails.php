π<?php 
include "../../Connection.php";
include "../../function.php";

if($_SERVER['REQUEST_METHOD'] == "POST"){

	if (isset($_POST['userId']) && isset($_POST['likes'])){
		$likes = fetchMultipleData("SELECT * FROM vender_directory_likes WHERE userId = $_POST[userId] and likes = 1");
		for ($i=0; $i < count($likes) ; $i++) { 
			$likes[$i]['directoryData'] = getVenderDirectoryDataUseing($likes[$i]['venderDirectoryId']);
		}
		$json = array("status" => 200, "userLikedData" => $likes);
	}
	if (isset($_POST['userId']) && isset($_POST['wishlist'])){
		$likes = fetchMultipleData("SELECT * FROM vender_directory_likes WHERE userId = $_POST[userId] and wishlist = 1");
		for ($i=0; $i < count($likes) ; $i++) { 
			$likes[$i]['directoryData'] = getVenderDirectoryDataUseing($likes[$i]['venderDirectoryId']);
		}
		$json = array("status" => 200, "userWishlistData" => $likes);
	}
	else if (isset($_POST['userId'])){
		$user = fetchTableSingleData("SELECT * FROM user_details WHERE userId = $_POST[userId]");
		$user['totalLikes'] = count(fetchMultipleData("SELECT * FROM vender_directory_likes WHERE userId = $_POST[userId] and likes = 1"));
		$user['wishlist'] = count(fetchMultipleData("SELECT * FROM vender_directory_likes WHERE userId = $_POST[userId] and wishlist = 1"));
		$json = array("status" => 200, "userFullDetails" => $user);
	}
	else{
		$json = array("status" => 400, "message" => "field is requred");
	}
}
else{
	$json = array("status" => 400, "message" => "field is requred");
}

header('Content-type: application/json');
echo json_encode($json);

?>
