<?php
include "../../Connection.php";
include "../../function.php";

error_reporting(E_ALL);
ini_set('display_errors', 1);

if($_SERVER['REQUEST_METHOD'] == "POST"){

	if ((!empty($_FILES["file"]) && isset($_POST['venderDirectoryId'])))
	{
		$name     = $_FILES['file']['name'];
		$tmpName  = $_FILES['file']['tmp_name'];
		$error    = $_FILES['file']['error'];
		$size     = $_FILES['file']['size'];
		$ext      = strtolower(pathinfo($name, PATHINFO_EXTENSION));
	   
		switch ($error) 
		{
			case UPLOAD_ERR_OK:
				if ($size/1024 > 10 * 1024 ) {
					$json = array("status" => 400, "message" => "Size To larg");
				}
				else{
					$ext = pathinfo($name, PATHINFO_EXTENSION);
					$rand = date("dmyhis") . "." . $ext;
					$targetPath =   '../../images' . DIRECTORY_SEPARATOR. $rand;
					$_SESSION['file'] = $targetPath;
					move_uploaded_file($tmpName,$targetPath);
	
					$resultat = mysqli_query($link,"insert into vender_directory_images values ('', '$_POST[venderDirectoryId]', '$rand')");
				    if ($resultat) {
				    	$json = array("status" => 200, "message" => "Sucessfully Images Uploaded", "Images" => fetchMultipleData("select * from vender_directory_images where venderDirectoryId = $_POST[venderDirectoryId]"));
				    }
				    else{
						$json = array("status" => 400, "message" => "Somthing is Wrong Try Agin");
				    }
				}
				break;
			default:
				$json = array("status" => 400, "message" => "Unknown error");
			break;
		}
	}
	else if (isset($_POST['imageid'])){
		if (mysqli_query($link,"DELETE FROM vender_directory_images WHERE id = $_POST[imageid]")){
			$json = array("status" => 200, "message" => "Images Delete Sucessfully");
		}
		else{
			$json = array("status" => 400, "message" => "Somthing is Wrong Try Agin");
		}
	}
	else
	{
		$json = array("status" => 400, "message" => "Parameter required");
	}
}
else{
	$json = array("status" => 400, "message" => "Parameter required");
}

header('Content-type: application/json');
echo json_encode($json);
?>