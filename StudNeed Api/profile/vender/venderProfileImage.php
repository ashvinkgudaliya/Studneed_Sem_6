<?php
include "../../Connection.php";
include "../../function.php";
error_reporting(E_ALL);
ini_set('display_errors', 1);

if($_SERVER['REQUEST_METHOD'] == "POST"){

	if (isset($_POST['venderId']))
	{
		if (!empty($_FILES["profilePic"])) {
			$name     = $_FILES['profilePic']['name'];
			$tmpName  = $_FILES['profilePic']['tmp_name'];
			$error    = $_FILES['profilePic']['error'];
			$size     = $_FILES['profilePic']['size'];

			switch ($error) 
			{
				case UPLOAD_ERR_OK:
					if ($size/1024 > 10 * 1024 ) {
						$json = array("status" => 400, "message" => "Size To larg");
					}
					else{
						$ext = pathinfo($name, PATHINFO_EXTENSION);
						$rand = date("dmyhis") . "." . $ext;
						$targetPath =   '../../images' . DIRECTORY_SEPARATOR. $rand;
						move_uploaded_file($tmpName,$targetPath);
		
						$resultat = mysqli_query($link,"UPDATE vender_details SET profilePic = '$rand' WHERE venderId = $_POST[venderId]");
					    if ($resultat) {

					    	$json = array("status" => 200, "message" => "Sucessfully Uploaded","venderData" => fetchTableSingleData("SELECT * from vender_details WHERE venderId = $_POST[venderId]"));
					    }
					    else{
							$json = array("status" => 400, "message" => mysqli_error($link));
					    }
					}
					break;
				default:
					$json = array("status" => 400, "message" => "Unknown error");
				break;
			}
		}
		else if (!empty($_FILES["proHeader"])) {
			$name     = $_FILES['proHeader']['name'];
			$tmpName  = $_FILES['proHeader']['tmp_name'];
			$error    = $_FILES['proHeader']['error'];
			$size     = $_FILES['proHeader']['size'];

			switch ($error) 
			{
				case UPLOAD_ERR_OK:
					if ($size/1024 > 10 * 1024 ) {
						$json = array("status" => 400, "message" => "Size To larg");
					}
					else{
						$ext = pathinfo($name, PATHINFO_EXTENSION);
						$rand = date("dmyhis") . "." . $ext;
						$targetPath =   '../../images' . DIRECTORY_SEPARATOR. $rand;
						move_uploaded_file($tmpName,$targetPath);
		
						$resultat = mysqli_query($link,"UPDATE vender_details SET proHeader = '$rand' WHERE venderId = $_POST[venderId]");
					    if ($resultat) {
					    	$json = array("status" => 200, "message" => "Sucessfully Uploaded","venderData" => fetchTableSingleData("SELECT * from vender_details WHERE venderId = $_POST[venderId]"));
					    }
					    else{
							$json = array("status" => 400, "message" => "Somthing is Wrong Try Agin");
					    }
					}
					break;
				default:
					$json = array("status" => 400, "message" => "Unknown error");
				break;
			}
		}
		else{
			$json = array("status" => 400, "message" => "File Not selected");
		}
	}
	else
	{
		$json = array("status" => 400, "message" => "Enter venderId");
	}
}
else{
	$json = array("status" => 400, "message" => "Parameter required");
}

header('Content-type: application/json');
echo json_encode($json);
?>