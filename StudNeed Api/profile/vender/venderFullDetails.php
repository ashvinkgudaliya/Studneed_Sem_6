<?php 
include "../../Connection.php";
include "../../function.php";

if($_SERVER['REQUEST_METHOD'] == "POST"){

	if (isset($_POST['venderId']) && isset($_POST['userId'])){
		//vender with user action
		$vender = fetchTableSingleData("SELECT * FROM vender_details WHERE venderId = $_POST[venderId]");
		$vender['venderTypeDetails'] = getVenderDataWithUserAction($_POST['venderId'],$_POST['userId']);
		$json = array("status" => 200, "venderFullDetails" => $vender);
	}
	else if (isset($_POST['venderId'])){
		$vender = fetchTableSingleData("SELECT * FROM vender_details WHERE venderId = $_POST[venderId]");
		$vender['venderTypeDetails'] = getVenderData($_POST['venderId']);
		$json = array("status" => 200, "venderFullDetails" => $vender);
	}
	else{
		$json = array("status" => 400, "message" => "field is requred");
	}
}
else{
	$json = array("status" => 400, "message" => "field is requred");
}

header('Content-type: application/json');
echo json_encode($json);

function getVenderData($venderId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
	$venderDirectry = fetchMultipleData("select * from vender_directory where venderId = $venderId");
	for ($i=0; $i < count($venderDirectry); $i++) { 
		$venderDirectry[$i]['directoryData'] = getDirectoryIdWithVenderDirectoryId($venderDirectry[$i]['venderDirectoryId'],$venderDirectry[$i]['directoryId']);
		$venderDirectry[$i]['directoryAction'] = getVenderDirectoryDetils($venderDirectry[$i]['venderDirectoryId']);
		
		$venderDirectoryId = $venderDirectry[$i]['venderDirectoryId'];
		$directoryId = $venderDirectry[$i]['directoryId'];
		$venderDirectry[$i]['directoryStandard'] = fetchMultipleData("SELECT * from vender_directry_standard where venderDirectoryId=$venderDirectoryId and directoryId=$directoryId");
	}
	return $venderDirectry;
}

function getVenderDataWithUserAction($venderId,$userId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
	$venderDirectry = fetchMultipleData("select * from vender_directory where venderId = $venderId");
	for ($i=0; $i < count($venderDirectry); $i++) { 
		$venderDirectry[$i]['directoryData'] = getDirectoryIdWithVenderDirectoryId($venderDirectry[$i]['venderDirectoryId'],$venderDirectry[$i]['directoryId']);
		$venderDirectry[$i]['userAction']['bLiked'] = userLikedAction($venderDirectry[$i]['venderDirectoryId'],$userId);
		$venderDirectry[$i]['userAction']['userRating'] = getUserRattingAction($venderDirectry[$i]['venderDirectoryId'],$userId);
		$venderDirectry[$i]['userAction']['wishlist'] = getUserWishlistAction($venderDirectry[$i]['venderDirectoryId'],$userId);
		$venderDirectry[$i]['directoryAction'] = getVenderDirectoryDetils($venderDirectry[$i]['venderDirectoryId']);

		$venderDirectoryId = $venderDirectry[$i]['venderDirectoryId'];
		$directoryId = $venderDirectry[$i]['directoryId'];
		// $venderDirectry[$i]['directoryStandard'] = fetchMultipleData("SELECT * from vender_directry_standard where venderDirectoryId=$venderDirectoryId and directoryId=$directoryId");
	}
	return $venderDirectry;
}

function getVenderDirectoryDetils($venderDirectoryId){
	$vender['likeCount'] = getLikesCount($venderDirectoryId);
	$vender['images'] = fetchMultipleData("select * from vender_directory_images where venderDirectoryId = $venderDirectoryId");
	$vender['commentCount'] = getCommentCount($venderDirectoryId);
	// $vender['comment'] = getCommentWithUserName($venderDirectoryId);
	$vender['rating'] = getRattingCount($venderDirectoryId);
	return $vender;
}
?>
