<?php

function getVenderDirectoryDataImageWith($query){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");

	$mainArr = array();

	$result = mysqli_query($link,$query);
	$count = mysqli_num_rows($result);

	if ($count > 0){
		$finfo = mysqli_fetch_fields($result);
	    while ($row = $result->fetch_assoc()) {
	    	$data = array();
	    	foreach ($finfo as $val) {
	    		$data[$val->name] = $row[$val->name];
	        }
	        $data['images'] = getVenderDirectoryImage($data['venderDirectoryId']);
	        $data['likes'] = getLikesCount($data['venderDirectoryId']);
	        $data['comment'] = getCommentCount($data['venderDirectoryId']);
	        $data['rating'] = getRattingCount($data['venderDirectoryId']);
	        $data['venderName'] = getVenderName($data['venderDirectoryId']);
			array_push($mainArr,$data);
	    }
	}
	return $mainArr;
}

function fetchMultipleData($query){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");

	$mainArr = array();

	$result = mysqli_query($link,$query);
	$count = mysqli_num_rows($result);

	if ($count > 0){
		$finfo = mysqli_fetch_fields($result);
	    while ($row = $result->fetch_assoc()) {
	    	$data = array();
	    	foreach ($finfo as $val) {
	    		$data[$val->name] = $row[$val->name];
	        }
			array_push($mainArr,$data);
	    }
	}
	return $mainArr;
}

function fetchTableSingleData($query){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
   
	$result = mysqli_query($link,$query);
	$count = mysqli_num_rows($result);

	if ($count == 1){
		$finfo = mysqli_fetch_fields($result);
	    while ($row = $result->fetch_assoc()) {
	    	$data = array();
	    	foreach ($finfo as $val) {
	    		$data[$val->name] = $row[$val->name];
	        }
			return $data;
	    }
	}
	return array();
}

//Only one Imgaes loadded
function getVenderDirectoryImage($venderDirectoryId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
	$result = mysqli_query($link,"select * from vender_directory_images where venderDirectoryId = '$venderDirectoryId'");
	$count = mysqli_num_rows($result);
	if ($count > 0){
		while($row = $result->fetch_assoc())
		{
			return	$row["name"] == "" ?"":$row["name"];
		}
	}
	else{
		return "";
	}
}

function getVenderDirectoryMultiImage($venderDirectoryId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
	$result = mysqli_query($link,"select * from vender_directory_images where venderDirectoryId = '$venderDirectoryId'");
	$img = array();
	$count = mysqli_num_rows($result);
	if ($count > 0){
		while($row = $result->fetch_assoc())
		{
			array_push($img, $row["name"] == "" ? "":$row["name"]);
		}
		return $img;
	}
	return array();
}

function getVenderName($venderDirectoryId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
	$venderDirectry = fetchTableSingleData("select * from vender_directory where venderDirectoryId = $venderDirectoryId");
	$data = fetchTableSingleData("select * from vender_details where venderId = $venderDirectry[venderId]");
	return $data['venderName'] == null ? "" : $data['venderName'];
}

function getLikesCount($venderDirectoryId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
	$result = mysqli_query($link,"select * from vender_directory_likes where venderDirectoryId = $venderDirectoryId and likes = 1");
	return mysqli_num_rows($result);
}

function userLikedAction($venderDirectoryId,$userId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
	$query = "select * from vender_directory_likes where venderDirectoryId = $venderDirectoryId and userId = $userId";
	$result = mysqli_query($link,$query);
	while($row = $result->fetch_assoc()){
		return $row['likes'];
	}
	return "0";
}

function getCommentCount($venderDirectoryId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
	$query = "select * from vender_directory_comments where venderDirectoryId = $venderDirectoryId";
	return mysqli_num_rows(mysqli_query($link,$query));
}

function getCommentWithUserName($venderDirectoryId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");

	$query = "select * from vender_directory_comments where venderDirectoryId = $venderDirectoryId ORDER BY time DESC";
	$mainArr = array();

	$result = mysqli_query($link,$query);
	$count = mysqli_num_rows($result);

	if ($count > 0){
		$finfo = mysqli_fetch_fields($result);
	    while ($row = $result->fetch_assoc()) {
	    	$data = array();
	    	foreach ($finfo as $val) {
	    		$data[$val->name] = $row[$val->name];
	        }
	        $data['userName'] = getUserName($data['userId']);
	        $data['userProfile'] =  getUserImage($data['userId']);
			array_push($mainArr,$data);
	    }
	}
	return $mainArr;
}

function getUserName($userId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
	$query = "select * from user_details where userId = $userId";
	$result = mysqli_query($link,$query);
	while($row = $result->fetch_assoc()){
		return $row['userName'];
	}
	return "Deactivate User";
}

function getUserImage($userId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
	$query = "select * from user_details where userId = $userId";
	$result = mysqli_query($link,$query);
	while($row = $result->fetch_assoc()){
		return $row['profilePic'];
	}
	return "defaultUser.jpeg";
}

function getUserWishlistAction($venderDirectoryId,$userId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
	$result = mysqli_query($link,"select * from vender_directory_likes where venderDirectoryId = $venderDirectoryId and userId = $userId");
	$row = mysqli_fetch_assoc($result); 
	$count = mysqli_num_rows($result);
	return $count == 0 ? "0" : $row['wishlist'];
}

function getUserRattingAction($venderDirectoryId,$userId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");
	$result = mysqli_query($link,"select ratting as rate from vender_directory_likes where venderDirectoryId = $venderDirectoryId and userId = $userId");
	$row = mysqli_fetch_assoc($result); 
	$count = mysqli_num_rows($result);
	return $count == 0 ? "0" : $row['rate'];
}

function getRattingCount($venderDirectoryId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");

	$result = mysqli_query($link,"select sum(ratting) as rattingSum from vender_directory_likes where venderDirectoryId = $venderDirectoryId");
	$row = mysqli_fetch_assoc($result); 
	$rattingTotal = $row['rattingSum'];

	$result = mysqli_query($link,"select * from vender_directory_likes where venderDirectoryId = $venderDirectoryId && ratting != 0");
	$count = mysqli_num_rows($result);
	return  $count == 0 ? 0 : ($rattingTotal/$count);
}

function rattingCount($venderDirectoryId){
	$link = mysqli_connect("localhost", "root", "root", "gujju_StudNeed");

	$result = mysqli_query($link,"select sum(ratting) as rattingSum from vender_directory_likes where venderDirectoryId = $venderDirectoryId");
	$row = mysqli_fetch_assoc($result); 
	$rattingTotal = $row['rattingSum'];

	$result = mysqli_query($link,"select * from vender_directory_likes where venderDirectoryId = $venderDirectoryId && ratting != 0");
	$count = mysqli_num_rows($result);

	$totalRatting = $count == 0 ? 0 : ($rattingTotal/$count);
	
	$result = mysqli_query($link,"select count(ratting) as rattingSum from vender_directory_likes where venderDirectoryId = $venderDirectoryId && ratting = 1");
	$row = mysqli_fetch_assoc($result); 
	$one = $row['rattingSum'];
	
	$result = mysqli_query($link,"select count(ratting) as rattingSum from vender_directory_likes where venderDirectoryId = $venderDirectoryId && ratting = 2");
	$row = mysqli_fetch_assoc($result); 
	$two = $row['rattingSum'];
	
	$result = mysqli_query($link,"select count(ratting) as rattingSum from vender_directory_likes where venderDirectoryId = $venderDirectoryId && ratting = 3");
	$row = mysqli_fetch_assoc($result); 
	$there = $row['rattingSum'];
	
	$result = mysqli_query($link,"select count(ratting) as rattingSum from vender_directory_likes where venderDirectoryId = $venderDirectoryId && ratting = 4");
	$row = mysqli_fetch_assoc($result); 
	$four = $row['rattingSum'];
	
	$result = mysqli_query($link,"select count(ratting) as rattingSum from vender_directory_likes where venderDirectoryId = $venderDirectoryId && ratting = 5");
	$row = mysqli_fetch_assoc($result); 
	$five = $row['rattingSum'];
	
	return array(
		'one' => $one,
		'two' => $two,
		'there' => $there,
		'four' => $four,
		'five' => $five,
		'rattingCount' => $count,
		'totalRatting' => $totalRatting
	);
}

function getFullDataWithUserAction($data,$userId){
	if ($data != null){
		$data['bLiked'] = userLikedAction($data['venderDirectoryId'],$userId);
		$data['userRating'] = getUserRattingAction($data['venderDirectoryId'],$userId);
		$data['wishlist'] = getUserWishlistAction($data['venderDirectoryId'],$userId);
		// $data['venderDetails'] = getVenderDirectoryIdDetils($data['venderDirectoryId']);
		$venderDirectoryId = $data['venderDirectoryId'];
		$venderDirectry = fetchTableSingleData("select * from vender_directory where venderDirectoryId = $venderDirectoryId");
		$data['venderDetails'] = fetchTableSingleData("select * from vender_details where venderId = $venderDirectry[venderId]");
		$data['directryType'] = fetchTableSingleData("select * from directory_type where directoryId = $venderDirectry[directoryId]");
		$data['likeCount'] = getLikesCount($venderDirectoryId);
		$data['images'] = getVenderDirectoryMultiImage($venderDirectoryId);
		$data['commentCount'] = getCommentCount($venderDirectoryId);
		$data['comment'] = getCommentWithUserName($venderDirectoryId);
		$data['rating'] = rattingCount($venderDirectoryId);

		return $data;
 	}
 	return $data;
}

function getVenderDirectoryIdDetils($venderDirectoryId){
	$venderDirectry = fetchTableSingleData("select * from vender_directory where venderDirectoryId = $venderDirectoryId");
	$vender['venderDetails'] = fetchTableSingleData("select * from vender_details where venderId = $venderDirectry[venderId]");
	$vender['directryType'] = fetchTableSingleData("select * from directory_type where directoryId = $venderDirectry[directoryId]");
	$vender['likeCount'] = getLikesCount($venderDirectoryId);
	$vender['images'] = fetchMultipleData("select * from vender_directory_images where venderDirectoryId = $venderDirectoryId");
	$vender['commentCount'] = getCommentCount($venderDirectoryId);
	$vender['comment'] = getCommentWithUserName($venderDirectoryId);
	$vender['rating'] = rattingCount($venderDirectoryId);
	return $vender;
}

function getDirectoryIdWithVenderDirectoryId($venderDirectoryId,$directoryId){
	$table = getDirectoryTableName($directoryId);

	$data = fetchTableSingleData("select * from $table where venderDirectoryId = $venderDirectoryId");
	$data['images'] = fetchMultipleData("select * from vender_directory_images where venderDirectoryId = $venderDirectoryId");
	return $data;
}

function getVenderDirectoryDataUseing($venderDirectoryId){
	$venderDirectry = fetchTableSingleData("select * from vender_directory where venderDirectoryId = $venderDirectoryId");
	return getDirectoryIdWithVenderDirectoryId($venderDirectoryId,$venderDirectry['directoryId']);
}

function getDirectoryTableName($directoryId){
	if ($directoryId == "7") {
		return "d_book_store_details";
	}
	else if ($directoryId == "2" || $directoryId == "3"){
		return "d_pg_hostel_details";
	}
	else if ($directoryId == "4" || $directoryId == "5" || $directoryId == "6"){
		return "d_clg_sch_cls_details";
	}
}

?>