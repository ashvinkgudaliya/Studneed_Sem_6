//
//  ColorListCell.swift
//  Former-Demo
//
//  Created by Ryo Aoyama on 11/8/15.
//  Copyright © 2015 Ryo Aoyama. All rights reserved.
//

import UIKit

final class ImagesListCell: UITableViewCell {
 
    // MARK: Public
    
    var imagesArray = [String]()
    var onColorSelected: ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure()
    }
    
    func select(item: Int, animated: Bool = false) {
        let indexPath = IndexPath(item: item, section: 0)
        collectionView.selectItem(at: indexPath, animated: animated, scrollPosition: [])
    }
    
    // MARK: Private
    
    @IBOutlet weak var flowLayOut: UICollectionViewFlowLayout!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private func configure() {
        selectionStyle = .none
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "ColorCell")
    }
}

extension ImagesListCell: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let length = collectionView.bounds.height
        return CGSize(width: length, height: length)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCell", for: indexPath) 
        
        if cell.viewWithTag(101) != nil {
            let images = cell.viewWithTag(101) as! UIImageView
            images.removeFromSuperview()
        }
        
        let images = UIImageView(frame: CGRect(x: 5, y: 5, width: cell.frame.width - 10, height: cell.frame.height - 10))
        images.isUserInteractionEnabled = false
        images.setImage(imageNamed: imagesArray[indexPath.row])
        
        images.layer.cornerRadius = images.frame.height / 2
        images.backgroundColor = UIColor.white
        images.layer.borderColor = Color.code.navigationBar.cgColor
        images.layer.borderWidth = 2
        images.clipsToBounds = true
        
        cell.addSubview(images)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let images = imagesArray[indexPath.item]
        onColorSelected?(images)
    }
}
