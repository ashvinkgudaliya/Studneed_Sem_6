//
//  CollapsibleTableViewController.swift
//  ios-swift-collapsible-table-section
//
//  Created by Yong Su on 5/30/16.
//  Copyright © 2016 Yong Su. All rights reserved.
//

import UIKit



class StandardViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    let tableView = UITableView()
    var standard  = [StandardData]()
    
    var directroyId = String()
    var venderDirectoryId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Standard"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(editTable))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
        
        tableView.register(UINib(nibName: "StandardCell", bundle: nil), forCellReuseIdentifier: "standardCell")

        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = Color.code.lightBar
        self.view = tableView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Standard.webServices.get(venderDirectoryId: venderDirectoryId, directoryId: directroyId) { data in
            self.standard.removeAll()
            for i in 0..<data.count {
                var temp = StandardData()
                temp.id = data.dictionary(at: i).integer(forKey: "id")
                temp.subCategory = data.dictionary(at: i).integer(forKey: "subCategory")
                temp.subSubCategory = data.dictionary(at: i).integer(forKey: "subSubCategory")
                temp.subCategoryName = data.dictionary(at: i).string(forKey: "subCategoryName")
                temp.subSubCategoryName = data.dictionary(at: i).string(forKey: "subSubCategoryName")
                
                temp.fees = data.dictionary(at: i).integer(forKey: "fees")
                temp.startTime = data.dictionary(at: i).string(forKey: "startTime")
                temp.endTime = data.dictionary(at: i).string(forKey: "endTime")
                self.standard.append(temp)
                self.tableView.reloadData()
            }
        }
    }
    
    func editTable(){
        let vc = AddStandardViewController()
        vc.directroyId = directroyId
        vc.venderDirectoryId = venderDirectoryId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - TableViewDelegate / required
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return standard.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 176
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let standardCell = tableView.dequeueReusableCell(withIdentifier: "standardCell", for: indexPath) as! StandardCell
        standardCell.selectionStyle = .none
        standardCell.backgroundColor = Color.code.lightBar
        standardCell.standardNameLbl.text = standard[indexPath.row].subSubCategoryName == "" ? standard[indexPath.row].subCategoryName : standard[indexPath.row].subSubCategoryName
        standardCell.feesLbl.text = "\(standard[indexPath.row].fees)"
        
        standardCell.startTimeLbl.text = standard[indexPath.row].startTime
        standardCell.endTimeLbl.text = standard[indexPath.row].endTime
        
        return standardCell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?{
        let moreAction = UITableViewRowAction(style: .default, title: "Delete", handler: {_,_ in
            Standard.webServices.delete(Id: self.standard[indexPath.row].id, handler: { isDeleted in
                if isDeleted {
                    self.standard.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .left)
                }
            })
        })
        
        moreAction.backgroundColor = Color.code.button
        return [moreAction]
    }
}
