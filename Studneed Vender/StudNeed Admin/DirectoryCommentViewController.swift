//
//  DirectoryCommentViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 25/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import CarbonKit

class DirectoryCommentsViewController: UIViewController,CarbonTabSwipeNavigationDelegate,LUNTabBarFloatingControllerAnimatedTransitioning{

    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var items : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        self.navigationItem.title = "My Directory Comments"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.view.backgroundColor = Color.code.lightBar
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(commentViewHideActionAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshData))
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        refreshData()
    }
    
    func refreshData() {
        VenderPersonal.data.getData { response in
            self.items.removeAll()
            if response{
                for i in 0..<venderFullData.array(forKey: "venderTypeDetails").count{
                    self.items.append(venderFullData.array(forKey: "venderTypeDetails").dictionary(at: i).dictionary(forKey: "directoryData").string(forKey: "name"))
                }
                if self.items.count != 0 {
                    self.carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: self.items, delegate: self)
                    self.carbonTabSwipeNavigation.insert(intoRootViewController: self)
                }
                self.style()
            }
        }
    }
    
    func commentViewHideActionAction(){
        let tabBarController: LUNTabBarController = (self.tabBarController as? LUNTabBarController)!
        tabBarController.hideFloatingTab()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func style(){
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.setIndicatorColor(Color.code.bar)
        for i in 0..<venderFullData.array(forKey: "venderTypeDetails").count{
            carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(venderFullData.array(forKey: "venderTypeDetails").dictionary(at: i).dictionary(forKey: "directoryData").string(forKey: "name").widthForString(), forSegmentAt: i)
        }
        
        carbonTabSwipeNavigation.setNormalColor(Color.code.navigationBar.withAlphaComponent(0.6), font: UIFont.systemFont(ofSize: 14))
        carbonTabSwipeNavigation.setSelectedColor(Color.code.navigationBar, font: UIFont.boldSystemFont(ofSize: 14))
    }
    
    func barPosition(for carbonTabSwipeNavigation: CarbonTabSwipeNavigation) -> UIBarPosition {
        return .top
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        let commentsViewController = CommentsViewController()
        commentsViewController.venderDirectoryId = venderFullData.array(forKey: "venderTypeDetails").dictionary(at: NSInteger(index)).integerValue(forKey: "venderDirectoryId")
        
        commentsViewController.venderDirectoryName = venderFullData.array(forKey: "venderTypeDetails").dictionary(at: NSInteger(index)).dictionary(forKey: "directoryData").string(forKey: "name")
        
        commentsViewController.directoryImages = venderFullData.array(forKey: "venderTypeDetails").dictionary(at: NSInteger(index)).dictionary(forKey: "directoryData").array(forKey: "images").dictionary(at: 0).string(forKey: "name")
        return commentsViewController
    }
    
    // MARK: - LUNTabBarFloatingControllerAnimatedTransitioning
    func floatingViewControllerStartedAnimatedTransition(_ isPresenting: Bool) {
        let angle = CGFloat(isPresenting ? -M_PI_2 : M_PI_2)
        navigationController?.navigationBar.transform = CGAffineTransform(rotationAngle: angle)
        navigationController?.navigationBar.alpha = isPresenting ? 0 : 1
    }
    func keyframeAnimation(forFloatingViewControllerAnimatedTransition isPresenting: Bool) -> (_: Void) -> Void {
        return {() -> Void in
            let backgroundColor = isPresenting ? UIColor.white.withAlphaComponent(0.95) : UIColor.white
            self.view.backgroundColor = backgroundColor
            UIView.addKeyframe(withRelativeStartTime: isPresenting ? 0.8 : 0, relativeDuration: 0.2, animations: {() -> Void in
                self.navigationController?.navigationBar.transform = CGAffineTransform.identity
                self.navigationController?.navigationBar.alpha = isPresenting ? 1 : 0
            })
        }
    }
    func floatingViewControllerFinishedAnimatedTransition(_ isPresenting: Bool, wasCompleted: Bool) {
    }
}


