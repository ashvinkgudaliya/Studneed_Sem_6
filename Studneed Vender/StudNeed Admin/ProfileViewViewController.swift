//
//  ProfileViewViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 19/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//
enum imagesProfile{
    case header,profile
}
let temp = ""

import UIKit
import Former
import Alamofire

class ProfileViewViewController: FormViewController ,UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    var contactNo = ""
    var header : StretchHeader!
    var navigationView = UIView()
    var gender = "Male"
    var imagePicker = UIImagePickerController()
    
    let avatarImage = UIImageView()
    var selecterImagesPiker : imagesProfile = .header
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
        configure()
        tableView.bounces = false
        imagePicker.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupHeaderView()
        let navibarHeight : CGFloat = navigationController!.navigationBar.bounds.height
        let statusbarHeight : CGFloat = UIApplication.shared.statusBarFrame.size.height
        navigationView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: navibarHeight + statusbarHeight)
        navigationView.backgroundColor = Color.code.navigationBar
        navigationView.alpha = 0.0
        view.addSubview(navigationView)
        
//        let button = UIButton(type: .custom)
//        button.frame = CGRect(x: 10, y: 20, width: 44, height: 44)
//        button.setImage(Images.name.menu?.withRenderingMode(.alwaysTemplate), for: UIControlState())
//        button.tintColor = UIColor.white
//        button.addTarget(self, action: #selector(openDrowerAction), for: .touchUpInside)
//        view.addSubview(button)
    }
    
    func setupHeaderView() {
        
        let options = StretchHeaderOptions()
        options.position = .fullScreenTop
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(setNeedImage))
        tap1.numberOfTapsRequired = 1
        
        header = StretchHeader()
        header.stretchHeaderSize(headerSize: CGSize(width: view.frame.size.width, height: self.view.frame.height/3),
                                 imageSize: CGSize(width: view.frame.size.width, height: (self.view.frame.height/3) - 40),
                                 controller: self,
                                 options: options)
        header.imageView.isUserInteractionEnabled = true
        header.imageView.addGestureRecognizer(tap1)
        header.imageView.setImage(imageNamed: currentLoginVenderDetais.string(forKey: "proHeader"))
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(setProfileImage))
        tap.numberOfTapsRequired = 1
        
        // custom
        avatarImage.frame = CGRect(x: 10, y: header.imageView.frame.height - 20, width: 60, height: 60)
        avatarImage.setImage(imageNamed: currentLoginVenderDetais.string(forKey: "profilePic"))
        avatarImage.layer.cornerRadius = 5.0
        avatarImage.layer.borderColor = UIColor.white.cgColor
        avatarImage.layer.borderWidth = 3.0
        avatarImage.clipsToBounds = true
        avatarImage.contentMode = .scaleAspectFill
        avatarImage.isUserInteractionEnabled = true
        avatarImage.addGestureRecognizer(tap)
        header.addSubview(avatarImage)
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: header.imageView.frame.width - 130 - 10, y: header.imageView.frame.height + 10, width: 130, height: 30)
        button.setTitle("Edit Profile", for: UIControlState())
        button.setTitleColor(UIColor.lightGray, for: UIControlState())
        button.layer.cornerRadius = 5.0
        button.addTarget(self, action: #selector(EditProfileAction), for: .touchUpInside)
        button.backgroundColor = UIColor.clear
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 1.0
        header.addSubview(button)
        header.backgroundColor = Color.code.lightBar
        tableView.tableHeaderView = header
    }
    
    func setProfileImage(){
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        selecterImagesPiker = .profile
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func setNeedImage(){
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        selecterImagesPiker = .header
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func EditProfileAction(sender:UIButton){
        if sender.isSelected {
            if name.text == nil || name.text!.isEmpty{
                Nottification.message.error(body: "Enter Full Name")
            }
            else if emailId.text == nil || emailId.text!.isEmpty{
                Nottification.message.error(body: "Enter Email ID")
            }
            else if !emailId.text!.isValidEmail(){
                Nottification.message.error(body: "Enter Valid Email ID")
            }
            else if contactNo.isEmpty{
                Nottification.message.error(body: "Enter Store Mobile Number")
            }
            else if !contactNo.isValidPhone(){
                Nottification.message.error(body: "Enter 10 Digit Mobile Number")
            }
            else if gender.isEmpty{
                Nottification.message.error(body: "Select Gender")
            }
//            else if address.text == nil || address.text!.isEmpty{
//                Nottification.message.error(body: "Enter Store Address")
//            }
//            else if locality.text == nil || locality.text!.isEmpty{
//                Nottification.message.error(body: "Enter Store Country")
//            }
//            else if country.text == nil || country.text!.isEmpty{
//                Nottification.message.error(body: "Enter Store Country")
//            }
//            else if state.text == nil || state.text!.isEmpty{
//                Nottification.message.error(body: "Enter Store State")
//            }
//            else if city.text == nil || city.text!.isEmpty{
//                Nottification.message.error(body: "Enter Store City")
//            }
//            else if pincode.text == nil || pincode.text!.isEmpty{
//                Nottification.message.error(body: "Enter Store Pincode")
//            }
            else{
                let parameter : Parameters = [
                    "venderId" : currentLoginVenderDetais.string(forKey: "venderId"),
                    "FullName" : name.text!,
                    "address" : address.text!,
                    "email" : emailId.text!,
                    "mobile" : contactNo,
                    "pinCode" : pincode.text!,
                    "country" : country.text!,
                    "locality" : locality.text!,
                    "state" : state.text!,
                    "city" : city.text!,
                    "gender" : gender
                ]
                VenderPersonal.data.editPersonalData(parameter: parameter, handler: { response in
                    sender.isSelected = !response
                    self.former[0...self.former.numberOfSections - 1].flatMap { $0.rowFormers }.forEach { field in
                        field.enabled = !response
                    }
                })
            }
        }
        else{
            self.former[0...self.former.numberOfSections - 1].flatMap { $0.rowFormers }.forEach { field in
                field.enabled = !sender.isSelected
            }
            sender.isSelected = !sender.isSelected
        }
        sender.setTitle("Edit Profile", for: .normal)
        sender.setTitle("Save Profile", for: .selected)
    }
    
//    func openDrowerAction(){
//        AGSideMenu.openOrClose()
//    }
    
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    fileprivate let name = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Full Name"
        }.configure { field in
            field.placeholder = "Enter Full Name"
    }
    
    fileprivate let emailId = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Email ID"
        }.configure {  field in
            field.placeholder = "Email ID"
    }

    let emailConformation = SwitchRowFormer<FormSwitchCell>() { field in
        field.titleLabel.text = "Email Conformation"
        field.titleLabel.textColor = Color.code.navigationBar
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.switchButton.onTintColor = .formerSubColor()
        }.configure { field in
            field.switchWhenSelected = false
        }.onSwitchChanged { field in
//            self.switchInfomationSection()
    }
    
    let contactConformation = SwitchRowFormer<FormSwitchCell>() { field in
        field.titleLabel.text = "Contact No. Conformation"
        field.titleLabel.textColor = Color.code.navigationBar
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.switchButton.onTintColor = .formerSubColor()
        }.configure { field in
            field.switchWhenSelected = false
        }.onSwitchChanged { field in
//            switchInfomationcontactConformation()
    }
    
    fileprivate let address = TextViewRowFormer<FormTextViewCell>() { field in
        field.textView.textColor = .formerSubColor()
        field.textView.font = .systemFont(ofSize: 15)
        }.configure { field in
            field.placeholder = "Add your \(titleName) Address"
    }
    
    fileprivate let locality = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Locality"
        }.configure { field in
            field.placeholder = "Enter Locality"
    }
    
    fileprivate let country = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "County"
        }.configure { field in
            field.placeholder = "Enter County"
    }
    
    fileprivate let state = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "State"
        }.configure { field in
            field.placeholder = "Enter State"
    }
    
    fileprivate let city = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "City"
        }.configure { field in
            field.placeholder = "Enter City"
    }
    
    fileprivate let pincode = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Pincode"
        }.configure { field in
            field.placeholder = "Enter Pincode"
    }
    
    private func configure() {
        let phoneRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
            field.titleLabel.text = "Phone"
            field.textField.inputAccessoryView = self.formerInputAccessoryView
            field.textField.delegate = self
            field.textField.keyboardType = .numberPad
            }.configure { field in
                field.placeholder = "Add your phone number"
            }.onTextChanged { field in
                self.contactNo = field
        }
        
        let genderRow = SegmentedRowFormer<FormSegmentedCell>(instantiateType: .Class) { field in
            field.tintColor = Color.code.navigationBar
            }.configure { field in
                field.segmentTitles = ["Male", "Female"]
                field.selectedIndex = currentLoginVenderDetais.string(forKey: "gender") == "" ? 0:(currentLoginVenderDetais.string(forKey: "gender") == "Male" ? 0: 1)
            }.onSegmentSelected({ (index, value) in
                self.gender = value
        })
        
        name.text = currentLoginVenderDetais.string(forKey: "fullName")
        emailId.text = currentLoginVenderDetais.string(forKey: "email")
        phoneRow.text = currentLoginVenderDetais.string(forKey: "mobile")
        self.contactNo = phoneRow.text!
        contactConformation.switchWhenSelected = currentLoginVenderDetais.string(forKey: "mobileConfirmed") == "0" ? false:true
        emailConformation.switchWhenSelected = currentLoginVenderDetais.string(forKey: "emailConfirmed") == "0" ? false:true
        
        address.text = currentLoginVenderDetais.string(forKey: "address")
        state.text = currentLoginVenderDetais.string(forKey: "state")
        city.text = currentLoginVenderDetais.string(forKey: "city")
        locality.text = currentLoginVenderDetais.string(forKey: "locality")
        country.text = currentLoginVenderDetais.string(forKey: "country")
        pincode.text = currentLoginVenderDetais.string(forKey: "pinCode")
        
        
        former.append(sectionFormer:
            SectionFormer(rowFormer: name, emailId, phoneRow),
            SectionFormer(rowFormer: genderRow),
            //SectionFormer(rowFormer: emailConformation),
            //SectionFormer(rowFormer: contactConformation),
            SectionFormer(rowFormer: address, locality, city, state, country,pincode)
        )
        
        self.former[0...self.former.numberOfSections - 1].flatMap { $0.rowFormers }.forEach { field in
            field.enabled = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) == nil{
            let len = (textField.text?.characters.count)! + string.characters.count
            if len <= 10 {
                return true
            }
            else{
                return false
            }
        }
        return false
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if selecterImagesPiker == .header{
                UploadWebservices(image:pickedImage,fileName: "proHeader")
            }
            else{
                UploadWebservices(image:pickedImage,fileName: "profilePic")
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func UploadWebservices(image:UIImage,fileName:String){
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(UIImagePNGRepresentation(image)!, withName: fileName, fileName: fileName, mimeType: "image/jpeg")
                multipartFormData.append(currentLoginVenderDetais.string(forKey: "venderId").data(using: String.Encoding.utf8)!, withName: "venderId")
        },
            to: Web.url.venderProfileImage,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        switch response.result{
                        case .success(_):
                            let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                            switch aDict.integer(forKey: Web.key.status){
                            case Web.statusCode.ok:
                                currentLoginVenderDetais = aDict.dictionary(forKey: "venderData")
                                UserDefaults.standard.set(currentLoginVenderDetais, forKey: "isUserLoginDetails")
                                
                                self.avatarImage.setImage(imageNamed: currentLoginVenderDetais.string(forKey: "profilePic"))
                                self.header.imageView.setImage(imageNamed: currentLoginVenderDetais.string(forKey: "proHeader"))
                                break
                            case Web.statusCode.failed:
                                Nottification.message.error(body: aDict.string(forKey: Web.key.message), showButton: true)
                                break
                            default:
                                Nottification.message.error(body: aDict.string(forKey: "message"))
                                break
                            }
                            break
                        case .failure(_):
                            Nottification.message.error(body: "Request failed")
                            break
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        })
    }
}

