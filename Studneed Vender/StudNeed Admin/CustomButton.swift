//
//  CustomButton.swift
//  Kitchry
//
//  Created by BoHuang on 2/7/17.
//  Copyright © 2017 Daniel Kim. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    @IBInspectable var isCircle : Bool = true
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isCircle {
            let maskPath = UIBezierPath(roundedRect: self.bounds,
                                        byRoundingCorners: [.topLeft,.topRight,.bottomLeft],
                                        cornerRadii: CGSize(width: self.layer.frame.size.height / 2, height: self.layer.frame.size.height / 2))
            let shape = CAShapeLayer()
            shape.path = maskPath.cgPath
            layer.mask = shape
            layer.masksToBounds = true
        }
    }
}

