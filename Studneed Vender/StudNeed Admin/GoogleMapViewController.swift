//
//  GoogleMapViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 25/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

protocol GoogleLocationControllerProtocol: NSObjectProtocol {
    func didFinishTask(placemark: CLPlacemark,location:CLLocation)
}

class GoogleMapViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate,GMSAutocompleteViewControllerDelegate{

    var locationManager = CLLocationManager()
    var mapView = GMSMapView()
    weak var delegate:GoogleLocationControllerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        design()
        navigationBarDesign()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.tabBarController?.tabBar.isHidden = false
    }
    
    func navigationBarDesign(){
        self.view.backgroundColor = Color.code.lightBar
        self.title = "Google Map"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(SearchBarOpenAction))
    }
    
    func design(){
        mapView = GMSMapView(frame: CGRect(x: 0, y: 50, width: self.view.frame.height, height: self.view.frame.width - 50))
        
        if locationManager.responds(to:#selector(requestAlwaysAuthorization)) {
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
        }
        
        self.view = mapView
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        mapView.delegate = self
        locationManager.delegate = self
    }
    

    //MARK: Action Method and method definded
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        locationManager.startUpdatingLocation()
        return true
    }
    
    
    func SearchBarOpenAction(){
        locationManager.stopUpdatingLocation()
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        UINavigationBar.appearance().barTintColor = Color.code.navigationBar
        UINavigationBar.appearance().tintColor = UIColor.white
        let searchBarTextAttributes: [String : AnyObject] = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: UIFont.systemFontSize)]
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = searchBarTextAttributes
        self.present(acController, animated: true, completion: { _ in })
    }
    
    func requestAlwaysAuthorization()
    {
        let status : CLAuthorizationStatus = CLLocationManager.authorizationStatus()
        if status == .authorizedWhenInUse || status == .denied {
            let title:String = (status == .denied) ? "Location services are off" : "Background location is not enabled"
            let message:String = "To use background location you must turn on 'Always' in the Location Services Settings"
            Nottification.message.info(title: title, body: message, showButton: true)
        }
        else if status == .denied {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    func geocoderAddress(location:CLLocation){
        CLGeocoder().reverseGeocodeLocation(location) { (placeMarks, error) in
            if (error == nil && (placeMarks?.count)! > 0)
            {
                self.delegate?.didFinishTask(placemark: (placeMarks?.last)!, location: location)
            }
        }
    }

   
    //MARK: locationmanager delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        requestAlwaysAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation = locations[0] as CLLocation
        mapView.clear()
        let fancy = GMSCameraPosition.camera(withLatitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, zoom: 18)
        mapView.camera = fancy
        geocoderAddress(location: currentLocation)
    }
    
    //MARK: GmsMapView delegate
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        setLocationInMap(coordinate.latitude, longitude: coordinate.longitude, zoom: 18)
        locationManager.stopUpdatingLocation()
    }
    
    func setLocationInMap(_ latitude: CLLocationDegrees, longitude: CLLocationDegrees, zoom: Float) {        
        mapView.clear()
        let position = CLLocationCoordinate2DMake(latitude, longitude)
        let marker = GMSMarker(position: position)
        marker.map = mapView
        geocoderAddress(location: CLLocation(latitude: latitude, longitude: longitude))
    }
    
    //MARK: Search Bar controller delegate 
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        viewController.dismiss(animated: true, completion: { _ in })
        mapView.clear()
        let fancy = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 18)
        mapView.camera = fancy
        
        let position = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude)
        let marker = GMSMarker(position: position)
        marker.map = mapView
        marker.title = place.formattedAddress
        geocoderAddress(location: CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude))
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        Nottification.message.error(body: "Google Search Action Failed Please try again...!")
        viewController.dismiss(animated: true, completion: { _ in })
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        locationManager.startUpdatingLocation()
        viewController.dismiss(animated: true, completion: { _ in })
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
