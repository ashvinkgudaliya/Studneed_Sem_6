//
//  MainCategory.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 20/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

var allDirectoryType = NSArray()
var currentOpenCategoryImages = String()

import UIKit
import Alamofire

class Main {
    
    static let webService = Main()
    
    func directoryType(compilation:@escaping (Bool) -> ()){
        Alamofire.request(Web.url.directoryType, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    allDirectoryType = aDict.array(forKey: Web.key.directoryType)
                    compilation(true)
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
    
    func getCategory(compilation:@escaping ([allMainCategory]) -> ()){
        Alamofire.request(Web.url.getCategory, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            //debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    var data : [allMainCategory] = []
                    for i in 0..<aDict.array(forKey: Web.key.category).count{
                        data.append(allMainCategory(id:  aDict.array(forKey: Web.key.category).dictionary(at: i).integer(forKey: "categoryId"), name:  aDict.array(forKey: Web.key.category).dictionary(at: i).string(forKey: "name"), subCategory: aDict.array(forKey: Web.key.category).dictionary(at: i).array(forKey: "subCategory")))
                    }
                    compilation(data)
                    break
                default:
                    Nottification.message.error(body: "Category Load request failed")
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
}


class allMainCategory{
    var id = NSInteger()
    var name = String()
    var subCategory : [SubCategory] = []
    
    init(id:NSInteger,name:String,subCategory:NSArray) {
        self.id = id
        self.name = name
        
        for i in 0..<subCategory.count{
            self.subCategory.append(SubCategory(id: subCategory.dictionary(at: i).integer(forKey: "subCategoryId"), name: subCategory.dictionary(at: i).string(forKey: "name"), subSubCategory: subCategory.dictionary(at: i).array(forKey: "subSubCategory")))
        }
    }
}

class SubCategory{
    var id = NSInteger()
    var name = String()
    var subSubCategory : [SubSubCategory] = []
    
    init(id:NSInteger,name:String,subSubCategory:NSArray) {
        self.id = id
        self.name = name
        for i in 0..<subSubCategory.count{
            self.subSubCategory.append(SubSubCategory(id: subSubCategory.dictionary(at: i).integer(forKey: "subCategoryId"), name: subSubCategory.dictionary(at: i).string(forKey: "fform")))
        }
    }
}

class SubSubCategory {
    var id = NSInteger()
    var name = String()
    
    init(id:NSInteger,name:String) {
        self.id = id
        self.name = name
    }
}
