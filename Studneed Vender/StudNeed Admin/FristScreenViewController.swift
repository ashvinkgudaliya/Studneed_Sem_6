//
//  ViewController.swift
//  PeparOnBoedDemo
//
//  Created by Ashvin Gudaliya on 26/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class FristScreenViewController: UIViewController,PaperOnboardingDataSource,PaperOnboardingDelegate{

    let skipButton = ZFRippleButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.code.lightBar
        self.navigationController?.isNavigationBarHidden = true
        
        
        
        let onboarding = PaperOnboarding(itemsCount: 3)
        onboarding.dataSource = self
        onboarding.delegate = self
        onboarding.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(onboarding)
        
        for attribute: NSLayoutAttribute in [.left, .right, .top, .bottom] {
            let constraint = NSLayoutConstraint(item: onboarding,
                                                attribute: attribute,
                                                relatedBy: .equal,
                                                toItem: view,
                                                attribute: attribute,
                                                multiplier: 1,
                                                constant: 0)
            view.addConstraint(constraint)
        }
        
        skipButton.frame = CGRect(x: (self.view.frame.width/4)*3, y: 30, width: self.view.frame.width/4, height: 40)
        skipButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        skipButton.setTitle("Skip >", for: .normal)
        skipButton.addTarget(self, action: #selector(skipButtonMethod), for: .touchUpInside)
        self.view.addSubview(skipButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func skipButtonMethod() {
        UserDefaults.standard.set(true, forKey: "isSeconTimeLoad")
        AppDelegate.shared.setRootViewController(viewController: LoginAndSingUpViewController())
    }
    
    func onboardingDidTransitonToIndex(_ index: Int) {
    }
    
    func onboardingConfigurationItem(_ item: OnboardingContentViewItem, index: Int) {
    }
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        skipButton.isHidden = true
        if index == 0{
            skipButton.isHidden = false
            skipButton.setTitle("Skip >", for: .normal)
        }
        if index == 2{
            skipButton.isHidden = false
            skipButton.setTitle("Done", for: .normal)
        }
    }
    
    func onboardingItemAtIndex(_ index: Int) -> OnboardingItemInfo {
        let titleFont = UIFont(name: "Nunito-Bold", size: 36.0) ?? UIFont.boldSystemFont(ofSize: 36.0)
        let descriptionFont = UIFont(name: "OpenSans-Regular", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
        return [
            (UIImage.Asset.Hotels.rawValue, "Hotels", "All hotels and hostels are sorted by hospitality rating", UIImage.Asset.Key.rawValue, UIColor(red:0.40, green:0.56, blue:0.71, alpha:1.00), UIColor.white, UIColor.white, titleFont,descriptionFont),
            (UIImage.Asset.book.rawValue, "Books", "We carefully verify all banks before add them into the app", UIImage.Asset.Wallet.rawValue, UIColor(red:0.40, green:0.69, blue:0.71, alpha:1.00), UIColor.white, UIColor.white, titleFont,descriptionFont),
            (UIImage.Asset.Stores.rawValue, "Stores", "All local stores are categorized for your convenience", UIImage.Asset.Shopping_Cart.rawValue, UIColor(red:0.61, green:0.56, blue:0.74, alpha:1.00), UIColor.white, UIColor.white, titleFont,descriptionFont)
            ][index]
    }
    
    func onboardingItemsCount() -> Int {
        return 3
    }
}
