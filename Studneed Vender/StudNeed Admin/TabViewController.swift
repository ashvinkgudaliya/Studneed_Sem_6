//
//  TabViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 12/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class TabViewController: LUNTabBarController {

    var fadeAnimationController = DemoFadeAnimationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        floatingTabIndex = 2
        transitionDuration = 0.3
        transitionScaleMultiplier = 1
        transitionAlphaMultiplier = 0.4
        
        let addDirectoryViewController = UINavigationController(rootViewController: AddDirectoryViewController())
        let myDirectoryViewController = UINavigationController(rootViewController: MyDirectoryViewController())
        let commentsViewController = UINavigationController(rootViewController: DirectoryCommentsViewController())
        let profileViewViewController = UINavigationController(rootViewController: ProfileViewViewController())
        
        self.viewControllers = [myDirectoryViewController,addDirectoryViewController,commentsViewController,profileViewViewController]
        
        let home = UITabBarItem(title: "Add Directory", image: Images.name.directory, tag: 0)
        let directory = UITabBarItem(title: "My Directory", image: Images.name.home, tag: 1)
        let commnet = UITabBarItem(title: "Comments", image: Images.name.chat, tag: 2)
        let profile = UITabBarItem(title: "profile",image: Images.name.user, tag: 3)
        
        myDirectoryViewController.tabBarItem = directory
        addDirectoryViewController.tabBarItem = home
        commentsViewController.tabBarItem = commnet
        profileViewViewController.tabBarItem = profile
        
        UITabBar.appearance().tintColor = Color.code.navigationBar
        
        fadeAnimationController = DemoFadeAnimationController()
        floatingContentHeight = 500.0 / 568.0 * view.bounds.size.height
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning {
        var animationController: UIViewControllerAnimatedTransitioning? = super.tabBarController(tabBarController, animationControllerForTransitionFrom: fromVC, to: toVC)
        
        if animationController == nil {
            animationController = fadeAnimationController
            fadeAnimationController.transitionDuration = transitionDuration
        }
        return animationController!
    }
}
