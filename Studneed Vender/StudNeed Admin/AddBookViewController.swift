//
//  AddBookViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 15/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import Former
import Alamofire
import MapKit


class AddBookViewController: FormViewController,UITextFieldDelegate,GoogleLocationControllerProtocol{

    var directoryId = NSInteger()
    var contactNo = ""
    var CurrentLocation = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        navigationBarDesign()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func navigationBarDesign(){
        self.view.backgroundColor = Color.code.lightBar
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(addData))
    }
    
    func addData(){
        if name.text == nil || name.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Name")
        }
        else if emailId.text != nil  || !emailId.text!.isEmpty{
            if !emailId.text!.isValidEmail(){
                Nottification.message.error(body: "Enter Valid Email ID")
            }
        }
        else if contactNo.isEmpty{
            Nottification.message.error(body: "Enter Store Mobile Number")
        }
        else if !contactNo.isValidPhone(){
            Nottification.message.error(body: "Enter 10 Digit Mobile Number")
        }
        else if String.onlyTime(date: startTime.date).isEmpty{
            Nottification.message.error(body: "Enter Store Openinges Time ")
        }
        else if String.onlyTime(date: endTime.date).isEmpty{
            Nottification.message.error(body: "Enter Store Closing Time")
        }
        else if discription.text == nil || discription.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Discription")
        }
        else if address.text == nil || address.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Address")
        }
        else if locality.text == nil || locality.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Country")
        }
        else if country.text == nil || country.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Country")
        }
        else if state.text == nil || state.text!.isEmpty{
            Nottification.message.error(body: "Enter Store State")
        }
        else if city.text == nil || city.text!.isEmpty{
            Nottification.message.error(body: "Enter Store City")
        }
        else if pincode.text == nil || pincode.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Pincode")
        }
        else{
            let parameter : Parameters = [
                "venderId" : currentLoginVenderDetais.string(forKey: "venderId"),
                "directoryId" : directoryId,
                "name" : name.text!,
                "address" : address.text!,
                "startTime" : String.onlyTime(date: startTime.date),
                "endTime" : String.onlyTime(date: endTime.date),
                "discription" : discription.text!,
                "emailId" : emailId.text!,
                "website" : webSite.text == nil ? "":webSite.text!,
                "contactNo" : contactNo,
                "pinCode" : pincode.text!,
                "country" : country.text!,
                "locality" : locality.text!,
                "state" : state.text!,
                "city" : city.text!,
                "latitute" : CurrentLocation.coordinate.latitude,
                "longitute" : CurrentLocation.coordinate.longitude
            ]
            VenderDirectory.data.add(parameter: parameter, addedDirectoryId: { venderDirId in
                let imagesViewController = ImageCollectionViewController(nibName: "ImageCollectionViewController", bundle: nil)
                imagesViewController.venderDirectoryId = venderDirId
                self.navigationController?.pushViewController(imagesViewController, animated: true)
            })
        }
    }
    
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    fileprivate let name = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Name"
        }.configure { field in
            field.placeholder = "Enter Name"
        }

    fileprivate let emailId = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Email ID"
        field.textField.keyboardType = .emailAddress
        }.configure { field in
            field.placeholder = "Store Email ID"
        }
    
    fileprivate let discription = TextViewRowFormer<FormTextViewCell>() { field in
        field.textView.textColor = .formerSubColor()
        field.textView.font = .systemFont(ofSize: 15)
        }.configure { field in
            field.placeholder = "Add your store discription"
        }
    
    fileprivate let webSite = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Web Site"
        }.configure { field in
            field.placeholder = "www.example.com"
        }
    
    fileprivate let address = TextViewRowFormer<FormTextViewCell>() { field in
        field.textView.textColor = .formerSubColor()
        field.textView.font = .systemFont(ofSize: 15)
        }.configure { field in
            field.placeholder = "Add your store Address"
        }
    
    fileprivate let locality = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Locality"
        }.configure { field in
            field.placeholder = "Enter Locality"
    }
    
    fileprivate let country = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "County"
        }.configure { field in
            field.placeholder = "Enter County"
    }
    
    fileprivate let state = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "State"
        }.configure { field in
            field.placeholder = "Enter State"
    }
    
    fileprivate let city = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "City"
        }.configure { field in
            field.placeholder = "Enter City"
    }
    
    fileprivate let pincode = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Pincode"
        }.configure { field in
            field.placeholder = "Enter Pincode"
    }
    
    fileprivate let startTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "Open Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup {
            $0.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)

    
    fileprivate let endTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "Close Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup {
            $0.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    private func configure() {
        title = "Add Book Store Details"
        
        let phoneRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
            field.titleLabel.text = "Phone"
            field.textField.inputAccessoryView = self.formerInputAccessoryView
            field.textField.delegate = self
            field.textField.keyboardType = .numberPad
            }.configure { field in
                field.placeholder = "Add your phone number"
            }.onTextChanged { field in
                self.contactNo = field
        }

        let mapLocation = SwitchRowFormer<FormSwitchCell>() { field in
            field.titleLabel.text = "Current Location Enabled"
            field.titleLabel.textColor = Color.code.navigationBar
            field.titleLabel.font = .boldSystemFont(ofSize: 15)
            field.switchButton.onTintColor = .formerSubColor()
            }.configure { field in
                field.switchWhenSelected = false
            }.onSwitchChanged { field in
                if field{
                    self.openMapActionController()
                }
        }
        
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.viewHeight = 40
                    $0.text = text
            }
        }
        
        former.append(sectionFormer:
            SectionFormer(rowFormer: name, emailId, phoneRow).set(headerViewFormer: createHeader("Basic Details")),
            SectionFormer(rowFormer: discription,webSite).set(headerViewFormer: createHeader("Discription")),
            SectionFormer(rowFormer: startTime, endTime).set(headerViewFormer: createHeader("Store open and close time")),
            SectionFormer(rowFormer: mapLocation).set(headerViewFormer: createHeader("Set Current Location")),
            SectionFormer(rowFormer: address,locality, city, state, country, pincode).set(headerViewFormer: createHeader("Address Details"))
        )
    }
}

extension AddBookViewController{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) == nil{
            let len = (textField.text?.characters.count)! + string.characters.count
            if len <= 10 {
                return true
            }
            else{
                return false
            }
        }
        return false
    }
    
    func openMapActionController() {
        let googleMap = GoogleMapViewController()
        googleMap.delegate = self
        self.navigationController?.pushViewController(googleMap, animated: true)
    }
    
    func didFinishTask(placemark: CLPlacemark, location: CLLocation) {
        self.pincode.text = placemark.addressDictionary?["ZIP"] as? String
        self.locality.text = placemark.addressDictionary?["Street"] as? String
        self.city.text = placemark.addressDictionary?["City"] as? String
        self.country.text = placemark.addressDictionary?["Country"] as? String
        self.state.text = placemark.addressDictionary?["State"] as? String
        self.CurrentLocation = location
        self.former.reload()
    }
}
