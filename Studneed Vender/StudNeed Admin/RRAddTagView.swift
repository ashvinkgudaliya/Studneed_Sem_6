//
//  RRAddTagView.swift
//  RRTagController
//
//  Created by Remi Robert on 20/02/15.
//  Copyright (c) 2015 Remi Robert. All rights reserved.
//

import UIKit

class RRAddTagView: UIView {

    lazy var title: UILabel = {
        let title = UILabel(frame: CGRect(x: 10, y: 10, width: UIScreen.main.bounds.size.width - 20, height: 20))
        title.font = UIFont.boldSystemFont(ofSize: 18)
        title.textAlignment = NSTextAlignment.center
        return title
    }()

    lazy var textEdit: UITextField = {
        let textEdit = UITextField(frame: CGRect(x: 10, y: 30, width: UIScreen.main.bounds.size.width - 20, height: 20))
        textEdit.returnKeyType = .done
        textEdit.font = UIFont.systemFont(ofSize: 14)
        return textEdit
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        title.text = "Add Services"
        self.addSubview(title)
        self.addSubview(textEdit)
        self.backgroundColor = Color.code.lightBar
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
