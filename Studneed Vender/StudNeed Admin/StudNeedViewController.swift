//
//  StudNeedViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 12/04/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class StudNeedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarDesign()
    }

    func navigationBarDesign(){
        self.title = "Users Request"
        self.view.backgroundColor = Color.code.lightBar
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
