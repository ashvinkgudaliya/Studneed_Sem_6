//
//  LoginAndSingUpViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 13/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class LoginAndSingUpViewController: UIViewController {
    
    let imagesBackBroundView = UIImageView()
    let btnLogin = ZFRippleButton()
    let btnSignUp = ZFRippleButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.code.lightBar
        designLoginAndSignUp()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func designLoginAndSignUp(){
        imagesBackBroundView.frame = self.view.bounds
        let y = (self.view.frame.height/10)*8.5
        let width = (self.view.frame.width - 60)/2
        let gap = self.view.frame.height/33
        btnLogin.frame = CGRect(x: 25, y: y, width: width, height: gap*3)
        btnSignUp.frame = CGRect(x: 35 + width, y: y, width: width , height: gap*3)
        
        imagesBackBroundView.image = UIImage(named: "HostalDefaultImages.jpg")
        
        btnLogin.setTitle("Login", for: UIControlState())
        btnLogin.layer.cornerRadius = 5
        btnLogin.setTitleColor(Color.code.button, for: UIControlState())
        btnLogin.addTarget(self, action:#selector(LoginAndSingUpViewController.loginViewBtnAction), for: UIControlEvents.touchUpInside)
        btnLogin.backgroundColor = UIColor.white
        
        btnSignUp.setTitle("Sign Up", for: UIControlState())
        btnSignUp.layer.cornerRadius = 5
        btnSignUp.rippleBackgroundColor = UIColor.clear
        btnSignUp.rippleColor = Color.code.bar
        btnSignUp.addTarget(self, action:#selector(LoginAndSingUpViewController.signUpViewBtnAction), for: UIControlEvents.touchUpInside)
        btnSignUp.backgroundColor = Color.code.button
        
        self.view.addSubview(imagesBackBroundView)
        self.view.addSubview(btnLogin)
        self.view.addSubview(btnSignUp)
    }
    
    func loginViewBtnAction(){
        self.navigationController?.pushViewController(LoginViewController(), animated: true)
    }
    
    func signUpViewBtnAction(){
        self.navigationController?.pushViewController(SignUpViewController(), animated: true)
    }
}
