//
//  StandardCell.swift
//  standardTable
//
//  Created by HiddenMind on 27/03/17.
//  Copyright © 2017 HiddenMind. All rights reserved.
//

import UIKit

class StandardCell: UITableViewCell {

    @IBOutlet weak var backgroundCellView: UIView!
    @IBOutlet weak var standardNameLbl: UILabel!

    @IBOutlet weak var feesLbl: UILabel!
    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    
    @IBOutlet weak var feesView: UIView!
    @IBOutlet weak var timeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundCellView.layer.cornerRadius = 5
        backgroundCellView.shadowEnable()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
