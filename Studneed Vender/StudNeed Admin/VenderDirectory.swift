//
//  Vender.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 12/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import Alamofire

var venderFullData = NSDictionary()

class VenderDirectory{
    static let data = VenderDirectory()
    
    func add(parameter:Parameters ,addedDirectoryId:@escaping (NSInteger)-> ()){
        Alamofire.request(Web.url.addDirectory, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    addedDirectoryId(aDict.integer(forKey: "venderDirectoryId"))
                    Nottification.message.success(body: aDict.string(forKey: "message"))
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
    
    func edit(parameter:Parameters ,handler:@escaping (Bool) -> ()){
        Alamofire.request(Web.url.editDirectory, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    handler(true)
                    Nottification.message.success(body: aDict.string(forKey: "message"))
                    break
                default:
                    handler(false)
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                handler(false)
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
    
    func pgServices(handler:@escaping ([tagIs]) -> ()){
        Alamofire.request(Web.url.pgServices, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    var tag : [tagIs] = []
                    
                    for i in 0..<aDict.array(forKey: "services").count{
                        if !aDict.array(forKey: "services").dictionary(at: i).string(forKey: "serviceName").isEmpty {
                            tag.append(tagIs(string: aDict.array(forKey: "services").dictionary(at: i).string(forKey: "serviceName"), isSelected: false))
                        }
                    }
                    handler(tag)
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
}
