//
//  Web.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 20/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
let DomainName = "http://localhost/StudNeed/"
let ImagesPath = "\(DomainName)images/"

class Web {
    
    class url {
        static let venderFullData = "\(DomainName)profile/vender/venderFullDetails.php"
        static let addDirectory = "\(DomainName)profile/vender/addVenderDirectory.php"
        static let editDirectory = "\(DomainName)profile/vender/editVenderDirectory.php"
        static let editPersonalDetails = "\(DomainName)profile/vender/editPersonalDetails.php"
        static let login = "\(DomainName)Login/vender/login.php"
        static let registration = "\(DomainName)Login/vender/registration.php"
        static let city = "\(DomainName)Details/city.php"
        
        static let directoryType = "\(DomainName)Category/directoryType.php"
        static let getCategory = "\(DomainName)Category/getCategory.php"
        
        static let venderProfileImage = "\(DomainName)profile/vender/venderProfileImage.php"
        static let uploadVenderDirectoryimages = "\(DomainName)profile/vender/uploadVenderDirectoryimages.php"
        
        static let comment = "\(DomainName)Details/addComments.php"
        
        static let pgServices = "\(DomainName)profile/vender/pgHostalServices.php"
        
        static let addStandard = "\(DomainName)profile/vender/addStandard.php"
        
    }
    
    class key {
        static let status = "status"
        static let directoryType = "directoryType"
        static let discription = "discription"
        static let name = "name"
        static let image = "image"
        static let category = "category"
        static let message = "message"
    }
    
    class statusCode {
        static let ok = 200
        static let failed = 401
    }
    
}


