//
//  PGViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 12/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import Former
import Alamofire
import MapKit
import PhotoSlider

class PGViewController: FormViewController,UITextFieldDelegate,GoogleLocationControllerProtocol{

    var gender = "Both"
    var contactNo = ""
    var displaydata = NSDictionary()
    var enabled = true
    var CurrentLocation = CLLocation()
    var imagesArr : [String] = []
    var tag : [tagIs] = []
    var selectedServices = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "name")
        address.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "address")
        discription.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "discription")
        emailId.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "emailId")
        webSite.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "website")
        contactNo = displaydata.dictionary(forKey: "directoryData").string(forKey: "contactNo")
        breakfartStartTime.date = displaydata.dictionary(forKey: "directoryData").string(forKey: "breakfartStartTime").onlyTime()
        breakfartEndTime.date = displaydata.dictionary(forKey: "directoryData").string(forKey: "breakfartEndTime").onlyTime()
        lunchStartTime.date = displaydata.dictionary(forKey: "directoryData").string(forKey: "lunchStartTime").onlyTime()
        lunchEndTime.date = displaydata.dictionary(forKey: "directoryData").string(forKey: "lunchEndTime").onlyTime()
        dinnerStartTime.date = displaydata.dictionary(forKey: "directoryData").string(forKey: "dinnerStartTime").onlyTime()
        dinnerEndTime.date = displaydata.dictionary(forKey: "directoryData").string(forKey: "dinnerEndTime").onlyTime()
        
        fees.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "fees")
        locality.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "locality")
        city.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "city")
        state.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "state")
        country.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "country")
        pincode.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "pinCode")
        
        selectedServices = displaydata.dictionary(forKey: "directoryData").string(forKey: "services")
        let tempServices : [String] = displaydata.dictionary(forKey: "directoryData").string(forKey: "services").components(separatedBy: " ")
        for temp in tempServices {
            if !temp.isEmpty {
                tag.append(tagIs(string: temp, isSelected: true))
            }
        }
        
        for i in 0..<displaydata.dictionary(forKey: "directoryData").array(forKey: "images").count{
            imagesArr.append(displaydata.dictionary(forKey: "directoryData").array(forKey: "images").dictionary(at: i).string(forKey: "name"))
        }
        
        configure()
        
        VenderDirectory.data.pgServices { webTag in
            for j in 0..<webTag.count {
                let allTag : tagIs = webTag[j]
                var isAdded = false
                
                for i in 0..<self.tag.count{
                    let selectedTag : tagIs = self.tag[i]
                    if allTag.string == selectedTag.string{
                        isAdded = true
                        break
                    }
                }
                
                if !isAdded{
                    self.tag.append(allTag)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func addData(request:@escaping (Bool) -> ()){
        if name.text == nil || name.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Name")
        }
        else if emailId.text != nil || !emailId.text!.isEmpty ? !emailId.text!.isValidEmail() : false{
            Nottification.message.error(body: "Enter Valid Email ID")
        }
        else if contactNo.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Mobile Number")
        }
        else if !contactNo.isValidPhone(){
            Nottification.message.error(body: "Enter 10 Digit Mobile Number")
        }
        else if fees.text == nil || fees.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName)  Fees")
        }
        else if discription.text  == nil || discription.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Discription")
        }
        else if address.text  == nil || address.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Address")
        }
        else if locality.text  == nil || locality.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Country")
        }
        else if country.text  == nil || country.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Country")
        }
        else if state.text  == nil || state.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) State")
        }
        else if city.text  == nil || city.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) City")
        }
        else if pincode.text  == nil || pincode.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Pincode")
        }
        else{
            let parameter : Parameters = [
                "id" : displaydata.dictionary(forKey: "directoryData").string(forKey: "id"),
                "venderDirectoryId" : displaydata.string(forKey: "venderDirectoryId"),
                "directoryId" : displaydata.string(forKey: "directoryId"),
                "name" : name.text!,
                "address" : address.text!,
                "gender" : gender,
                "breakfartEndTime" : String.onlyTime(date: breakfartEndTime.date),
                "breakfartStartTime" : String.onlyTime(date: breakfartStartTime.date),
                "lunchStartTime" : String.onlyTime(date: lunchStartTime.date),
                "lunchEndTime" : String.onlyTime(date: lunchEndTime.date),
                "dinnerStartTime" : String.onlyTime(date: dinnerStartTime.date),
                "dinnerEndTime" : String.onlyTime(date: dinnerEndTime.date),
                "services" : selectedServices,
                "fees" : fees.text!,
                "discription" : discription.text!,
                "emailId" : emailId.text!,
                "website" : webSite.text == nil ? "":webSite.text!,
                "contactNo" : contactNo,
                "locality" : locality.text!,
                "pinCode" : pincode.text!,
                "country" : country.text!,
                "state" : state.text!,
                "city" : city.text!,
                "latitute" : CurrentLocation.coordinate.latitude,
                "longitute" : CurrentLocation.coordinate.longitude
                ]
            VenderDirectory.data.edit(parameter: parameter, handler: { response in
                request(response)
            })
        }
    }
    
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    fileprivate let name = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Name"
        }.configure { field in
            field.placeholder = "Enter Name"
    }
    
    
    fileprivate let emailId = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Email ID"
        }.configure {  field in
            field.placeholder = "Email ID"
    }
    
    fileprivate let discription = TextViewRowFormer<FormTextViewCell>() { field in
        field.textView.textColor = .formerSubColor()
        field.textView.font = .systemFont(ofSize: 15)
        }.configure { field in
            field.placeholder = "Add your \(titleName) discription"
    }
    
    fileprivate let webSite = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Web Site"
        }.configure { field in
            field.placeholder = "www.example.com"
    }
    
    fileprivate let address = TextViewRowFormer<FormTextViewCell>() { field in
        field.textView.textColor = .formerSubColor()
        field.textView.font = .systemFont(ofSize: 15)
        }.configure { field in
            field.placeholder = "Add your \(titleName) Address"
    }
    
    fileprivate let locality = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Locality"
        }.configure { field in
            field.placeholder = "Enter Locality"
    }
    
    fileprivate let country = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "County"
        }.configure { field in
            field.placeholder = "Enter County"
    }
    
    fileprivate let state = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "State"
        }.configure { field in
            field.placeholder = "Enter State"
    }
    
    fileprivate let city = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "City"
        }.configure { field in
            field.placeholder = "Enter City"
    }
    
    fileprivate let pincode = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Pincode"
        }.configure { field in
            field.placeholder = "Enter Pincode"
    }
    
    fileprivate let fees = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Fees"
        field.textField.keyboardType = .numberPad
        }.configure { field in
            field.placeholder = "Amounts"
    }
    
    fileprivate let breakfartStartTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "Start Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup { field in
            field.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    fileprivate let breakfartEndTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "End Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup { field in
            field.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    fileprivate let lunchStartTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "Start Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup { field in
            field.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    fileprivate let lunchEndTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "End Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup { field in
            field.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    fileprivate let dinnerStartTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "Start Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup { field in
            field.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    fileprivate let dinnerEndTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "End Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup { field in
            field.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    
    private func configure() {
        title = "Add \(titleName) Details"
        
        let phoneRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
            field.titleLabel.text = "Phone"
            field.textField.inputAccessoryView = self.formerInputAccessoryView
            field.textField.delegate = self
            field.textField.keyboardType = .numberPad
            }.configure { field in
                field.placeholder = "Add your phone number"
            }.onTextChanged { field in
                self.contactNo = field
        }
        phoneRow.text = self.contactNo
        
        let positions = ["Boys Only", "Girls Only", "Both"]
        var selectedIndex = 2
        for i in 0..<positions.count{
            if displaydata.dictionary(forKey: "directoryData").string(forKey: "gender") == positions[i]{
                selectedIndex = i
                self.gender = positions[i]
            }
        }
        
        let colorListRow = CustomRowFormer<ImagesListCell>(instantiateType: .Nib(nibName: "ImagesListCell")) {
            $0.imagesArray = self.imagesArr
            $0.onColorSelected = { images in
                var images : [ImagesData] = []
                var photos : [Photo] = []
                for i in 0..<self.displaydata.dictionary(forKey: "directoryData").array(forKey: "images").count{
                    images.append(ImagesData(
                        id: self.displaydata.dictionary(forKey: "directoryData").array(forKey: "images").dictionary(at: i).integerValue(forKey: "id"),
                        venderDirectoryId: self.displaydata.dictionary(forKey: "directoryData").array(forKey: "images").dictionary(at: i).string(forKey: "venderDirectoryId"),
                        name: self.displaydata.dictionary(forKey: "directoryData").array(forKey: "images").dictionary(at: i).string(forKey: "name")))
                    
                    photos.append(PhotoSlider.Photo(imageURL: URL(string: ImagesPath + self.displaydata.dictionary(forKey: "directoryData").array(forKey: "images").dictionary(at: i).string(forKey: "name"))!))
                }
                let imagesGallery = ImageCollectionViewController(nibName: "ImageCollectionViewController", bundle: nil)
                imagesGallery.images = images
                imagesGallery.photos = photos
                imagesGallery.isBackCondition = true
                imagesGallery.venderDirectoryId = self.displaydata.integerValue(forKey: "venderDirectoryId")
                self.navigationController?.pushViewController(imagesGallery, animated: true)
            }
            }.configure {
                $0.rowHeight = 60
        }
        
        let genderRow = SegmentedRowFormer<FormSegmentedCell>(instantiateType: .Class) {
            $0.tintColor = Color.code.navigationBar
            }.configure {
                $0.segmentTitles = positions
                $0.selectedIndex = selectedIndex
            }.onSegmentSelected({ (index, value) in
                self.gender = value
            })
        
        let mapLocation = SwitchRowFormer<FormSwitchCell>() { field in
            field.titleLabel.text = "Current Location Enabled"
            field.titleLabel.textColor = Color.code.navigationBar
            field.titleLabel.font = .boldSystemFont(ofSize: 15)
            field.switchButton.onTintColor = .formerSubColor()
            }.configure { field in
                field.switchWhenSelected = false
            }.onSwitchChanged { field in
                if field{
                    self.openMapActionController()
                }
        }
        
        let venderServices = LabelRowFormer<FormLabelCell>()
            .configure {
                $0.text = "Added Services"
                $0.subText = "\(self.tag.count)"
            }.onSelected { [weak self] filed in

                RRTagController.displayTagController(parentController: self!, tagsString: (self?.tag)!, blockFinish: { (selectedTags, unSelectedTags) -> () in
                    self?.selectedServices = ""
                    for tagTemp in selectedTags{
                        var isadded = false
                        for i in 0..<(self?.tag.count)!{
                            let temp : tagIs? = (self?.tag[i])!
                            if tagTemp.textContent == temp?.string {
                                temp?.isSelected = true
                                self?.tag[i] = temp!
                                isadded = true
                            }
                        }
                        
                        if !isadded {
                            self?.tag.append(tagIs(string: tagTemp.textContent, isSelected: true))
                        }
                        self?.selectedServices = (self?.selectedServices)! + " " + tagTemp.textContent
                        filed.subText = "\(selectedTags.count)"
                    }
                    self?.former.reload()
                }) { () -> () in
                }
                self?.former.deselect(animated: true)
        }
        
        let disableRowText: ((Bool) -> String) = { name in
            return (name ? "Edit All Data" : "Changes Saved")
        }
        let disableRow = LabelRowFormer<FormLabelCell>()
            .configure { field in
                field.text = disableRowText(true)
            }.onSelected(disableRowSelected)
        
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure { header in
                    header.viewHeight = 40
                    header.text = text
            }
        }
        
        former.append(sectionFormer:
            SectionFormer(rowFormer: disableRow),
              SectionFormer(rowFormer: colorListRow).set(headerViewFormer: createHeader("Images Gallary")),
              SectionFormer(rowFormer: name, emailId, phoneRow)
                .set(headerViewFormer: createHeader("Basic Details")),
              SectionFormer(rowFormer: genderRow)
                .set(headerViewFormer: createHeader("Select \(titleName) Type")),
              SectionFormer(rowFormer: fees),
              SectionFormer(rowFormer: discription,webSite)
                .set(headerViewFormer: createHeader("Discription")),
              SectionFormer(rowFormer: venderServices)
                .set(headerViewFormer: createHeader("Other Services")),
              
              SectionFormer(rowFormer: breakfartStartTime, breakfartEndTime)
                .set(headerViewFormer: createHeader("Breakfast Time")),
              SectionFormer(rowFormer: lunchStartTime, lunchEndTime)
                .set(headerViewFormer: createHeader("Lunch time")),
              SectionFormer(rowFormer: dinnerStartTime, dinnerEndTime)
                .set(headerViewFormer: createHeader("Dinner time")),
              
              SectionFormer(rowFormer: mapLocation).set(headerViewFormer: createHeader("Set Current Location")),
              SectionFormer(rowFormer: address,locality , city, state, country, pincode)
                .set(headerViewFormer: createHeader("Address Details"))
        )
        
        self.former[2...self.former.numberOfSections - 1].flatMap { $0.rowFormers }.forEach { field in
            field.enabled = !self.enabled
        }
        self.enabled = !self.enabled
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) == nil{
            let len = (textField.text?.characters.count)! + string.characters.count
            if len <= 10 {
                return true
            }
            else{
                return false
            }
        }
        return false
    }
    
    private func disableRowSelected(rowFormer: RowFormer) {
        guard let disableRow = rowFormer as? LabelRowFormer<FormLabelCell> else { return }
        if disableRow.text == "Changes Saved" {
            self.addData(request: { response in
                if response {
                    self.former[2...self.former.numberOfSections - 1].flatMap { $0.rowFormers }.forEach { field in
                        field.enabled = !self.enabled
                    }
                    disableRow.text = (self.enabled ? "Edit All Data" : "Changes Saved")
                    disableRow.update()
                    self.enabled = !self.enabled
                }
                self.former.deselect(animated: true)
            })
        }
        else{
            self.former.deselect(animated: true)
            self.former[2...self.former.numberOfSections - 1].flatMap { $0.rowFormers }.forEach { field in
                field.enabled = !enabled
            }
            disableRow.text = (enabled ? "Edit All Data" : "Changes Saved")
            disableRow.update()
            self.enabled = !self.enabled
        }
    }
    
    func openMapActionController() {
        let googleMap = GoogleMapViewController()
        googleMap.delegate = self
        self.navigationController?.pushViewController(googleMap, animated: true)
    }
    
    func didFinishTask(placemark: CLPlacemark, location: CLLocation) {
        self.pincode.text = placemark.addressDictionary?["ZIP"] as? String
        self.locality.text = placemark.addressDictionary?["Street"] as? String
        self.city.text = placemark.addressDictionary?["City"] as? String
        self.country.text = placemark.addressDictionary?["Country"] as? String
        self.state.text = placemark.addressDictionary?["State"] as? String
        self.CurrentLocation = location
        self.former.reload()
    }
}
