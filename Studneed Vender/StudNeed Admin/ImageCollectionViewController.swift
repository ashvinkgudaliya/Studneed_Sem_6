//
//  ImageCollectionViewController.swift
//  CollectionViewDemo
//
//  Created by HiddenMind on 23/03/17.
//  Copyright © 2017 HiddenMind. All rights reserved.
//

import UIKit
import Alamofire
import PhotoSlider

class ImageCollectionViewController: UICollectionViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIViewControllerTransitioningDelegate,PhotoSliderDelegate,ZoomingAnimationControllerTransitioning{
    
    @IBOutlet var flowLayOut: UICollectionViewFlowLayout!
    var images : [ImagesData] = []
    var photos : [Photo] = []
    var imagePicker = UIImagePickerController()
    private let reuseIdentifier = "Cell"
    var venderDirectoryId = NSInteger()
    var currentRow = 0
    var isBackCondition :Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.register(UINib(nibName: "ImagesCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        flowLayOut.itemSize = CGSize(width: ((self.collectionView?.frame.width)! - 41)/3, height: ((self.collectionView?.frame.width)! - 41)/3)
        flowLayOut.sectionInset = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        navigationBarDesign()
    }
    
    func navigationBarDesign(){
        self.collectionView?.backgroundColor = Color.code.lightBar
        self.title = "Images"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(jumpToDirectoryView))
    }

    func jumpToDirectoryView(){
        if images.count == 0{
            Nottification.message.info(body: "Please, select Atleast One Imges", showButton: true)
        }
        else{
            if !isBackCondition {
                AppDelegate.shared.setRootViewController(viewController: TabViewController())
            }
            else{
                self.navigationController!.popViewController(animated: true)
            }
        }
    }
    
    internal override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: UICollectionViewDataSourc
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count + 1
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ImagesCell
        if images.count == indexPath.row{
            cell.ImageSelectedView.image = #imageLiteral(resourceName: "pluseImagesAddIcon.png")
            cell.deleteBtn.isHidden = true
        }
        else{
            cell.ImageSelectedView.setImage(imageNamed: images[indexPath.row].name)
            cell.deleteBtn.tag = images[indexPath.row].id
            cell.deleteBtn.layer.setValue(indexPath.row, forKey: "index")
            cell.deleteBtn.isHidden = false
            cell.deleteBtn.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if images.count == indexPath.row{
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let photoSlider = PhotoSlider.ViewController(photos: photos)
            photoSlider.delegate = self
            photoSlider.currentPage = indexPath.row
            photoSlider.transitioningDelegate = self
            present(photoSlider, animated: true, completion: nil)
        }
    }
    
    func deleteAction(sender:UIButton){
        Alamofire.request(Web.url.uploadVenderDirectoryimages, method: .post, parameters: ["imageid":sender.tag], encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    self.images.remove(at: sender.layer.value(forKey: "index") as! Int)
                    self.collectionView?.performBatchUpdates({
                        let indexPath = IndexPath(item: sender.layer.value(forKey: "index") as! Int, section: 0)
                        self.collectionView?.deleteItems(at: [indexPath])
                    }, completion: nil)
                    break
                default:
                    Nottification.message.error(body: "Category Load request failed")
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            uploadImageToServer(image: pickedImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func uploadImageToServer(image:UIImage){
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(UIImagePNGRepresentation(image)!, withName: "file", fileName: "file", mimeType: "image/jpeg")
                multipartFormData.append(("\(self.venderDirectoryId)").data(using: String.Encoding.utf8)!, withName: "venderDirectoryId")
        },
            to: Web.url.uploadVenderDirectoryimages,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        switch response.result{
                        case .success(_):
                            let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                            switch aDict.integer(forKey: Web.key.status){
                            case Web.statusCode.ok:
                                Nottification.message.success(body: aDict.string(forKey: "message"))
                                self.images.removeAll()
                                self.photos.removeAll()
                                for i in 0..<aDict.array(forKey: "Images").count{
                                    self.images.append(ImagesData(
                                        id: aDict.array(forKey: "Images").dictionary(at: i).integerValue(forKey: "id"),
                                        venderDirectoryId: aDict.array(forKey: "Images").dictionary(at: i).string(forKey: "venderDirectoryId"),
                                        name: aDict.array(forKey: "Images").dictionary(at: i).string(forKey: "name")))
                                    self.photos.append(PhotoSlider.Photo(imageURL: URL(string: ImagesPath + aDict.array(forKey: "Images").dictionary(at: i).string(forKey: "name"))!))
                                }
                                self.collectionView?.reloadData()
                                break
                            case Web.statusCode.failed:
                                Nottification.message.error(body: aDict.string(forKey: Web.key.message), showButton: true)
                                break
                            default:
                                Nottification.message.error(body: aDict.string(forKey: "message"))
                                break
                            }
                            break
                        case .failure(_):
                            Nottification.message.error(body: "Request failed")
                            break
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        })
    }
}

extension ImageCollectionViewController {
    func transitionDestinationImageView(sourceImageView: UIImageView) {
        
        let indexPath = collectionView?.indexPathsForSelectedItems?.first
        let cell = collectionView?.cellForItem(at: indexPath!) as! ImagesCell
        
        let frame: CGRect = (collectionView?.convert(cell.frame, to: self.view))!
        sourceImageView.frame = frame
    }
    
    func transitionSourceImageView() -> UIImageView {
        
        let indexPath = collectionView?.indexPathsForSelectedItems?.first
        let cell = collectionView?.cellForItem(at: indexPath!) as! ImagesCell
        let imageView = UIImageView(image: cell.ImageSelectedView.image)
        
        let frame: CGRect = (collectionView?.convert(cell.frame, to: self.view))!
        
        imageView.frame = frame
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        
        return imageView
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let animationController = PhotoSlider.ZoomingAnimationController(present: false)
        animationController.sourceTransition = dismissed as? ZoomingAnimationControllerTransitioning
        animationController.destinationTransition = self
        view.frame = dismissed.view.bounds
        
        return animationController
        
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let animationController = PhotoSlider.ZoomingAnimationController(present: true)
        animationController.sourceTransition = source as? ZoomingAnimationControllerTransitioning
        animationController.destinationTransition = presented as? ZoomingAnimationControllerTransitioning
        return animationController
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        guard let collectionView = collectionView else {
            return
        }
        
        collectionView.contentOffset = CGPoint(x: CGFloat(currentRow) * view.bounds.width, y: 0.0)
    }
    
    // MARK: - UIContentContainer
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    
    func photoSliderControllerWillDismiss(_ viewController: PhotoSlider.ViewController) {
        currentRow = viewController.currentPage
        let indexPath = IndexPath(item: currentRow, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
    }
}

