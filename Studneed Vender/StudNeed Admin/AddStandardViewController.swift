//
//  AddStandardViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 02/04/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

class AddStandardViewController: UIViewController ,SCPopDatePickerDelegate{

    var allCategoryData : [allMainCategory] = []
    
    var mainCategoryButton = UIButton()
    var subCategoryBtn = UIButton()
    var subSubCategoryBtn = UIButton()
    var selections = [String]()
    var popView = SGPopSelectView()
    
    var mainCategoryIndex = 0
    var subCategoryIndex = 0
    var subSubCategoryIndex = 0
    
    var startTimeLbl = UILabel()
    var endTimelbl = UILabel()
    var startTimeBtn = UIButton()
    var endTimeBtn = UIButton()
    
    let datePicker = SCPopDatePicker()
    let date = Date()
    var selectedBtnDatePiker = 0
    
    let feesLbl = UILabel()
    let feesTxt = UITextField()
    let addBtn = UIButton()
    
    var directroyId = String()
    var venderDirectoryId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Standard"
        self.view.backgroundColor = Color.code.lightBar
        Main.webService.getCategory { response in
            self.allCategoryData = response
            self.designbtn()
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func designbtn(){
        mainCategoryButton.frame = CGRect(x: 10, y: 20, width: self.view.frame.width - 20, height: 50)
        subCategoryBtn.frame = CGRect(x: 10, y: mainCategoryButton.frame.height + mainCategoryButton.frame.origin.y + 10, width: self.view.frame.width - 20, height: 50)
        subSubCategoryBtn.frame = CGRect(x: 10, y: subCategoryBtn.frame.height + subCategoryBtn.frame.origin.y + 10, width: self.view.frame.width - 20, height: 50)
        
        mainCategoryButton.setTitle("Category Type", for: .normal)
        subCategoryBtn.setTitle("Category", for: .normal)
        subSubCategoryBtn.setTitle("Sub Category Type", for: .normal)
        
        mainCategoryButton.addTarget(self, action: #selector(mainCategorySelectAction), for: .touchUpInside)
        subCategoryBtn.addTarget(self, action: #selector(SubCategorySelectAction), for: .touchUpInside)
        subSubCategoryBtn.addTarget(self, action: #selector(SubSubCategorySelectAction), for: .touchUpInside)
        subSubCategoryBtn.isHidden = true
        
        for btn in [mainCategoryButton,subCategoryBtn,subSubCategoryBtn]{
            btn.backgroundColor = Color.code.button
            btn.layer.cornerRadius = 8
        }
        
        self.view.addSubview(mainCategoryButton)
        self.view.addSubview(subCategoryBtn)
        self.view.addSubview(subSubCategoryBtn)
    }
    
    func mainCategorySelectAction(sender:UIButton){
        selections.removeAll()
        for i in 0..<self.allCategoryData.count{
            selections.append(self.allCategoryData[i].name)
        }
        popView?.selections = selections
        popView?.selectedHandle = {(_ selectedIndex: Int) -> Void in
            sender.setTitle(self.allCategoryData[selectedIndex].name, for: .normal)
            self.mainCategoryIndex = selectedIndex
            self.popView?.hide(true)
            self.subCategoryBtn.setTitle("Category", for: .normal)
            self.subSubCategoryBtn.setTitle("Sub Category Type", for: .normal)
        }
        
        let p: CGPoint = sender.center
        popView?.show(from: view, at: p, animated: true)
    }
    
    func SubCategorySelectAction(sender:UIButton){
        selections.removeAll()
        for i in 0..<self.allCategoryData[mainCategoryIndex].subCategory.count{
            selections.append(self.allCategoryData[mainCategoryIndex].subCategory[i].name)
        }
        popView?.selections = selections
        popView?.selectedHandle = {(_ selectedIndex: Int) -> Void in
            sender.setTitle(self.allCategoryData[self.mainCategoryIndex].subCategory[selectedIndex].name, for: .normal)
            if self.allCategoryData[self.mainCategoryIndex].subCategory[selectedIndex].subSubCategory.count != 0{
                self.subSubCategoryBtn.isHidden = false
                self.subSubCategoryBtn.setTitle("Sub Category Type", for: .normal)
            }
            else{
                self.subSubCategoryBtn.isHidden = true
            }
            self.subCategoryIndex = selectedIndex
            self.popView?.hide(true)
            
            self.timePicker()
        }
        
        let p: CGPoint = sender.center
        popView?.show(from: view, at: p, animated: true)
    }
    
    func SubSubCategorySelectAction(sender:UIButton){
        selections.removeAll()
        for i in 0..<self.allCategoryData[mainCategoryIndex].subCategory[subCategoryIndex].subSubCategory.count{
            selections.append(self.allCategoryData[mainCategoryIndex].subCategory[subCategoryIndex].subSubCategory[i].name)
        }
        popView?.selections = selections
        popView?.selectedHandle = {(_ selectedIndex: Int) -> Void in
            sender.setTitle(self.allCategoryData[self.mainCategoryIndex].subCategory[self.subCategoryIndex].subSubCategory[selectedIndex].name, for: .normal)
                self.subSubCategoryIndex = selectedIndex
            self.popView?.hide(true)
        }
        
        let p: CGPoint = sender.center
        popView?.show(from: view, at: p, animated: true)
    }
    
    func timePicker(){
        startTimeLbl.frame = CGRect(x: 10, y: subSubCategoryBtn.frame.height + subSubCategoryBtn.frame.origin.y + 20, width: (self.view.frame.width - 20)/2, height: 25)
        endTimelbl.frame = CGRect(x: 10 + startTimeLbl.frame.width + startTimeLbl.frame.origin.x, y: subSubCategoryBtn.frame.height + subSubCategoryBtn.frame.origin.y + 20, width: (self.view.frame.width - 20)/2, height: 25)
        
        startTimeBtn.frame = CGRect(x: 10, y: startTimeLbl.frame.height + startTimeLbl.frame.origin.y + 10, width: (self.view.frame.width - 30)/2, height: 40)
        endTimeBtn.frame = CGRect(x: 10 + startTimeLbl.frame.width + startTimeLbl.frame.origin.x, y: startTimeLbl.frame.height + startTimeLbl.frame.origin.y + 10, width: (self.view.frame.width - 30)/2, height: 40)
        
        startTimeLbl.text = "Start Time"
        startTimeLbl.textAlignment = .center
        endTimelbl.text = "End Time"
        endTimelbl.textAlignment = .center
        
        startTimeBtn.setTitle("-- : -- : --", for: .normal)
        startTimeBtn.backgroundColor = Color.code.button
        startTimeBtn.tag = 1
        startTimeBtn.layer.cornerRadius = 8
        startTimeBtn.addTarget(self, action: #selector(pickerOpen), for: .touchUpInside)
        
        endTimeBtn.setTitle("-- : -- : --", for: .normal)
        endTimeBtn.backgroundColor = Color.code.button
        endTimeBtn.tag = 2
        endTimeBtn.layer.cornerRadius = 8
        endTimeBtn.addTarget(self, action: #selector(pickerOpen), for: .touchUpInside)
        
        self.view.addSubview(startTimeLbl)
        self.view.addSubview(endTimelbl)
        self.view.addSubview(startTimeBtn)
        self.view.addSubview(endTimeBtn)
        
        self.datePicker.datePickerType = .time
        self.datePicker.showBlur = true
        self.datePicker.btnFontColour = UIColor.white
        self.datePicker.btnColour = UIColor.darkGray
        self.datePicker.showCornerRadius = false
        self.datePicker.delegate = self
        
        feesLbl.frame = CGRect(x: 20, y: endTimeBtn.frame.height + endTimeBtn.frame.origin.y + 20, width: self.view.frame.width - 40, height: 30)
        feesTxt.frame = CGRect(x: 15, y: feesLbl.frame.height + feesLbl.frame.origin.y + 5, width: self.view.frame.width - 30, height: 30)
        addBtn.frame = CGRect(x: 10, y: feesTxt.frame.height + feesTxt.frame.origin.y + 30, width: self.view.frame.width - 20, height: 50)
        
        
        feesLbl.text = "Fees"
        feesTxt.placeholder = "Enter Fees Amount"
        feesTxt.font = UIFont.systemFont(ofSize: 20)
        
        addBtn.backgroundColor = Color.code.button
        addBtn.layer.cornerRadius = 8
        addBtn.setTitle("Add", for: .normal)
        addBtn.addTarget(self, action: #selector(addStandardAction), for: .touchUpInside)
        
        self.view.addSubview(feesLbl)
        self.view.addSubview(feesTxt)
        self.view.addSubview(addBtn)
    }
    
    func pickerOpen(sender:UIButton){
        selectedBtnDatePiker = sender.tag
        self.datePicker.show(attachToView: self.view)
    }
    
    func scPopDatePickerDidSelectDate(_ date: Date) {
        switch selectedBtnDatePiker {
        case 1:
            startTimeBtn.setTitle(String.onlyTime(date: date), for: .normal)
            break
        case 2:
            endTimeBtn.setTitle(String.onlyTime(date: date), for: .normal)
            break
        default:
            break
        }
    }
    
    func addStandardAction(){
        Standard.webServices.add(venderDirectoryId: venderDirectoryId, directoryId: directroyId,
                                 subCategory: self.allCategoryData[mainCategoryIndex].subCategory[subCategoryIndex].id,
                                 subSubCategory: self.allCategoryData[self.mainCategoryIndex].subCategory[self.subCategoryIndex].subSubCategory[subSubCategoryIndex].id,
                                 startTime: (startTimeBtn.titleLabel?.text)!, endTime: (endTimeBtn.titleLabel?.text)!, fees: feesTxt.text!) { responds in
            if responds {
                self.navigationController!.popViewController(animated: true)
            }
        }
    }
}
