//
//  MyDirectoryViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 12/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import CarbonKit

class MyDirectoryViewController: UIViewController,CarbonTabSwipeNavigationDelegate {
    
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var items : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "My Directory"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.view.backgroundColor = Color.code.lightBar
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshData))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "request", style: .plain, target: self, action: #selector(showUsersNeeds))
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        refreshData()
    }
    
    func refreshData(){
        VenderPersonal.data.getData { response in
            self.items.removeAll()
            if response{
                for i in 0..<venderFullData.array(forKey: "venderTypeDetails").count{
                    self.items.append(venderFullData.array(forKey: "venderTypeDetails").dictionary(at: i).dictionary(forKey: "directoryData").string(forKey: "name"))
                }
                if self.items.count != 0 {
                    self.carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: self.items, delegate: self)
                    self.carbonTabSwipeNavigation.insert(intoRootViewController: self)
                }
                self.style()
            }
        }
    }
    
    func showUsersNeeds(){
        self.navigationController?.pushViewController(StudNeedViewController(), animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func style(){
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.setIndicatorColor(Color.code.bar)
        for i in 0..<venderFullData.array(forKey: "venderTypeDetails").count{
            carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(venderFullData.array(forKey: "venderTypeDetails").dictionary(at: i).dictionary(forKey: "directoryData").string(forKey: "name").widthForString(), forSegmentAt: i)
        }
        
        carbonTabSwipeNavigation.setNormalColor(Color.code.navigationBar.withAlphaComponent(0.6), font: UIFont.systemFont(ofSize: 14))
        carbonTabSwipeNavigation.setSelectedColor(Color.code.navigationBar, font: UIFont.boldSystemFont(ofSize: 14))
    }
    
    func barPosition(for carbonTabSwipeNavigation: CarbonTabSwipeNavigation) -> UIBarPosition {
        return .top
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        switch venderFullData.array(forKey: "venderTypeDetails").dictionary(at: NSInteger(index)).string(forKey: "directoryId"){
        case "2","3":
            let vc = PGViewController()
            vc.displaydata = venderFullData.array(forKey: "venderTypeDetails").dictionary(at: NSInteger(index))
            return vc
        case "4","5","6":
            let vc = SchoolViewController()
            vc.displaydata = venderFullData.array(forKey: "venderTypeDetails").dictionary(at: NSInteger(index))
            return vc
        case "7":
            let vc = BookViewController()
            vc.displaydata = venderFullData.array(forKey: "venderTypeDetails").dictionary(at: NSInteger(index))
            return vc
        default:
            return UIViewController()
        }
    }
}
