//
//  BookViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 12/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import Former
import Alamofire
import MapKit
import PhotoSlider


class BookViewController: FormViewController ,UITextFieldDelegate,GoogleLocationControllerProtocol{
    
    var contactNo = ""
    var enabled = true
    var displaydata = NSDictionary()
    var CurrentLocation = CLLocation()
    var imagesArr : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "name")
        address.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "address")
        discription.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "discription")
        emailId.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "emailId")
        webSite.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "website")
        contactNo = displaydata.dictionary(forKey: "directoryData").string(forKey: "contactNo")
        
        locality.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "locality")
        city.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "city")
        state.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "state")
        country.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "country")
        pincode.text = displaydata.dictionary(forKey: "directoryData").string(forKey: "pinCode")
        
        startTime.date = displaydata.dictionary(forKey: "directoryData").string(forKey: "startTime").onlyTime()
        endTime.date = displaydata.dictionary(forKey: "directoryData").string(forKey: "endTime").onlyTime()
        
        for i in 0..<displaydata.dictionary(forKey: "directoryData").array(forKey: "images").count{
            imagesArr.append(displaydata.dictionary(forKey: "directoryData").array(forKey: "images").dictionary(at: i).string(forKey: "name"))
        }
        
        configure()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addData(request:@escaping (Bool) -> ()){
        if name.text == nil || name.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Name")
        }
        else if emailId.text != nil || !address.text!.isEmpty{
            if !emailId.text!.isValidEmail(){
                Nottification.message.error(body: "Enter Valid Email ID")
            }
        }
        else if contactNo.isEmpty{
            Nottification.message.error(body: "Enter Store Mobile Number")
        }
        else if !contactNo.isValidPhone(){
            Nottification.message.error(body: "Enter 10 Digit Mobile Number")
        }
        else if String.onlyTime(date: startTime.date).isEmpty{
            Nottification.message.error(body: "Enter Store Openinges Time ")
        }
        else if String.onlyTime(date: endTime.date).isEmpty{
            Nottification.message.error(body: "Enter Store Closing Time")
        }
        else if discription.text == nil || discription.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Discription")
        }
        else if address.text == nil || address.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Address")
        }
        else if locality.text == nil || locality.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Country")
        }
        else if country.text == nil || country.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Country")
        }
        else if state.text == nil || state.text!.isEmpty{
            Nottification.message.error(body: "Enter Store State")
        }
        else if city.text == nil || city.text!.isEmpty{
            Nottification.message.error(body: "Enter Store City")
        }
        else if pincode.text == nil || pincode.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Pincode")
        }
        else{
            let parameter : Parameters = [
                "id" : displaydata.dictionary(forKey: "directoryData").string(forKey: "id"),
                "venderDirectoryId" : displaydata.string(forKey: "venderDirectoryId"),
                "directoryId" : displaydata.string(forKey: "directoryId"),
                "name" : name.text!,
                "address" : address.text!,
                "startTime" : String.onlyTime(date: startTime.date),
                "endTime" : String.onlyTime(date: endTime.date),
                "discription" : discription.text!,
                "emailId" : emailId.text!,
                "website" : webSite.text == nil ? "":webSite.text!,
                "contactNo" : contactNo,
                "pinCode" : pincode.text!,
                "country" : country.text!,
                "locality" : locality.text!,
                "state" : state.text!,
                "city" : city.text!,
                "latitute" : CurrentLocation.coordinate.latitude,
                "longitute" : CurrentLocation.coordinate.longitude
            ]
            VenderDirectory.data.edit(parameter: parameter, handler: { response in
                request(response)
            })
        }
    }
    
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    fileprivate let name = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Name"
        }.configure { field in
            field.placeholder = "Enter Name"
    }
    
    fileprivate let emailId = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Email ID"
        field.textField.keyboardType = .emailAddress
        }.configure { field in
            field.placeholder = "Store Email ID"
    }
    
    fileprivate let discription = TextViewRowFormer<FormTextViewCell>() { field in
        field.textView.textColor = .formerSubColor()
        field.textView.font = .systemFont(ofSize: 15)
        }.configure { field in
            field.placeholder = "Add your store discription"
    }
    
    fileprivate let webSite = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Web Site"
        }.configure { field in
            field.placeholder = "www.example.com"
    }
    
    fileprivate let address = TextViewRowFormer<FormTextViewCell>() { field in
        field.textView.textColor = .formerSubColor()
        field.textView.font = .systemFont(ofSize: 15)
        }.configure { field in
            field.placeholder = "Add your store Address"
    }
    fileprivate let locality = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Locality"
        }.configure { field in
            field.placeholder = "Enter Locality"
    }
    
    fileprivate let country = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "County"
        }.configure { field in
            field.placeholder = "Enter County"
    }
    
    fileprivate let state = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "State"
        }.configure { field in
            field.placeholder = "Enter State"
    }
    
    fileprivate let city = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "City"
        }.configure { field in
            field.placeholder = "Enter City"
    }
    
    fileprivate let pincode = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Pincode"
        }.configure { field in
            field.placeholder = "Enter Pincode"
    }
    
    fileprivate let startTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "Open Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup {
            $0.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    
    fileprivate let endTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "Close Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup {
            $0.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    private func configure() {
        title = "Add Book Store Details"
        
        let phoneRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
            field.titleLabel.text = "Phone"
            field.textField.inputAccessoryView = self.formerInputAccessoryView
            field.textField.delegate = self
            field.textField.keyboardType = .numberPad
            }.configure { field in
                field.placeholder = "Add your phone number"
            }.onTextChanged { field in
                self.contactNo = field
        }
        
        phoneRow.text = self.contactNo
        
        let mapLocation = SwitchRowFormer<FormSwitchCell>() { field in
            field.titleLabel.text = "Current Location Enabled"
            field.titleLabel.textColor = Color.code.navigationBar
            field.titleLabel.font = .boldSystemFont(ofSize: 15)
            field.switchButton.onTintColor = .formerSubColor()
            }.configure { field in
                field.switchWhenSelected = false
            }.onSwitchChanged { field in
                if field{
                    self.openMapActionController()
                }
        }

        let colorListRow = CustomRowFormer<ImagesListCell>(instantiateType: .Nib(nibName: "ImagesListCell")) {
            $0.imagesArray = self.imagesArr
            $0.onColorSelected = { images in
                var images : [ImagesData] = []
                var photos : [Photo] = []
                for i in 0..<self.displaydata.dictionary(forKey: "directoryData").array(forKey: "images").count{
                    images.append(ImagesData(
                        id: self.displaydata.dictionary(forKey: "directoryData").array(forKey: "images").dictionary(at: i).integerValue(forKey: "id"),
                        venderDirectoryId: self.displaydata.dictionary(forKey: "directoryData").array(forKey: "images").dictionary(at: i).string(forKey: "venderDirectoryId"),
                        name: self.displaydata.dictionary(forKey: "directoryData").array(forKey: "images").dictionary(at: i).string(forKey: "name")))
    
                    photos.append(PhotoSlider.Photo(imageURL: URL(string: ImagesPath + self.displaydata.dictionary(forKey: "directoryData").array(forKey: "images").dictionary(at: i).string(forKey: "name"))!))
                }
                let imagesGallery = ImageCollectionViewController(nibName: "ImageCollectionViewController", bundle: nil)
                imagesGallery.images = images
                imagesGallery.photos = photos
                imagesGallery.isBackCondition = true
                imagesGallery.venderDirectoryId = self.displaydata.integerValue(forKey: "venderDirectoryId")
                self.navigationController?.pushViewController(imagesGallery, animated: true)
            }
            }.configure {
                $0.rowHeight = 60
        }
        
        let disableRowText: ((Bool) -> String) = { name in
            return (name ? "Edit All Data" : "Changes Saved")
        }
        let disableRow = LabelRowFormer<FormLabelCell>()
            .configure { field in
                field.text = disableRowText(true)
            }.onSelected(disableRowSelected)
        
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.viewHeight = 40
                    $0.text = text
            }
        }
        
        former.append(sectionFormer:
            SectionFormer(rowFormer: disableRow),
            SectionFormer(rowFormer: colorListRow).set(headerViewFormer: createHeader("Images Gallary")),
            SectionFormer(rowFormer: name, emailId, phoneRow).set(headerViewFormer: createHeader("Basic Details")),
            SectionFormer(rowFormer: discription,webSite).set(headerViewFormer: createHeader("Discription")),
            SectionFormer(rowFormer: startTime, endTime).set(headerViewFormer: createHeader("Store open and close time")),
            SectionFormer(rowFormer: mapLocation).set(headerViewFormer: createHeader("Set Current Location")),
            SectionFormer(rowFormer: address,locality, city, state, country, pincode).set(headerViewFormer: createHeader("Address Details"))
        )
        
        self.former[2...self.former.numberOfSections - 1].flatMap { $0.rowFormers }.forEach { field in
            field.enabled = !self.enabled
        }
        self.enabled = !self.enabled
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) == nil{
            let len = (textField.text?.characters.count)! + string.characters.count
            if len <= 10 {
                return true
            }
            else{
                return false
            }
        }
        return false
    }

    private func disableRowSelected(rowFormer: RowFormer) {
        guard let disableRow = rowFormer as? LabelRowFormer<FormLabelCell> else { return }
        if disableRow.text == "Changes Saved" {
            self.addData(request: { response in
                if response {
                    self.former[2...self.former.numberOfSections - 1].flatMap { $0.rowFormers }.forEach { field in
                        field.enabled = !self.enabled
                    }
                    disableRow.text = (self.enabled ? "Edit All Data" : "Changes Saved")
                    disableRow.update()
                    self.enabled = !self.enabled
                }
                self.former.deselect(animated: true)
            })
        }
        else{
            self.former.deselect(animated: true)
            self.former[2...self.former.numberOfSections - 1].flatMap { $0.rowFormers }.forEach { field in
                field.enabled = !enabled
            }
            disableRow.text = (enabled ? "Edit All Data" : "Changes Saved")
            disableRow.update()
            self.enabled = !self.enabled
        }
    }
    
    func openMapActionController() {
        let googleMap = GoogleMapViewController()
        googleMap.delegate = self
        self.navigationController?.pushViewController(googleMap, animated: true)
    }
    
    func didFinishTask(placemark: CLPlacemark, location: CLLocation) {
        self.pincode.text = placemark.addressDictionary?["ZIP"] as? String
        self.locality.text = placemark.addressDictionary?["Street"] as? String
        self.city.text = placemark.addressDictionary?["City"] as? String
        self.country.text = placemark.addressDictionary?["Country"] as? String
        self.state.text = placemark.addressDictionary?["State"] as? String
        self.CurrentLocation = location
        self.former.reload()
    }
}
