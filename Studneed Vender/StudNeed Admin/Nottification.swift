//
//  Nottification.swift
//  TiffinChimp
//
//  Created by HiddenMind on 28/12/16.
//  Copyright © 2016 HiddenMind. All rights reserved.
//

import SwiftMessages

class Nottification{
    static let message = Nottification()
    
    func error(title:String = "Error",body:String,showButton:Bool = false){
        let error = MessageView.viewFromNib(layout: .TabView)
        error.configureTheme(.error)
        error.button?.isHidden = !showButton
        error.configureContent(title: title, body: body, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Okay", buttonTapHandler: { _ in SwiftMessages.hide() })

        var errorConfig = SwiftMessages.defaultConfig
        if showButton{
            errorConfig.duration = .forever
        }
        errorConfig.dimMode = .gray(interactive: true)
        errorConfig.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        SwiftMessages.show(config: errorConfig, view: error)
    }
    
    func warning(title:String = "Warning", body:String){
        let warning = MessageView.viewFromNib(layout: .CardView)
        warning.configureTheme(.warning)
        warning.configureDropShadow()
        
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        warning.configureContent(title: title, body: body, iconText: iconText)
        warning.button?.isHidden = true
        var warningConfig = SwiftMessages.defaultConfig
        warningConfig.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        
        SwiftMessages.show(config: warningConfig, view: warning)
    }
    
    func success(title:String = "Success", body:String){
        let success = MessageView.viewFromNib(layout: .CardView)
        success.configureTheme(.success)
        success.configureDropShadow()
        success.configureContent(title: title, body: body)
        success.button?.isHidden = true
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .top
        successConfig.presentationContext = .window(windowLevel: UIWindowLevelNormal)
        
        SwiftMessages.show(config: successConfig, view: success)
    }
    
    func info(title:String = "Info",body:String,showButton:Bool = false){
        let info = MessageView.viewFromNib(layout: .MessageView)
        info.configureTheme(.info)
        info.button?.isHidden = !showButton
        info.configureContent(title: title, body: body, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Got it") { _ in SwiftMessages.hide()}
        var infoConfig = SwiftMessages.defaultConfig
        infoConfig.presentationStyle = .top
        if showButton{
            infoConfig.duration = .forever
        }
        SwiftMessages.show(config: infoConfig, view: info)
    }
}
