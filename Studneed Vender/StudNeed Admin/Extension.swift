//
//  Extension.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 15/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(hex:Int, alpha:CGFloat = 1.0) {
        self.init(
            red:   CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8)  / 255.0,
            blue:  CGFloat((hex & 0x0000FF) >> 0)  / 255.0,
            alpha: alpha
        )
    }
}

extension String{
    func isValidEmail() -> Bool {
        let emailTest = NSPredicate(format:"SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}")
        return emailTest.evaluate(with: self)
    }
    
    func isValidPhone() -> Bool{
        let emailTest = NSPredicate(format:"SELF MATCHES %@", "[0-9]{10}")
        return emailTest.evaluate(with: self)
    }
    
    func isString() -> Bool{
        let emailTest = NSPredicate(format:"SELF MATCHES %@", "[A-Za-z]")
        return emailTest.evaluate(with: self)
    }
}
extension UIView{
    func isBlurEffect(){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.alpha = 0.5
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
    
    func shadowEnable(height:NSInteger = 2,width:NSInteger = 0){
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowOpacity = 0.6
    }
    
    func isAssign3DEffect(){
        self.layer.shadowOffset = CGSize(width:10 ,height:10)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.6
        self.layer.masksToBounds = false
    }
    
    func rounding(ByCorners:UIRectCorner,Radii:CGFloat){
        let maskPath = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: ByCorners,
                                    cornerRadii: CGSize(width: Radii, height: Radii))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        self.layer.mask = shape
    }
}

extension UIImageView{
    func setImage(imageNamed:String){
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                let url =  URL(string: "\(ImagesPath)\(imageNamed)")
                self.sd_setImage(with: url, placeholderImage: Images.name.lodding, options: SDWebImageOptions.cacheMemoryOnly, completed: { (image, error, cache, url) in
                })
            }
        }
    }
    
    func setUserImage(imageNamed:String){
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                let url =  URL(string: "\(ImagesPath)\(imageNamed)")
                self.sd_setImage(with: url, placeholderImage: Images.name.lodding, options: SDWebImageOptions.cacheMemoryOnly, completed: { (image, error, cache, url) in
                })
            }
        }
    }
}

extension String{
    func heightForString(font:CGFloat, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont.systemFont(ofSize: font)
        label.text = self
        label.sizeToFit()
        return label.frame.height
    }
    
    func widthForString() -> CGFloat{
        let label:UILabel = UILabel()
//        label.numberOfLines = 0
//        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont.systemFont(ofSize: 16)
        label.text = self
        label.sizeToFit()
        return label.frame.width + 30
    }
}

extension NSDictionary{
    func integerValue(forKey:String) -> NSInteger{
        if self.value(forKey: forKey) != nil{
            return (self.value(forKey: forKey) as! NSString).integerValue
        }
        else{
            return 0
        }
    }

}



extension String {
    static func onlyTime(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss";
        return dateFormatter.string(from: date)
    }
    
    func onlyTime() -> Date {
        if !self.isEmpty {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss";
            return dateFormatter.date(from: self)!
        }
        else{
            return Date()
        }
    }
}

extension UIViewController{
    func backButtonAction(){
        self.navigationController!.popViewController(animated: true)
    }
}
