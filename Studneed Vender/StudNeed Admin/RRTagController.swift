//
//  RRTagController.swift
//  RRTagController
//
//  Created by Remi Robert on 20/02/15.
//  Copyright (c) 2015 Remi Robert. All rights reserved.
//

import UIKit

struct Tag {
    var isSelected: Bool
    var isLocked: Bool
    var textContent: String
}

let colorUnselectedTag = UIColor.white
let colorSelectedTag = UIColor(red:0.22, green:0.7, blue:0.99, alpha:1)

let colorTextUnSelectedTag = UIColor(red:0.33, green:0.33, blue:0.35, alpha:1)
let colorTextSelectedTag = UIColor.white

class RRTagController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UITextFieldDelegate{

    fileprivate var tags: Array<Tag>!
    fileprivate var navigationBarItem: UINavigationItem!
    fileprivate var leftButton: UIBarButtonItem!
    fileprivate var rigthButton: UIBarButtonItem!
    fileprivate var _totalTagsSelected = 0
    fileprivate var heightKeyboard: CGFloat = 0
    
    var blockFinih: ((_ selectedTags: Array<Tag>, _ unSelectedTags: Array<Tag>) -> ())!
    var blockCancel: (() -> ())!

    var totalTagsSelected: Int {
        get {
            return self._totalTagsSelected
        }
        set {
            if newValue == 0 {
                self._totalTagsSelected = 0
                return
            }
            self._totalTagsSelected += newValue
            self._totalTagsSelected = (self._totalTagsSelected < 0) ? 0 : self._totalTagsSelected
            self.navigationBarItem = UINavigationItem(title: "Services")
            self.navigationBarItem.leftBarButtonItem = self.leftButton
            self.navigationBarItem.rightBarButtonItem = self.rigthButton
            self.navigationBar.pushItem(self.navigationBarItem, animated: false)
        }
    }
    
    lazy var collectionTag: UICollectionView = {
        let layoutCollectionView = UICollectionViewFlowLayout()
        layoutCollectionView.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        layoutCollectionView.itemSize = CGSize(width: 90, height: 20)
        layoutCollectionView.minimumLineSpacing = 10
        layoutCollectionView.minimumInteritemSpacing = 5
        let collectionTag = UICollectionView(frame: self.view.frame, collectionViewLayout: layoutCollectionView)
        collectionTag.contentInset = UIEdgeInsets(top: 84, left: 0, bottom: 20, right: 0)
        collectionTag.delegate = self
        collectionTag.dataSource = self
        collectionTag.backgroundColor = UIColor.white
        collectionTag.register(RRTagCollectionViewCell.self, forCellWithReuseIdentifier: RRTagCollectionViewCellIdentifier)
        return collectionTag
    }()
    
    lazy var addNewTagCell: RRTagCollectionViewCell = {
        let addNewTagCell = RRTagCollectionViewCell()
        addNewTagCell.contentView.addSubview(addNewTagCell.textContent)
        addNewTagCell.textContent.text = "+"
        addNewTagCell.frame.size = CGSize(width: 40, height: 40)
        addNewTagCell.backgroundColor = UIColor.gray
        return addNewTagCell
    }()
    
    lazy var navigationBar: UINavigationBar = {
        let navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 64))
        
        self.navigationBarItem = UINavigationItem(title: "Services")
        self.navigationBarItem.leftBarButtonItem = self.leftButton
        
        navigationBar.pushItem(self.navigationBarItem, animated: true)
        
        self.view.backgroundColor = Color.code.lightBar
        navigationBar.isTranslucent = false
        navigationBar.tintColor = UIColor.white
        navigationBar.barTintColor = Color.code.navigationBar
        navigationBar.barStyle = .blackTranslucent
    
        return navigationBar
    }()
    
    func cancelTagController() {
        self.dismiss(animated: true, completion: { () -> Void in
            self.blockCancel()
        })
    }
    
    func finishTagController() {
        var selected: Array<Tag> = Array()
        var unSelected: Array<Tag> = Array()
        
        for currentTag in tags {
            if currentTag.isSelected {
                selected.append(currentTag)
            }
            else {
                unSelected.append(currentTag)
            }
        }
        self.dismiss(animated: true, completion: { () -> Void in
            self.blockFinih(selected,unSelected)
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
            if indexPath.row < tags.count {
                return RRTagCollectionViewCell.contentHeight(tags[indexPath.row].textContent)
            }
            return CGSize(width: 40, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedCell: RRTagCollectionViewCell? = collectionView.cellForItem(at: indexPath) as? RRTagCollectionViewCell
        
        if indexPath.row < tags.count {
            if tags[indexPath.row].isSelected == false {
                tags[indexPath.row].isSelected = true
                selectedCell?.animateSelection(tags[indexPath.row].isSelected)
                totalTagsSelected = 1
            }
            else {
                tags[indexPath.row].isSelected = false
                selectedCell?.animateSelection(tags[indexPath.row].isSelected)
                totalTagsSelected = -1
            }
        }
        else {
            let alertController = UIAlertController(title: "Add Service Name", message: "Please input your service", preferredStyle: .alert)
            
            let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
                 let field = alertController.textFields![0]
                let spaceSet = CharacterSet.whitespaces
                let contentTag = field.text?.trimmingCharacters(in: spaceSet)
                if strlen(contentTag) > 0 {
                    let newTag = Tag(isSelected: false, isLocked: false, textContent: contentTag!)
                    self.tags.insert(newTag, at: self.tags.count)
                    self.collectionTag.reloadData()
                }
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
            
            alertController.addTextField { (textField) in
                textField.placeholder = "Service Name"
            }
            
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: RRTagCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: RRTagCollectionViewCellIdentifier, for: indexPath) as? RRTagCollectionViewCell
        
        if indexPath.row < tags.count {
            let currentTag = tags[indexPath.row]
            cell?.initContent(currentTag)
        }
        else {
            cell?.initAddButtonContent()
        }
        return cell!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white

        leftButton = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(RRTagController.cancelTagController))
        rigthButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.done, target: self, action: #selector(RRTagController.finishTagController))
        totalTagsSelected = 0
        self.view.addSubview(collectionTag)
        self.view.addSubview(navigationBar)
        
        
    }
    
    class func displayTagController(parentController: UIViewController, tagsString: [tagIs],
        blockFinish: @escaping (_ selectedTags: Array<Tag>, _ unSelectedTags: Array<Tag>)->(), blockCancel: @escaping ()->()) {
        let tagController = RRTagController()
            tagController.tags = Array()
            for currentTag in tagsString {
                tagController.tags.append(Tag(isSelected: currentTag.isSelected, isLocked: false, textContent: currentTag.string))
            }
            tagController.blockCancel = blockCancel
            tagController.blockFinih = blockFinish
            parentController.present(tagController, animated: true, completion: nil)
    }

    class func displayTagController(parentController: UIViewController, _ tags: [Tag],
        blockFinish: @escaping (_ selectedTags: Array<Tag>, _ unSelectedTags: Array<Tag>)->(), blockCancel: @escaping ()->()) {
            let tagController = RRTagController()
            tagController.tags = tags
            tagController.blockCancel = blockCancel
            tagController.blockFinih = blockFinish
            tagController.present(tagController, animated: true, completion: nil)
    }
}

class tagIs{
    var string = String()
    var isSelected = Bool()
    
    init(string:String,isSelected:Bool){
        self.string = string
        self.isSelected = isSelected
    }
}
