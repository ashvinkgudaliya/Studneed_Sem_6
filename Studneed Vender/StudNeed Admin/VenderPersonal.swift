//
//  VenderPersonal.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 25/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import Alamofire

class VenderPersonal {
    static let data = VenderPersonal()
    
    func editPersonalData(parameter:Parameters, handler:@escaping (Bool) -> ()) {
        Alamofire.request(Web.url.editPersonalDetails, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    currentLoginVenderDetais = aDict.dictionary(forKey: "venderData")
                    UserDefaults.standard.set(currentLoginVenderDetais, forKey: "isUserLoginDetails")
                    Nottification.message.success(body: aDict.string(forKey: "message"))
                    handler(true)
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
    
    func getData(handler:@escaping (Bool) -> ()){
        
        let parameter : Parameters = [
            "venderId" : currentLoginVenderDetais.string(forKey: "venderId")
        ]
        
        Alamofire.request(Web.url.venderFullData, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    venderFullData = aDict.dictionary(forKey: "venderFullDetails")
                    handler(true)
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
}
