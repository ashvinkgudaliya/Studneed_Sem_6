//
//  Standard.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 02/04/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import Alamofire

class Standard {
    static let webServices = Standard()
    
    func get(venderDirectoryId:Any,directoryId:Any,handler:@escaping (NSArray) -> ()){
        
        let parameter : Parameters = [
            "directoryId" : directoryId,
            "venderDirectoryId" : venderDirectoryId
        ]
        
        Alamofire.request(Web.url.addStandard, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    handler(aDict.array(forKey: "allStandard"))
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
    
    func add(venderDirectoryId:Any, directoryId:Any, subCategory:Any, subSubCategory:Any, startTime:String, endTime:String, fees:String, handler:@escaping (Bool) -> ()){
        
        let parameter : Parameters = [
            "directoryId" : directoryId,
            "venderDirectoryId" : venderDirectoryId,
            "subCategory" : subCategory,
            "subSubCategory" : subSubCategory,
            "startTime" : startTime,
            "endTime" : endTime,
            "fees" : fees
        ]
        
        Alamofire.request(Web.url.addStandard, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    handler(true)
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
    
    func delete(Id:Any,handler:@escaping (Bool) -> ()){
        
        let parameter : Parameters = [
            "id" : Id
        ]
        
        Alamofire.request(Web.url.addStandard, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    handler(true)
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
}


struct StandardData {
    var endTime = String()
    var fees = NSInteger()
    var id = NSInteger()
    var startTime = String()
    var subCategory = NSInteger()
    var subCategoryName = String()
    var subSubCategory = NSInteger()
    var subSubCategoryName = String()
}
