//
//  SignUpViewController.swift
//  StudNeed
//
//  Created by Ashvin Gudaliya on 15/02/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import Alamofire

class SignUpViewController: UIViewController,UITextFieldDelegate{

    let imagesBackBroundView = UIImageView()
    
    let txtUserName = AMInputView()
    let txtEmailId = AMInputView()
    let txtPhoneNo = AMInputView()
    let txtPassword = AMInputView()
    let btnSignUp = ZFRippleButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarDesign()
        designLoginView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func navigationBarDesign(){
        self.view.backgroundColor = Color.code.lightBar
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Registration"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
    }
    
    func designLoginView(){
        imagesBackBroundView.frame = self.view.bounds
        imagesBackBroundView.image = UIImage(named: "HostalDefaultImages.jpg")
        imagesBackBroundView.isBlurEffect()
        self.view.addSubview(imagesBackBroundView)
        
        let gap = self.view.frame.height/33
        
        txtUserName.frame = CGRect(x: gap, y: (self.navigationController?.navigationBar.frame.origin.y)! + (self.navigationController?.navigationBar.frame.height)! + gap, width: self.view.frame.width - gap*2, height: gap*3)
        txtEmailId.frame = CGRect(x: gap, y: txtUserName.frame.height + txtUserName.frame.origin.y + gap, width: self.view.frame.width - gap*2, height: gap*3)
        txtPhoneNo.frame = CGRect(x: gap, y: txtEmailId.frame.height + txtEmailId.frame.origin.y + gap, width: self.view.frame.width - gap*2, height: gap*3)
        txtPassword.frame = CGRect(x: gap, y: txtPhoneNo.frame.height + txtPhoneNo.frame.origin.y + gap, width: self.view.frame.width - gap*2, height: gap*3)
        btnSignUp.frame = CGRect(x: gap, y: txtPassword.frame.height + txtPassword.frame.origin.y + gap*2, width: self.view.frame.width - gap*2, height: gap*3)
        
        txtUserName.placeholder = "Username"
        txtUserName.isCheckValidation = .userName
        txtUserName.returnKeyType = .next
        
        txtEmailId.placeholder = "Email Id"
        txtEmailId.isCheckValidation = .email
        txtEmailId.returnKeyType = .next
        
        txtPhoneNo.placeholder = "Phone No."
        txtPhoneNo.isCheckValidation = .phone
        txtPhoneNo.returnKeyType = .next
        
        txtPassword.placeholder = "Password"
        txtPassword.isCheckValidation = .password
        txtPassword.returnKeyType = .done
        
        btnSignUp.setTitle("Register", for: UIControlState())
        btnSignUp.layer.cornerRadius = 5
        btnSignUp.rippleBackgroundColor = UIColor.clear
        btnSignUp.rippleColor = Color.code.bar
        btnSignUp.addTarget(self, action: #selector(SignUpViewController.signUpButtonAction), for: .touchUpInside)
        btnSignUp.backgroundColor = Color.code.button
        
        self.view.addSubview(txtUserName)
        self.view.addSubview(txtEmailId)
        self.view.addSubview(txtPhoneNo)
        self.view.addSubview(txtPassword)
        self.view.addSubview(btnSignUp)
    }
    
    func signUpButtonAction(){
        if txtUserName.isValidated && txtEmailId.isValidated && txtPhoneNo.isValidated && txtPassword.isValidated{
            let parameter : Parameters = [
                "password":txtPassword.text,
                "venderName":txtUserName.text,
                "mobile":txtPhoneNo.text,
                "email":txtEmailId.text
            ]
            Alamofire.request(Web.url.registration, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                debugPrint(response)
                switch response.result{
                case .success(_):
                    let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    switch aDict.integer(forKey: Web.key.status){
                    case Web.statusCode.ok:
                        Nottification.message.success(body: aDict.string(forKey: Web.key.message))
                        _=self.navigationController?.popViewController(animated: true)
                        break
                    case Web.statusCode.failed:
                        Nottification.message.error(body: aDict.string(forKey: Web.key.message), showButton: true)
                        break
                    default:
                        Nottification.message.error(body: "Registration request failed")
                        break
                    }
                    break
                case .failure(_):
                    Nottification.message.error(body: "Request failed")
                    break
                }
            }
        }
    }
}
