//
//  DirectoryCommentsViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 12/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

class Message: NSObject {
    var username: String = ""
    var text: String = ""
    var images : String = ""
    var commetId : String = ""
}

import UIKit
import Alamofire

let DEBUG_CUSTOM_TYPING_INDICATOR = false

class CommentsViewController: SLKTextViewController {
    
    var messages = [Message]()
    var venderDirectoryId = NSInteger()
    var venderDirectoryName = String()
    var directoryImages = String()
    
    var users : [String] = []
    
    var searchResult: [String]?
    
    var pipWindow: UIWindow?
    
    var editingMessage = Message()
    
    override var tableView: UITableView {
        get {
            return super.tableView!
        }
    }
    
    // MARK: - Initialisation
    
    override class func tableViewStyle(for decoder: NSCoder) -> UITableViewStyle {
        
        return .plain
    }
    
    func commonInit() {
        
        NotificationCenter.default.addObserver(self.tableView, selector: #selector(UITableView.reloadData), name: NSNotification.Name.UIContentSizeCategoryDidChange, object: nil)
        NotificationCenter.default.addObserver(self,  selector: #selector(CommentsViewController.textInputbarDidMove(_:)), name: NSNotification.Name.SLKTextInputbarDidMove, object: nil)
    }
    
    override func viewDidLoad() {
        commentDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let parameter : Parameters = [
            "venderDirectoryId" : venderDirectoryId
        ]
        
        Alamofire.request(Web.url.comment, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    self.messages.removeAll()
                    let commentArr = aDict.array(forKey: "comment")
                    for i in 0..<commentArr.count{
                        let message = Message()
                        message.username = commentArr.dictionary(at: i).string(forKey: "userId") == "0" ? self.venderDirectoryName : commentArr.dictionary(at: i).string(forKey: "userName")
                        message.text = commentArr.dictionary(at: i).string(forKey: "comment")
                        message.images = commentArr.dictionary(at: i).string(forKey: "userId") == "0" ? self.directoryImages : commentArr.dictionary(at: i).string(forKey: "userProfile")
                        message.commetId = commentArr.dictionary(at: i).string(forKey: "id")
                        self.messages.append(message)
                    }
                    
                    for i in 0..<self.messages.count{
                        var isAdded = false
                        for j in 0..<self.users.count{
                            if self.users[j] == self.messages[i].username{
                                isAdded = true
                                break
                            }
                        }
                        if !isAdded {
                            if self.messages[i].username != self.venderDirectoryName {
                                self.users.append(self.messages[i].username)
                            }
                        }
                    }
                    self.tableView.reloadData()
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
    
    func commentDesign(){
        // Register a SLKTextView subclass, if you need any special appearance and/or behavior customisation.
        self.registerClass(forTextView: MessageTextView.classForCoder())
        
        if DEBUG_CUSTOM_TYPING_INDICATOR == true {
            // Register a UIView subclass, conforming to SLKTypingIndicatorProtocol, to use a custom typing indicator view.
            self.registerClass(forTypingIndicatorView: TypingIndicatorView.classForCoder())
        }
        
        super.viewDidLoad()
        
        self.commonInit()
        
        // Example's configuration
        //self.configureActionItems()
        
        // SLKTVC's configuration
        self.bounces = true
        self.shakeToClearEnabled = true
        self.isKeyboardPanningEnabled = true
        self.shouldScrollToBottomAfterKeyboardShows = false
        self.isInverted = true
        
        self.leftButton.setImage(UIImage(named: "icn_upload"), for: UIControlState())
        self.leftButton.tintColor = UIColor.gray
        
        self.rightButton.setTitle(NSLocalizedString("Send", comment: ""), for: UIControlState())
        
        self.textInputbar.autoHideRightButton = true
        self.textInputbar.maxCharCount = 256
        self.textInputbar.counterStyle = .split
        self.textInputbar.counterPosition = .top
        
        self.textInputbar.editorTitle.textColor = UIColor.darkGray
        self.textInputbar.editorLeftButton.tintColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
        self.textInputbar.editorRightButton.tintColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
        
        if DEBUG_CUSTOM_TYPING_INDICATOR == false {
            self.typingIndicatorView!.canResignByTouch = true
        }
        
        self.tableView.separatorStyle = .none
        self.tableView.register(MessageTableViewCell.classForCoder(), forCellReuseIdentifier: MessengerCellIdentifier)
        
        self.autoCompletionView.register(MessageTableViewCell.classForCoder(), forCellReuseIdentifier: AutoCompletionCellIdentifier)
        self.registerPrefixes(forAutoCompletion: ["@"])
        
        self.textView.placeholder = "Message";
        
        self.textView.registerMarkdownFormattingSymbol("*", withTitle: "Bold")
        self.textView.registerMarkdownFormattingSymbol("_", withTitle: "Italics")
        self.textView.registerMarkdownFormattingSymbol("~", withTitle: "Strike")
        self.textView.registerMarkdownFormattingSymbol("`", withTitle: "Code")
        self.textView.registerMarkdownFormattingSymbol("```", withTitle: "Preformatted")
        self.textView.registerMarkdownFormattingSymbol(">", withTitle: "Quote")
    }
    
    // MARK: - Lifeterm
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
    // MARK: - Example's Configuration
    
    func configureActionItems() {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.view.backgroundColor = Color.code.lightBar

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icn_arrow_down"), style: .plain, target: self, action: #selector(hideOrShowTextInputbar(_:)))
    }
    
    // MARK: - Action Methods
    
    func hideOrShowTextInputbar(_ sender: AnyObject) {
        
        guard let buttonItem = sender as? UIBarButtonItem else {
            return
        }
        
        let hide = !self.isTextInputbarHidden
        let image = hide ? UIImage(named: "icn_arrow_up") : UIImage(named: "icn_arrow_down")
        
        self.setTextInputbarHidden(hide, animated: true)
        buttonItem.image = image
    }
    
    func didLongPressCell(_ gesture: UIGestureRecognizer) {
        
        if gesture.state != .began {
            return
        }
        
        guard let cell = gesture.view as? MessageTableViewCell else {
            return
        }
        
        if self.messages[cell.indexPath.row].username == venderDirectoryName {
            self.editingMessage = self.messages[cell.indexPath.row]
            self.editText(self.editingMessage.text)
            self.tableView.scrollToRow(at: cell.indexPath, at: .bottom, animated: true)
        }
    }
    

    
    func textInputbarDidMove(_ note: Notification) {
        
        guard let pipWindow = self.pipWindow else {
            return
        }
        
        guard let userInfo = (note as NSNotification).userInfo else {
            return
        }
        
        guard let value = userInfo["origin"] as? NSValue else {
            return
        }
        
        var frame = pipWindow.frame
        frame.origin.y = value.cgPointValue.y - 60.0
        
        pipWindow.frame = frame
    }
    
    // MARK: - Overriden Methods
    
    override func ignoreTextInputbarAdjustment() -> Bool {
        return super.ignoreTextInputbarAdjustment()
    }
    
    override func forceTextInputbarAdjustment(for responder: UIResponder!) -> Bool {
        
        if #available(iOS 8.0, *) {
            guard let _ = responder as? UIAlertController else {
                // On iOS 9, returning YES helps keeping the input view visible when the keyboard if presented from another app when using multi-tasking on iPad.
                return UIDevice.current.userInterfaceIdiom == .pad
            }
            return true
        }
        else {
            return UIDevice.current.userInterfaceIdiom == .pad
        }
    }
    
    // Notifies the view controller that the keyboard changed status.
    override func didChangeKeyboardStatus(_ status: SLKKeyboardStatus) {
        switch status {
        case .willShow:
            print("Will Show")
        case .didShow:
            print("Did Show")
        case .willHide:
            print("Will Hide")
        case .didHide:
            print("Did Hide")
        }
    }
    
    // Notifies the view controller that the text will update.
    override func textWillUpdate() {
        super.textWillUpdate()
    }
    
    // Notifies the view controller that the text did update.
    override func textDidUpdate(_ animated: Bool) {
        super.textDidUpdate(animated)
    }
    
    // Notifies the view controller when the left button's action has been triggered, manually.
    override func didPressLeftButton(_ sender: Any!) {
        super.didPressLeftButton(sender)
        
        self.dismissKeyboard(true)
        self.performSegue(withIdentifier: "Push", sender: nil)
    }
    
    // Notifies the view controller when the right button's action has been triggered, manually or by using the keyboard return key.
    override func didPressRightButton(_ sender: Any!) {
        let parameter : Parameters = [
            "venderDirectoryId" : venderDirectoryId,
            "userId" : 0,
            "comment" : self.textView.text
        ]
        
        Alamofire.request(Web.url.comment, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    self.textView.refreshFirstResponder()
                    
                    let message = Message()
                    message.username = self.venderDirectoryName
                    message.text = self.textView.text
                    message.images = self.directoryImages
                    message.commetId = aDict.string(forKey: "messageId")
                    
                    let indexPath = IndexPath(row: 0, section: 0)
                    let rowAnimation: UITableViewRowAnimation = self.isInverted ? .bottom : .top
                    let scrollPosition: UITableViewScrollPosition = self.isInverted ? .bottom : .top
                    
                    self.tableView.beginUpdates()
                    self.messages.insert(message, at: 0)
                    self.tableView.insertRows(at: [indexPath], with: rowAnimation)
                    self.tableView.endUpdates()
                    
                    self.tableView.scrollToRow(at: indexPath, at: scrollPosition, animated: true)
                    self.tableView.reloadRows(at: [indexPath], with: .automatic)
                    super.didPressRightButton(sender)
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
    
    override func keyForTextCaching() -> String? {
        
        return Bundle.main.bundleIdentifier
    }
    
    // Notifies the view controller when tapped on the right "Accept" button for commiting the edited text
    override func didCommitTextEditing(_ sender: Any) {
        let parameter : Parameters = [
            "commentId" : self.editingMessage.commetId,
            "comment" : self.textView.text
        ]
        
        Alamofire.request(Web.url.comment, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            debugPrint(response)
            switch response.result{
            case .success(_):
                let aDict = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                switch aDict.integer(forKey: Web.key.status){
                case Web.statusCode.ok:
                    self.editingMessage.text = self.textView.text
                    self.tableView.reloadData()
                    super.didCommitTextEditing(sender)
                    break
                default:
                    Nottification.message.error(body: aDict.string(forKey: "message"))
                    break
                }
                break
            case .failure(_):
                Nottification.message.error(body: "Request failed")
                break
            }
        }
    }
    
    // Notifies the view controller when tapped on the left "Cancel" button
    override func didCancelTextEditing(_ sender: Any) {
        super.didCancelTextEditing(sender)
    }
    
    override func canPressRightButton() -> Bool {
        return super.canPressRightButton()
    }
    
    override func canShowTypingIndicator() -> Bool {
        
        if DEBUG_CUSTOM_TYPING_INDICATOR == true {
            return true
        }
        else {
            return super.canShowTypingIndicator()
        }
    }
    
    override func shouldProcessText(forAutoCompletion text: String) -> Bool {
        return true
    }
    
    override func didChangeAutoCompletionPrefix(_ prefix: String, andWord word: String) {
        
        var array:Array<String> = []
        let wordPredicate = NSPredicate(format: "self BEGINSWITH[c] %@", word);
        
        self.searchResult = nil
        
        if prefix == "@" {
            if word.characters.count > 0 {
                array = self.users.filter { wordPredicate.evaluate(with: $0) };
            }
            else {
                array = self.users
            }
        }
        
        var show = false
        
        if array.count > 0 {
            let sortedArray = array.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            self.searchResult = sortedArray
            show = sortedArray.count > 0
        }
        
        self.showAutoCompletionView(show)
    }
    
    override func heightForAutoCompletionView() -> CGFloat {
        
        guard let searchResult = self.searchResult else {
            return 0
        }
        
        let cellHeight = self.autoCompletionView.delegate?.tableView!(self.autoCompletionView, heightForRowAt: IndexPath(row: 0, section: 0))
        guard let height = cellHeight else {
            return 0
        }
        return height * CGFloat(searchResult.count)
    }
    
    // MARK: - UITableViewDataSource Methods
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tableView {
            return self.messages.count
        }
        else {
            if let searchResult = self.searchResult {
                return searchResult.count
            }
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableView {
            return self.messageCellForRowAtIndexPath(indexPath)
        }
        else {
            return self.autoCompletionCellForRowAtIndexPath(indexPath)
        }
    }
    
    func messageCellForRowAtIndexPath(_ indexPath: IndexPath) -> MessageTableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: MessengerCellIdentifier) as! MessageTableViewCell
        
        if cell.gestureRecognizers?.count == nil {
            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(CommentsViewController.didLongPressCell(_:)))
            cell.addGestureRecognizer(longPress)
        }
        
        let message = self.messages[(indexPath as NSIndexPath).row]
        
        if message.username == venderDirectoryName {
            cell.titleLabel.attributedText = NSAttributedString(string: message.username, attributes: [ NSForegroundColorAttributeName: UIColor.black ])
        }
        else{
            cell.titleLabel.text = message.username
        }
        cell.bodyLabel.text = message.text
        cell.thumbnailView.setUserImage(imageNamed: message.images)
        
        cell.indexPath = indexPath
        cell.usedForMessage = true
        
        // Cells must inherit the table view's transform
        // This is very important, since the main table view may be inverted
        cell.transform = self.tableView.transform
        
        return cell
    }
    
    func autoCompletionCellForRowAtIndexPath(_ indexPath: IndexPath) -> MessageTableViewCell {
        
        let cell = self.autoCompletionView.dequeueReusableCell(withIdentifier: AutoCompletionCellIdentifier) as! MessageTableViewCell
        cell.indexPath = indexPath
        cell.selectionStyle = .default
        
        guard let searchResult = self.searchResult else {
            return cell
        }
        
        guard let prefix = self.foundPrefix else {
            return cell
        }
        
        var text = searchResult[(indexPath as NSIndexPath).row]
        
        if prefix == "#" {
            text = "# " + text
        }
        
        cell.titleLabel.text = text
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == self.tableView {
            let message = self.messages[(indexPath as NSIndexPath).row]
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineBreakMode = .byWordWrapping
            paragraphStyle.alignment = .left
            
            let pointSize = MessageTableViewCell.defaultFontSize()
            
            let attributes = [
                NSFontAttributeName : UIFont.systemFont(ofSize: pointSize),
                NSParagraphStyleAttributeName : paragraphStyle
            ]
            
            var width = tableView.frame.width-kMessageTableViewCellAvatarHeight
            width -= 25.0
            
            let titleBounds = (message.username as NSString).boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            let bodyBounds = (message.text as NSString).boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            
            if message.text.characters.count == 0 {
                return 0
            }
            
            var height = titleBounds.height
            height += bodyBounds.height
            height += 40
            
            if height < kMessageTableViewCellMinimumHeight {
                height = kMessageTableViewCellMinimumHeight
            }
            
            return height
        }
        else {
            return kMessageTableViewCellMinimumHeight
        }
    }
    
    // MARK: - UITableViewDelegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.autoCompletionView {
            
            guard let searchResult = self.searchResult else {
                return
            }
            
            var item = searchResult[(indexPath as NSIndexPath).row]
            
            if self.foundPrefix == "@" && self.foundPrefixRange.location == 0 {
                item += ":"
            }
            
            item += " "
            
            self.acceptAutoCompletion(with: item, keepPrefix: true)
        }
    }
    // MARK: - UIScrollViewDelegate Methods
    
    // Since DirectoryCommentsViewController uses UIScrollViewDelegate to update a few things, it is important that if you override this method, to call super.
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
    }
    
    // MARK: - UITextViewDelegate Methods
    
    override func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return true
    }
    
    override func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        // Since DirectoryCommentsViewController uses UIScrollViewDelegate to update a few things, it is important that if you override this method, to call super.
        return true
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return super.textView(textView, shouldChangeTextIn: range, replacementText: text)
    }
    
    override func textView(_ textView: SLKTextView, shouldOfferFormattingForSymbol symbol: String) -> Bool {
        
        if symbol == ">" {
            let selection = textView.selectedRange
            
            // The Quote formatting only applies new paragraphs
            if selection.location == 0 && selection.length > 0 {
                return true
            }
            
            // or older paragraphs too
            let prevString = (textView.text as NSString).substring(with: NSMakeRange(selection.location-1, 1))
            
            if CharacterSet.newlines.contains(UnicodeScalar((prevString as NSString).character(at: 0))!) {
                return true
            }
            
            return false
        }
        
        return super.textView(textView, shouldOfferFormattingForSymbol: symbol)
    }
    
    override func textView(_ textView: SLKTextView, shouldInsertSuffixForFormattingWithSymbol symbol: String, prefixRange: NSRange) -> Bool {
        
        if symbol == ">" {
            return false
        }
        
        return super.textView(textView, shouldInsertSuffixForFormattingWithSymbol: symbol, prefixRange: prefixRange)
    }
}
