//
//  AddPgViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 16/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//



import UIKit
import Former
import Alamofire
import KYNavigationProgress
import MapKit

class AddPgViewController: FormViewController,UITextFieldDelegate,GoogleLocationControllerProtocol{
    
    var directoryId = NSInteger()
    var gender = "Both"
    var contactNo = ""
    var CurrentLocation = CLLocation()
    var tag : [tagIs] = []
    var selectedServices = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        navigationBarDesign()
        // Do any additional setup after loading the view.
        
        VenderDirectory.data.pgServices { webTag in
            if self.tag.count == 0{
                self.tag = webTag
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func navigationBarDesign(){
        self.view.backgroundColor = Color.code.lightBar
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(addData))
    }

    
    func addData(){
        if name.text == nil || name.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Name")
        }
        else if emailId.text != nil || !emailId.text!.isEmpty ? !emailId.text!.isValidEmail() : false{
            Nottification.message.error(body: "Enter Valid Email ID")
        }
        else if contactNo.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Mobile Number")
        }
        else if !contactNo.isValidPhone(){
            Nottification.message.error(body: "Enter 10 Digit Mobile Number")
        }
        else if fees.text == nil || fees.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName)  Fees")
        }
        else if discription.text  == nil || discription.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Discription")
        }
        else if address.text  == nil || address.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Address")
        }
        else if locality.text  == nil || locality.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Country")
        }
        else if country.text  == nil || country.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Country")
        }
        else if state.text  == nil || state.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) State")
        }
        else if city.text  == nil || city.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) City")
        }
        else if pincode.text  == nil || pincode.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Pincode")
        }
        else{
            let parameter : Parameters = [
                "venderId" : currentLoginVenderDetais.string(forKey: "venderId"),
                "directoryId" : directoryId,
                "name" : name.text!,
                "address" : address.text!,
                "gender" : gender,
                "breakfartEndTime" : String.onlyTime(date: breakfartEndTime.date),
                "breakfartStartTime" : String.onlyTime(date: breakfartStartTime.date),
                "lunchStartTime" : String.onlyTime(date: lunchStartTime.date),
                "lunchEndTime" : String.onlyTime(date: lunchEndTime.date),
                "dinnerStartTime" : String.onlyTime(date: dinnerStartTime.date),
                "dinnerEndTime" : String.onlyTime(date: dinnerEndTime.date),
                "fees" : fees.text!,
                "services" : selectedServices,
                "discription" : discription.text!,
                "emailId" : emailId.text!,
                "website" : webSite.text == nil ? "":webSite.text!,
                "contactNo" : contactNo,
                "locality" : locality.text!,
                "pinCode" : pincode.text!,
                "country" : country.text!,
                "state" : state.text!,
                "city" : city.text!,
                "latitute" : CurrentLocation.coordinate.latitude,
                "longitute" : CurrentLocation.coordinate.longitude
            ]
            VenderDirectory.data.add(parameter: parameter, addedDirectoryId: { venderDirId in
                let imagesViewController = ImageCollectionViewController(nibName: "ImageCollectionViewController", bundle: nil)
                imagesViewController.venderDirectoryId = venderDirId
                self.navigationController?.pushViewController(imagesViewController, animated: true)
            })
        }
    }
    
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    fileprivate let name = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Name"
        }.configure { field in
            field.placeholder = "Enter Name"
        }
    
    
    fileprivate let emailId = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Email ID"
        }.configure {  field in
            field.placeholder = "Email ID"
        }

    fileprivate let discription = TextViewRowFormer<FormTextViewCell>() { field in
        field.textView.textColor = .formerSubColor()
        field.textView.font = .systemFont(ofSize: 15)
        }.configure { field in
            field.placeholder = "Add your \(titleName) discription"
        }
    
    fileprivate let webSite = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Web Site"
        }.configure { field in
            field.placeholder = "www.example.com"
    }
    
    fileprivate let address = TextViewRowFormer<FormTextViewCell>() { field in
        field.textView.textColor = .formerSubColor()
        field.textView.font = .systemFont(ofSize: 15)
        }.configure { field in
            field.placeholder = "Add your \(titleName) Address"
        }
    
    fileprivate let locality = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Locality"
        }.configure { field in
            field.placeholder = "Enter Locality"
    }
    
    fileprivate let country = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "County"
        }.configure { field in
            field.placeholder = "Enter County"
    }
    
    fileprivate let state = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "State"
        }.configure { field in
            field.placeholder = "Enter State"
    }
    
    fileprivate let city = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "City"
        }.configure { field in
            field.placeholder = "Enter City"
    }
    
    fileprivate let pincode = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Pincode"
        }.configure { field in
            field.placeholder = "Enter Pincode"
    }
    
    fileprivate let fees = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Fees"
        field.textField.keyboardType = .numberPad
        }.configure { field in
            field.placeholder = "Amounts"
        }

    fileprivate let breakfartStartTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "Start Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup { field in
            field.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    fileprivate let breakfartEndTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "End Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup { field in
            field.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)

    fileprivate let lunchStartTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "Start Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup { field in
            field.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    fileprivate let lunchEndTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "End Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup { field in
            field.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    fileprivate let dinnerStartTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "Start Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup { field in
            field.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)
    
    fileprivate let dinnerEndTime = InlineDatePickerRowFormer<FormInlineDatePickerCell>() { field in
        field.titleLabel.text = "End Time"
        field.titleLabel.textColor = .formerColor()
        field.titleLabel.font = .boldSystemFont(ofSize: 15)
        field.displayLabel.textColor = .formerSubColor()
        field.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup { field in
            field.datePicker.datePickerMode = .time
        }.displayTextFromDate(String.onlyTime)

    private func configure() {
        title = "Add \(titleName) Details"
        
        let phoneRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
            field.titleLabel.text = "Phone"
            field.textField.inputAccessoryView = self.formerInputAccessoryView
            field.textField.delegate = self
            field.textField.keyboardType = .numberPad
            }.configure { field in
                field.placeholder = "Add your phone number"
            }.onTextChanged { field in
                self.contactNo = field
        }
        
        let mapLocation = SwitchRowFormer<FormSwitchCell>() { field in
            field.titleLabel.text = "Current Location Enabled"
            field.titleLabel.textColor = Color.code.navigationBar
            field.titleLabel.font = .boldSystemFont(ofSize: 15)
            field.switchButton.onTintColor = .formerSubColor()
            }.configure { field in
                field.switchWhenSelected = false
            }.onSwitchChanged { field in
                if field{
                    self.openMapActionController()
                }
        }

        let genderRow = SegmentedRowFormer<FormSegmentedCell>(instantiateType: .Class) { field in
            field.tintColor = .formerSubColor()
            }.configure { field in
                field.segmentTitles = ["Boys Only", "Girls Only", "Both"]
                field.selectedIndex = 2
            }.onSegmentSelected({ (index, value) in
                self.gender = value
            })
        
        let venderServices = LabelRowFormer<FormLabelCell>()
            .configure {
                $0.text = "Added Services"
                $0.subText = "\(self.tag.count)"
            }.onSelected { [weak self] filed in
                
                RRTagController.displayTagController(parentController: self!, tagsString: (self?.tag)!, blockFinish: { (selectedTags, unSelectedTags) -> () in
                    self?.selectedServices = ""
                    for tagTemp in selectedTags{
                        var isadded = false
                        for i in 0..<(self?.tag.count)!{
                            let temp : tagIs? = (self?.tag[i])!
                            if tagTemp.textContent == temp?.string {
                                temp?.isSelected = true
                                self?.tag[i] = temp!
                                isadded = true
                            }
                        }
                        
                        if !isadded {
                            self?.tag.append(tagIs(string: tagTemp.textContent, isSelected: true))
                        }
                        self?.selectedServices = (self?.selectedServices)! + " " + tagTemp.textContent
                        filed.subText = "\(selectedTags.count)"
                    }
                    self?.former.reload()
                }) { () -> () in
                }
                self?.former.deselect(animated: true)
        }

        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure { header in
                    header.viewHeight = 40
                    header.text = text
            }
        }
        
        former.append(sectionFormer:
            SectionFormer(rowFormer: name, emailId, phoneRow)
                .set(headerViewFormer: createHeader("Basic Details")),
            SectionFormer(rowFormer: genderRow)
                .set(headerViewFormer: createHeader("Select \(titleName) Type")),
            SectionFormer(rowFormer: fees),
            SectionFormer(rowFormer: discription,webSite)
                .set(headerViewFormer: createHeader("Discription")),
            SectionFormer(rowFormer: venderServices)
                .set(headerViewFormer: createHeader("Other Services")),
            
            SectionFormer(rowFormer: breakfartStartTime, breakfartEndTime)
                .set(headerViewFormer: createHeader("Breakfast ime")),
            SectionFormer(rowFormer: lunchStartTime, lunchEndTime)
                .set(headerViewFormer: createHeader("Lunch time")),
            SectionFormer(rowFormer: dinnerStartTime, dinnerEndTime)
                .set(headerViewFormer: createHeader("Dinner time")),
            
            SectionFormer(rowFormer: mapLocation).set(headerViewFormer: createHeader("Set Current Location")),
            SectionFormer(rowFormer: address,locality , city, state, country, pincode).set(headerViewFormer: createHeader("Address Details"))
        )
    }
}

extension AddPgViewController{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) == nil{
            let len = (textField.text?.characters.count)! + string.characters.count
            if len <= 10 {
                return true
            }
            else{
                return false
            }
        }
        return false
    }
    
    func openMapActionController() {
        let googleMap = GoogleMapViewController()
        googleMap.delegate = self
        self.navigationController?.pushViewController(googleMap, animated: true)
    }
    
    func didFinishTask(placemark: CLPlacemark, location: CLLocation) {
        self.pincode.text = placemark.addressDictionary?["ZIP"] as? String
        self.locality.text = placemark.addressDictionary?["Street"] as? String
        self.city.text = placemark.addressDictionary?["City"] as? String
        self.country.text = placemark.addressDictionary?["Country"] as? String
        self.state.text = placemark.addressDictionary?["State"] as? String
        self.CurrentLocation = location
        self.former.reload()
    }
}

