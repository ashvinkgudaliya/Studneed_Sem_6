//
//  AddSchollViewController.swift
//  StudNeed Admin
//
//  Created by Ashvin Gudaliya on 16/03/17.
//  Copyright © 2017 Ashvin Gudaliya. All rights reserved.
//

import UIKit
import Former
import Alamofire
import MapKit


var titleName = String()
class AddSchollViewController: FormViewController,UITextFieldDelegate,GoogleLocationControllerProtocol{
    
    var directoryId = NSInteger()
    var gender = "Both"
    var contactNo = ""
    var CurrentLocation = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        navigationBarDesign()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func navigationBarDesign(){
        self.view.backgroundColor = Color.code.lightBar
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.code.navigationBar
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Images.name.back, style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(addData))
    }
    
    func addData(){
        if name.text == nil || name.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Name")
        }
        else if emailId.text == nil || !emailId.text!.isEmpty{
            if !emailId.text!.isValidEmail(){
                Nottification.message.error(body: "Enter Valid Email ID")
            }
        }
        else if contactNo.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Mobile Number")
        }
        else if !contactNo.isValidPhone(){
            Nottification.message.error(body: "Enter 10 Digit Mobile Number")
        }
        else if discription.text == nil || discription.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Discription")
        }
        else if address.text == nil || address.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Address")
        }
        else if locality.text == nil || locality.text!.isEmpty{
            Nottification.message.error(body: "Enter Store Country")
        }
        else if country.text == nil || country.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Country")
        }
        else if state.text == nil || state.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) State")
        }
        else if city.text == nil || city.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) City")
        }
        else if pincode.text == nil || pincode.text!.isEmpty{
            Nottification.message.error(body: "Enter \(titleName) Pincode")
        }
        else{
            let parameter : Parameters = [
                "venderId" : currentLoginVenderDetais.string(forKey: "venderId"),
                "directoryId" : directoryId,
                "name" : name.text!,
                "address" : address.text!,
                "gender" : gender,
                "discription" : discription.text!,
                "emailId" : emailId.text!,
                "website" : webSite.text == nil ? "":webSite.text!,
                "contactNo" : contactNo,
                "pinCode" : pincode.text!,
                "country" : country.text!,
                "locality" : locality.text!,
                "state" : state.text!,
                "city" : city.text!,
                "latitute" : CurrentLocation.coordinate.latitude,
                "longitute" : CurrentLocation.coordinate.longitude
            ]
            VenderDirectory.data.add(parameter: parameter, addedDirectoryId: { venderDirId in
                let imagesViewController = ImageCollectionViewController(nibName: "ImageCollectionViewController", bundle: nil)
                imagesViewController.venderDirectoryId = venderDirId
                self.navigationController?.pushViewController(imagesViewController, animated: true)
            })
        }
    }
    
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    let name = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Name"
        }.configure { field in
            field.placeholder = "Enter Name"
        }
    
    let emailId = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Email ID"
        field.textField.keyboardType = .emailAddress
        }.configure { field in
            field.placeholder = "Enter Email ID"
        }

    let discription = TextViewRowFormer<FormTextViewCell>() { field in
        field.textView.textColor = .formerSubColor()
        field.textView.font = .systemFont(ofSize: 15)
        }.configure { field in
            field.placeholder = "Add your \(titleName) discription"
        }
    
    let webSite = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Web Site"
        }.configure { field in
            field.placeholder = "www.example.com"
        }
    
    let address = TextViewRowFormer<FormTextViewCell>() { field in
        field.textView.textColor = .formerSubColor()
        field.textView.font = .systemFont(ofSize: 15)
        }.configure { field in
            field.placeholder = "Add your \(titleName) Address"
        }
    
    fileprivate let locality = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Locality"
        }.configure { field in
            field.placeholder = "Enter Locality"
    }
    
    fileprivate let country = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "County"
        }.configure { field in
            field.placeholder = "Enter County"
    }
    
    fileprivate let state = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "State"
        }.configure { field in
            field.placeholder = "Enter State"
    }
    
    fileprivate let city = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "City"
        }.configure { field in
            field.placeholder = "Enter City"
    }
    
    fileprivate let pincode = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
        field.titleLabel.text = "Pincode"
        }.configure { field in
            field.placeholder = "Enter Pincode"
    }
    
    private func configure() {
        title = "Add \(titleName) Details"
        
        let phoneRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { field in
            field.titleLabel.text = "Phone"
            field.textField.inputAccessoryView = self.formerInputAccessoryView
            field.textField.delegate = self
            field.textField.keyboardType = .numberPad
            }.configure { field in
                field.placeholder = "Add your phone number"
            }.onTextChanged {
                self.contactNo = $0
        }
    
        let mapLocation = SwitchRowFormer<FormSwitchCell>() { field in
            field.titleLabel.text = "Current Location Enabled"
            field.titleLabel.textColor = Color.code.navigationBar
            field.titleLabel.font = .boldSystemFont(ofSize: 15)
            field.switchButton.onTintColor = .formerSubColor()
            }.configure { field in
                field.switchWhenSelected = false
            }.onSwitchChanged { field in
                if field{
                    self.openMapActionController()
                }
        }
        
        let positions = ["Boys Only", "Girls Only", "Both"]
        let genderRow = SegmentedRowFormer<FormSegmentedCell>(instantiateType: .Class) {
            $0.tintColor = .formerSubColor()
            }.configure {
                $0.segmentTitles = positions
                $0.selectedIndex = 2
            }.onSegmentSelected({ (index, value) in
                self.gender = value
            })
        
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.viewHeight = 40
                    $0.text = text
            }
        }
        
        former.append(sectionFormer:
            SectionFormer(rowFormer: name, emailId, phoneRow).set(headerViewFormer: createHeader("Basic Details")),
            SectionFormer(rowFormer: genderRow).set(headerViewFormer: createHeader("Select \(titleName) Type")),
            SectionFormer(rowFormer: discription,webSite).set(headerViewFormer: createHeader("Discription")),
            SectionFormer(rowFormer: mapLocation).set(headerViewFormer: createHeader("Set Current Location")),
            SectionFormer(rowFormer: address, locality, city, state, country,pincode).set(headerViewFormer: createHeader("Address Details"))
        )
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) == nil{
            let len = (textField.text?.characters.count)! + string.characters.count
            if len <= 10 {
                return true
            }
            else{
                return false
            }
        }
        return false
    }
    
    func openMapActionController() {
        let googleMap = GoogleMapViewController()
        googleMap.delegate = self
        self.navigationController?.pushViewController(googleMap, animated: true)
    }
    
    func didFinishTask(placemark: CLPlacemark, location: CLLocation) {
        self.pincode.text = placemark.addressDictionary?["ZIP"] as? String
        self.locality.text = placemark.addressDictionary?["Street"] as? String
        self.city.text = placemark.addressDictionary?["City"] as? String
        self.country.text = placemark.addressDictionary?["Country"] as? String
        self.state.text = placemark.addressDictionary?["State"] as? String
        self.CurrentLocation = location
        self.former.reload()
    }
}
